var myWin;
var myFieldName;
var imageElement;
var ckeditoryPath = baseUrl + 'lib/ckfinder/';

function openCKFinder(fieldName, url, type, win) {
    tinyMCE.activeEditor.windowManager.open({
        file: ckeditoryPath+'ckfinder.html?type=Images',
        title : 'CKFinder',
        width: 700,
        height: 500,
        resizable: "yes",
        inline: true,
        close_previous: "no",
        popup_css: false
    }, {
        window : win,
        input : fieldName
    });
    return false;
}

function ckFileBrowser(fieldName, url, type, win) {
    myWin = win;
    myFieldName = fieldName;

    // You can use the "CKFinder" class to render CKFinder in a page:
    var finder = new CKFinder();

    // The path for the installation of CKFinder (default = "/ckfinder/").
    finder.basePath = ckeditoryPath;

    // Name of a function which is called when a file is selected in CKFinder.
    finder.selectActionFunction = SetFileField;

    // Additional data to be passed to the selectActionFunction in a second argument.
    // We'll use this feature to pass the Id of a field that will be updated.
    finder.selectActionData = fieldName;

    // Launch CKFinder
    finder.popup();
}

function SetFileField(fileUrl, data){
    console.log(fileUrl);
    $("#"+data.selectActionData).val(fileUrl);
}

function imageManager(element) {
    console.log(element);
    imageElement = element;
    // You can use the "CKFinder" class to render CKFinder in a page:
    var finder = new CKFinder();

    // The path for the installation of CKFinder (default = "/ckfinder/").
    finder.basePath = ckeditoryPath;

    // Name of a function which is called when a file is selected in CKFinder.
    finder.selectActionFunction = imageManagerSetFileField;

    // Additional data to be passed to the selectActionFunction in a second argument.
    // We'll use this feature to pass the Id of a field that will be updated.
    //finder.selectActionData = field_name;

    // Launch CKFinder
    finder.popup();
}

function imageManagerSetFileField(fileUrl, data){
    imageElement.val(fileUrl);
}

function selectFileWithCKFinder( elementId ) {
    CKFinder.popup( {
        chooseFiles: true,
        width: 800,
        height: 600,
        onInit: function( finder ) {
            finder.on( 'files:choose', function( evt ) {
                var file = evt.data.files.first();
                var path = file.getUrl();
                var baseurlupload = baseUrl + 'uploads';
                var chuoi = path.replace(baseurlupload, "");
                elementId.val(chuoi);
            } );
            finder.on( 'file:choose:resizedImage', function( evt ) {
                var output = document.getElementById( elementId );
                output.value = evt.data.resizedUrl;
            } );
        }

    } );

}
