<?php

namespace App;

use DB;
use Yajra\Datatables\Datatables;
use Lang;
class CatProductModel extends MyModel
{
    protected $table    =   'cat_products';
    public $timestamps  =   true;

    public function product()
    {
        return $this->hasMany('App\ProductModel', 'id_cat', 'id');
    }

    public function init_data($action=false)
    {
        return array(
            array(
                'name'          =>  'title',
                'label'         =>  'Tên danh mục',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'priority',
                'label'         =>  'Ưu tiên',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'cat_parent',
                'label'         =>  'Danh mục cha',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'editor',
                'label'         =>  'Người sửa',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'action',
                'label'         =>  ('<div class="btn-group">
                                         <a style="width: 82px;" href="'.url('admin/cat_products/add').'" class="btn btn btn-success">Thêm</a>
                                    </div>'),
                'orderable'     =>  'false',
                'width'         =>  '5%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
        );
    }

    public function getData($action=array())
    {
        //active
        $active=$this->active_list;

        $result = DB::table($this->table)
            ->leftjoin('users as users_creator', 'users_creator.id', '=', $this->table.'.creator_id')
            ->leftjoin('users as users_editor', 'users_editor.id', '=', $this->table.'.updator_id')
            ->select($this->table.'.title_vn as title',$this->table.'.active',$this->table.'.created_at',$this->table.'.id',$this->table.'.parent_id','users_creator.name as creator','users_editor.name as editor',$this->table.'.priority');

        return Datatables::of($result)

            ->addColumn('action', function($user) use ($action) {
                return '<div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Chức năng
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>                       
                             <i class="fa fa-pencil fa-fw" style="margin-left:10px"></i> <a href="'.url('admin/cat_products/edit/'.$user->id).'" style="    display: inline-block;width: 75%;">Chỉnh sửa</a>
                       </li>
                        <li>
                           <i class="fa fa-trash-o  fa-fw" style="margin-left:10px"></i><a onclick="return xacnhanxoa(\'Bạn có muốn xóa không ?\')" href="'.url('admin/cat_products/delete/'.$user->id).'" style="display: inline-block;width: 75%;">Xóa</a>
                        </li>
                    </ul>
                  </div>';
            })
            ->editColumn('active', function ($result) use ($active) {
                return $active[$result->active];
            })
            ->filterColumn('title', function($query,$keyword){
                $query->where($this->table.'.title_vn', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('category', function($query,$keyword){
                $query->where('users_creator.name', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('editor', function($query,$keyword){
                $query->where('users_editor.name', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('priority', function($query,$keyword){
                $query->where($this->table.'.priority', 'like', '%'.$keyword.'%');
            })
            //cách search $key với $value khác nhau
            ->filterColumn('active', function($query,$keyword) use ($active){
                foreach($active as $key=>$value){
                    if($value==$keyword){
                        $query->where('news.active', '=', $key);
                        break;
                    }
                }
            })

            ->make(true);
    }

    public function get_cat_parent($limit)
    {
        $result = DB::table($this->table)
                ->select($this->table.'.id',$this->table.'.title_vn',$this->table.'.active',$this->table.'.parent_id',$this->table.'.code')
                ->where($this->table.'.active', '=', '1')
                ->where($this->table.'.parent_id', '=', '0')
                ->orderBy('priority')
                ->limit($limit)
                ->get()->toArray();
        return $result;
    }


    public function load_SubMenu($id_cat)
    {
        $result   =   DB::table($this->table)
            ->select($this->table.'.id',$this->table.'.title_vn',$this->table.'.parent_id',$this->table.'.active',$this->table.'.code')
            ->where($this->table.'.parent_id', '=', $id_cat)
            ->where($this->table.'.active', '=', '1')
            ->orderBy('priority')
            ->get()->toArray();
        return $result;
    }


    public function load_product_by_code($code)
    {
        $result   =   DB::table($this->table)
            ->select('products.title_vn','products.img_avatar1','products.code','products.content_vn','products.created_at','products.id_cat','products.price_mb','products.price_mt','products.price_mn',$this->table.'.id')
            ->join('products', 'products.id_cat', '=', $this->table.'.id')
            ->where($this->table.'.code', '=', $code)
            ->paginate(16);

        return $result;
    }



    public function load_product_by_id($id_cat)
    {
        $result   =   DB::table($this->table)
            ->select('products.title_vn','products.img_avatar1','products.code','products.content_vn','products.created_at','products.id_cat','products.price_mb','products.price_mt','products.price_mn',$this->table.'.id as id_category', $this->table.'.title_vn as cat_name')
            ->join('products', 'products.id_cat', '=', $this->table.'.id')
            ->where('products.id_cat_parent', '=', $id_cat)
            ->where('products.active', '=', '1')
            ->limit(8)
            ->get()->toArray();
        return $result;
    }

    public function loadCatLimit($limit)
    {
        $result   =   DB::table($this->table)
            ->select($this->table.'.id', $this->table.'.title_vn',$this->table.'.code',$this->table.'.active',$this->table.'.banner_cat')
            ->where($this->table.'.active', '=', '1')
            ->where($this->table.'.parent_id', '=', '0')
            ->limit($limit)
            ->get()->toArray();
        return $result;
    }
}
