<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroupModel extends Model
{
    protected $table    =   'group';
    public $timestamps  =   true;

    public function User()
    {
        return $this->hasMany('App\UserModel', 'group_id', 'id');
    }

    public function init_data($action = false)
    {
        return array(
            array(
                'name' => 'title',
                'label' => 'Tên Nhóm quản trị',
                'orderable' => '',
                'width' => '10%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
            array(
                'name'  => 'creator',
                'label' => 'Thời gian tạo',
                'orderable' => '',
                'width' => '10%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
            array(
                'name'  => 'editor',
                'label' => 'Thời gian sửa',
                'orderable' => '',
                'width' => '10%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
            array(
                'name'  => 'active',
                'label' => 'Kích hoạt',
                'orderable' => '',
                'width' => '10%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'select',
                )
            ),
            array(
                'name'  => 'action',
                'label' => ('<div class="btn-group">
                        <a style="width: 82px;" href="'.url('admin/user/add').'" class="btn btn btn-success">'.Lang::get('actionbtn.btnadd').'</a>
                        </div>'),
                'orderable' => 'false',
                'width' => '5%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
        );
    }

    public function getData($action=array())
    {
        //active
        $active=$this->active_list;

        $result = DB::table($this->table)
            ->select($this->table.'.name as title',$this->table.'.active',$this->table.'.updated_at as editor',$this->table.'.created_at as creator',$this->table.'.id');

        return Datatables::of($result)

            ->addColumn('action', function($user) use ($action) {
                return '<div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">'.Lang::get('actionbtn.btnfunction').'
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>                       
                             <i class="fa fa-pencil fa-fw" style="margin-left:10px"></i> <a href="'.url('admin/usergroup/edit/'.$user->id).'" style="    display: inline-block;width: 75%;">'.Lang::get('actionbtn.btnedit').'</a>
                       </li>
                        <li>
                           <i class="fa fa-trash-o  fa-fw" style="margin-left:10px"></i><a onclick="return xacnhanxoa(\'Bạn có muốn xóa không ?\')" href="'.url('admin/usergroup/delete/'.$user->id).'" style="display: inline-block;width: 75%;">'.Lang::get('actionbtn.btndel').'</a>
                        </li>
                    </ul>
                  </div>';
            })

            ->editColumn('active', function ($result) use ($active) {
                return $active[$result->active];
            })
            //cách search $key với $value khác nhau
            ->filterColumn('active', function($query,$keyword) use ($active){
                foreach($active as $key=>$value){
                    if($value==$keyword){
                        $query->where($this->table.'.active', '=', $key);
                        break;
                    }
                }
            })
            ->filterColumn('updated_at', function($query,$keyword){ // có ni search người sửa
                $query->where($this->table.'.updated_at', 'like', '%'.$keyword.'%');
            })

            ->filterColumn('created_at', function($query,$keyword){ // có ni search người sửa
                $query->where($this->table.'.created_at', 'like', '%'.$keyword.'%');
            })
            ->make(true);
    }
}
