<?php
/**
*viết hàm trong app/Libraries/helper.php rất hay xong thêm vào composer.json 
*    "files" : [
*            "app/Libraries/Functions.php",
*            "app/Libraries/Helper.php"
*        ]
*    xong gọi được hàm đó mọi nơi
*	 chạy lệnh composer dump-autoload cập nhật lại 
*/


/**
* hello
*
* @param string
*/
if (! function_exists('hello')) {

 	function hello(){

    	echo 'hello111';
    	
    }
}

/**
* Sửa lại url có đuôi .html
*
* @param string
*/
if (! function_exists('site_url')) {

    function site_url($value)
    {
         return asset($value);
    }
}

/**
* Select category 
*
* @param string
*/
if (! function_exists('cat_parent')) {

    function cat_parent($data,$parent=0,$str="&#187;",$select=0){
        foreach($data as $key=>$val){
            $id     =   $val['id'];
            $name   =   $val["title_vn"];
            if($val['parent_id'] == $parent){
                if($select!=0 && $id==$select){
                    echo "<option value='$id' selected='selected'>$str $name</option>";
                }else{
                    echo "<option value='$id'>$str $name</option>";
                }
                cat_parent($data,$id,'&nbsp;'.$str."&#187;",$select);
            }
        }
    }
}

?>
