<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App;
use Lang;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class LocaleController extends Controller
{
    public function changeLocale(Request $request)
    {
        
        if($request->ajax()){
            $request->session()->put('locale',$request->input('locale'));
        }
        $data = array(
            'locale' => $request->input('locale') ,
        );
        echo json_encode($data);
    }

    public function formChangeLocle(Request $request)
    {
        $data_locale = $request->input('btn_locale');
        $request->session()->put('locale',$data_locale);
        return redirect('/');
    }
}
