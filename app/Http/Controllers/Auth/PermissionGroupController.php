<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BackendController;
use App\PermissionGroupModel;
use App\UserGroupModel;
use App\PermissionModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionGroupController extends BackendController
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new PermissionGroupModel);
    }
    //--------------------------------------------------|
    //-----------  GET LIST TABLE PERMISSION  ----------|
    //--------------------------------------------------|
    public function getList()
    {
        $group                  =   UserGroupModel::all()->toArray();
        $permissiongroup        =    new PermissionGroupModel();
        $list                   =   $permissiongroup->permission_role();
        $array=array();
        if (!empty($permissiongroup)){
            foreach ($group as $listGroup){
                $data=array();
                foreach ($list as $item){
                    if ($listGroup['id'] == $item->group_id ){
                        $data[]   =   array(
                            'perrmission_group_id'  =>  $item->perrmission_group_id,
                            'group_name'            =>  $item->group_name,
                            'permission_name'       =>  $item->permission_name,
                            'slug'                  =>  $item->slug,
                            'group_id'              =>  $item->group_id,
                            'permission_id'         =>  $item->permission_id,
                            'action_view'           =>  $item->action_view,
                            'action_add'            =>  $item->action_add,
                            'action_edit'           =>  $item->action_edit,
                            'action_delete'         =>  $item->action_delete
                        );
                    }
                }
                $array[]   =   array(
                    "id"            =>      $listGroup['id'],
                    "name"          =>      $listGroup['title'],
                    "permission"    =>      $data,
                    "created_at"    =>      $listGroup['created_at'],
                    "updated_at"    =>      $listGroup['updated_at']
                );
            }
        }
        return view('auth.permission_group.list',compact('array'));
    }
    //--------------------------------------------------|
    //-----------  Return Data and save if submit  ----------|
    //--------------------------------------------------|
    public function edit(Request $request, $id=null)
    {
        if ($request->isMethod('post')){
            $this->validate($request,
                [
                    "group_name"        =>  'required|max:100|min:3',
                    "group_content"     =>  'required',
                ],
                [
                    "group_name.required"       =>  'Tên nhóm không được để trống',
                    "group_name.max"            =>  'độ dài tối da của tên nhóm không quá 100 kí tự',
                    "group_name.min"            =>  'dộ dài tối thiểu của tên nhóm không được dưới 3 kí tự',
                    "group_content.required"    =>  'Tên nhóm không được để trống',
                ]
            );

            if (!empty($request->toArray())){
                $id_user    =   Auth::user()->id;
                if($id>0){
                    $group              =   UserGroupModel::find($id);
                    $group->updator_id  =   $id_user;
                }else{
                    $group              =   new UserGroupModel;
                    $group->creator_id  =   $id_user;
                }
                $group->title           =   $request->group_name;
                $group->content         =   $request->group_content;
                $group->active          =   $request->active;
                $group->save();
                foreach($request->toArray() as $val){
                    if(is_array($val)){
                        if($id>0){
                            $PermissionGroupModel   = new PermissionGroupModel();
                            $check                  = $PermissionGroupModel->checkPermissionRole($val['permission_id'],$id);
                            $permission_group       = PermissionGroupModel::find($check->id);
                        }else{
                            $permission_group       = new PermissionGroupModel;
                            if(isset($group->id)){
                                $permission_group->group_id         =   $group->id;
                            }
                            $permission_group->permission_id        =  $val['permission_id'];
                        }
                        $permission_group->action_view      =   isset($val['view'])?1:0;
                        $permission_group->action_add       =   isset($val['add'])?1:0;
                        $permission_group->action_edit      =   isset($val['edit'])?1:0;
                        $permission_group->action_delete    =   isset($val['delete'])?1:0;
                        $permission_group->save();
                    }
                }

                if($id>0){
                    return redirect('admin/permissiongroup/list')->with(['flash_level'=>'success','flash_messages'=>'Sửa nhóm quyền thành công !!!']);
                }else{
                    return redirect('admin/permissiongroup/list')->with(['flash_level'=>'success','flash_messages'=>'Thêm nhóm quyền thành công !!!']);
                }
            }
        }
        if ($id > 0){
            $group  =   UserGroupModel::where("id", '=',  $id)->get()->first();
            if (!empty($group)){
                $group_name         =   $group->title;
                $group_id           =   $group->id;
                $group_content      =   $group->content;
                $permissiongroup    =   new PermissionGroupModel();
                $data               =   $permissiongroup->role_permision_checkid($id);
            }
            else{
                return redirect('admin/permissiongroup/list')->with(['flash_level'=>'danger','flash_messages'=>'Lỗi không có nhóm quyền']);
            }
        }else{
            $data = PermissionModel::select('id as permission_id','title as permission_name','slug','content','created_at','updated_at')->orderBy('title', 'asc')->get();
        }
        return view('auth.permission_group.update',compact('data','permissionRole','list','group_name','group_id', 'group_content'));
    }
}
