<?php

namespace App\Http\Controllers\Auth;

use App\TagsModel;
use App\Http\Controllers\BackendController;
use Validator;
use Illuminate\Routing\Redirector;
use App\Libraries\Functions;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Lang;

class TagsController extends BackendController
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new TagsModel());
    }

    public function edit(Request $requests, $id = null)
    {
        if($requests->isMethod('post')){
            $rules  =   [
                'title_vn'  =>  'required',
            ];
            $messages   =   [
                'title_vn.required' =>  'Trường bắt buộc phải điền',
            ];
            $validator  =   Validator::make($requests->all(), $rules, $messages);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }else{
                $function   = new Functions();
                $id_user    =   Auth::user()->id;
                if($id > 0){
                    $tags                =   TagsModel::find($id);
                    $tags->updator_id    =   $id_user;
                }else{
                    $tags                =   new TagsModel();
                    $tags->creator_id    =   $id_user;
                }
                $tags->title_vn          =   $requests->input('title_vn');
                $tags->code              =   $function->changeTitle($requests->input('title_vn'));

                $tags->save();
                if($id > 0){
                    return redirect('admin/tags/list')->with(['flash_level'=>'success', 'flash_message'=>'Chỉnh sửa tags thành công']);
                }else{
                    return redirect('admin/tags/list')->with(['flash_level'=> 'success', 'flash_message'=>'Thêm tags thành công']);
                }
            }
        };
        if($id > 0){
            $tags =   TagsModel::find($id);
            if(isset($tags)){
                $data['tags'] =   $tags;
            }else{
                return redirect('admin/tags/list')->with(['flash_level'=>'danger', 'flash_message'=>'tags không tồn tại']);
            }
        }else{
            $data['tags'] =   null;
        }
        return view('auth.tags.update', compact('data'));
    }
}
