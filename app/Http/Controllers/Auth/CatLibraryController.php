<?php

namespace App\Http\Controllers\Auth;

use App\CatLibraryModel;
use App\Libraries\Functions;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\BackendController;
use Validator;
use Illuminate\Routing\Redirector;
use Gate;
use Auth;
use Lang;

class CatLibraryController extends BackendController
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new CatLibraryModel);
    }
    public function edit(Request $requests, $id = null)
    {
        if($requests->isMethod('post')){
            $rules  =   [
                'title_vn'      =>  'required',
                'content_vn'    =>  'required',
            ];
            $messages   =   [
                'title_vn.required'     =>  'Trường bắt buộc phải điền',
                'content_vn.required'   =>  'Trường bắt buộc phải điền',
            ];
            $validator  =   Validator::make($requests->all(), $rules, $messages);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }else{
                $function   = new Functions();
                $id_user    =   Auth::user()->id;
                if($id > 0){
                    $catlib                =   CatLibraryModel::find($id);
                    $catlib->updator_id    =   $id_user;
                }else{
                    $catlib                =   new CatLibraryModel();
                    $catlib->creator_id    =   $id_user;
                }
                $catlib->active            =   $requests->input('active');
                $catlib->title_vn          =   $requests->input('title_vn');
                $catlib->content_vn        =   $requests->input('content_vn');
                $catlib->code              =   $function->changeTitle($requests->input('title_vn'));
                $catlib->save();
                if($id > 0){
                    return redirect('admin/cat_library/list')->with(['flash_level'=>'success', 'flash_messages'=>'Chỉnh sửa danh mục thành công']);
                }else{
                    return redirect('admin/cat_library/list')->with(['flash_level'=> 'success', 'flash_messages'=>'Thêm danh mục thành công']);
                }
            }
        };

        if($id > 0){
            $catlib = CatLibraryModel::find($id);
            if(isset($catlib)){
                $data['catlib']    =   $catlib;
            }else{
                return redirect('admin/cat_library/list')->with(['flash_level'=>'danger', 'flash_messages'=>'Danh mục này không tồn tại']);
            }
        }else{
            $data['catlib']    =   null;
        }
        return view('auth.catlibrary.update', compact('data'));

    }
}
