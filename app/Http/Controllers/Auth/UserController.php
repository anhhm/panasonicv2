<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Libraries\Functions;
use App\UserModel;
use App\UserGroupModel;

class UserController extends BackendController
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new UserModel);
    }
    public function edit(Request $request, $id=null)
    {
        if ($request->isMethod('post')){
            if ($id>0){
                $user   =   UserModel::find($id);
            }else{
                $user   =   new UserModel();
            }

            $user->name     =   $request->input('name');
            $user->email    =   $request->input('email');
            $user->password =   bcrypt($request->input('pass'));
            $user->active   =   $request->input('active');
            $user->group_id =   $request->input('id_group');
            $user->save();
            if ($id>0){
                return redirect('admin/users/list')->with(['flash_level'=>'success', 'flash_messages'=>'chỉnh sửa thông tin thành công']);
            }else{
                return redirect('admin/users/list')->with(['flash_level'=>'success', 'flash_messages'=>'Thêm thông tin quản trị thành công']);
            }
        }
        if($id>0){
            $user = UserModel::find($id);

            if(!empty($user)){

                $data['user']=$user;
            }else{
                return redirect('admin/users/list')->with(['flash_level'=>'danger','flash_messages'=>'Lỗi không có user !!']);
            }
        }else{
            $data['user']=null;
        }

        $data['group_user']=UserGroupModel::all()->toArray();
        return view('auth.user.update',compact('data'));
    }
}
