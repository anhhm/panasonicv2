<?php

namespace App\Http\Controllers\Auth;

use App\LibraryModel;
use App\CatLibraryModel;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\BackendController;
use Validator;
use Illuminate\Routing\Redirector;
use App\Libraries\Functions;
use Auth;
class LibraryController extends BackendController
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new LibraryModel);
    }

    public function edit(Request $request, $id=null)
    {
        if ($request->isMethod('post'))
        {
            $rules  =   [
                'title_vn'     =>  'required',
            ];
            $messages   =   [
                'title_vn.required'        =>  'Trường bắt buộc phải điền',
            ];

            $validator  =   Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }else{
                $Functions          = new Functions();
                $id_user            =   Auth::user()->id;
                if($id>0){
                    $library               =   LibraryModel::find($id);
                    $library->updator_id   =   $id_user;
                }else{
                    $library               =   new LibraryModel;
                    $library->creator_id   =   $id_user;
                }

                $library->active       =   $request->input('active');
                $library->id_cat       =   $request->input('id_cat');
                $library->img_avatar   =   $request->input('avatar_project');
                if ($library->img_avatar == ''){
                    $library->img_avatar   =   site_url('uploads/images/no-img.jpg');
                }
                if ($library->list_img == ''){
                    $library->list_img   =   site_url('uploads/images/no-img.jpg');
                }
                $library->title_vn    =   $request->input('title_vn');
                $library->link        =   $request->input('link');
                $library->save();
                if($id>0){
                    return redirect('admin/library/list')->with(['flash_level'=>'success','flash_messages'=>'Cập nhật thư viện thành công !!!']);
                }else{
                    return redirect('admin/library/list')->with(['flash_level'=>'success','flash_messages'=>'Thêm Thư viện thành công !!!']);
                }
            }
        }
        if($id>0){
            $library = LibraryModel::find($id);
            if(!empty($library)){
                $data['library']=$library;
            }else{
                return redirect('admin/library/list')->with(['flash_level'=>'danger','flash_messages'=>'Lỗi không có Ảnh !!']);
            }
        }else{
            $data['library']=null;
        }

        $data['cat_lib']   =   CatLibraryModel::all();
        return view('auth.library.update',compact('data'));
    }

    public function postImages(Request $request)
    {
        if ($request->ajax()) {
            if ($request->hasFile('file')) {
                $imageFiles = $request->file('file');
                // set destination path
                $folderDir = 'uploads/library';
                $destinationPath = base_path() . '/' . $folderDir;
                // this form uploads multiple files
                foreach ($request->file('file') as $fileKey => $fileObject ) {
                    // make sure each file is valid
                    if ($fileObject->isValid()) {
                        // make destination file name
                        $destinationFileName = time() . $fileObject->getClientOriginalName();
                        // move the file from tmp to the destination path
                        $fileObject->move($destinationPath, $destinationFileName);
                        // save the the destination filename
                        $library = new LibraryModel;
                        $library->list_img = $folderDir . $destinationFileName;
                    }
                }
            }
        }
    }
}
