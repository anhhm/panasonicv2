<?php

namespace App\Http\Controllers\Auth;

use App\ProductModel;
use App\CatProductModel;
use App\TagsModel;
use App\ProducerModel;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use Validator;
use Illuminate\Routing\Redirector;
use App\Libraries\Functions;
use Auth;

class ProductController extends BackendController
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new ProductModel);
    }

    public function edit(Request $request, $id=null)
    {
        if ($request->isMethod('post'))
        {
            $rules  =   [
                'title_vn'      =>  'required',
                'content_vn'    =>  'required',
                'price_mb'      =>  'required',
                'price_mt'      =>  'required',
                'price_mn'      =>  'required',
            ];
            $messages   =   [
                'title_vn.required'        =>  'Trường bắt buộc phải điền',
                'content_vn.required'      =>  'Trường bắt buộc phải điền',
                'price_mb.required'        =>  'Trường bắt buộc phải điền',
                'price_mt.required'        =>  'Trường bắt buộc phải điền',
                'price_mn.required'        =>  'Trường bắt buộc phải điền',
            ];

            $validator  =   Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }else{
                $Functions          = new Functions();
                $id_user            =   Auth::user()->id;
                if($id>0){
                    $product               =   ProductModel::find($id);
                    $product->updator_id   =   $id_user;
                }else{
                    $product               =   new ProductModel;
                    $product->creator_id   =   $id_user;
                }
                $product->title_vn          =   $request->input('title_vn');
                $product->model             =   $request->input('model');
                $product->id_cat            =   $request->input('id_cat_childen');
                $product->id_cat_parent     =   $request->input('id_cat_parent');
                $product->content_vn        =   $request->input('content_vn');
                $product->detail_vn         =   $request->input('detail_vn');
                $product->active            =   $request->input('active');
                $product->img_avatar1       =   $request->input('avatar_project');

                $product->vat               =   $request->input('vat');
                $product->price_mb          =   $request->input('price_mb');
                $product->price_mt          =   $request->input('price_mt');
                $product->price_mn          =   $request->input('price_mn');
                $product->guarantee         =   $request->input('guarantee');
                $product->status            =   $request->input('status');
                $product->id_producer       =   $request->input('producer');

                $product->code              =   $Functions->changeTitle($request->input('title_vn'));
                $product->metakeyword       =   $request->input('metakeyword');
                $product->metadescription   =   $request->input('metadescription');
                $tags                       =   $request->input('tags');
                $product->tag               =   json_encode($tags);
                if ($product->img_avatar1 == ''){
                    $product->img_avatar1   =   site_url('uploads/images/no-img.jpg');
                }

                if($request->input('filenames') != null){
                    foreach($request->input('filenames') as $file){
                        $listimg[]    =   $file;  
                    }
                }else{
                    $listimg[]        =   '';
                }
                $product->list_img      =   json_encode($listimg);

                $product->save();
                if($id>0){
                    return redirect('admin/products/list')->with(['flash_level'=>'success','flash_messages'=>'Cập nhật thành công !!!']);
                }else{
                    return redirect('admin/products/list')->with(['flash_level'=>'success','flash_messages'=>'Thêm sản phẩm thành công !!!']);
                }
            }
        }
        if($id>0){
            $product    =   ProductModel::find($id);
            if(!empty($product)){
                $data['product']    =   $product;
            }else{
                return redirect('admin/products/list')->with(['flash_level'=>'danger','flash_messages'=>'Lỗi không có sản phẩm này !!']);
            }
        }else{
            $data['product']    =   null;
        }

        $listtags           =   json_decode($data['product']['tag']);
        if (isset($listtags)) {
            foreach ($listtags as $key => $value) {
                $key                    =   TagsModel::select('id', 'title_vn')->where('id', '=', $value)->get()->toArray();
                $data['listtag'][]      =   $key[0];
            }
        }
        if (!empty($data['product']['list_img'])) {
            $data['listimg']    =   json_decode($data['product']['list_img']);
        }
        $data['tags']                   =   TagsModel::all()->toArray();
        $data['producer']               =   ProducerModel::all()->toArray();
        $data['cat_product']            =   CatProductModel::select('id', 'title_vn')->where('parent_id', '=', '0')->where('active', '=', '1')->get()->toArray();
        // $data['cat_product_child']      =   CatProductModel::select('id', 'title_vn')->where('parent_id', '>', '0')->where('active', '=', '1')->get()->toArray();
        $data['cat_product_child']      =   CatProductModel::select('id','title_vn','parent_id')->get()->toArray();

        $data['Functions']              =   new Functions();
        // echo "<pre>";
        // print_r($data['cat_product_child']);
        // die();
        return view('auth.products.update',compact('data'));
    }
}
