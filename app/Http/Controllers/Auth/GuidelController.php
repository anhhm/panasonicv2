<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use Auth;
use App\UserModel;
use Illuminate\Support\Facades\View;

class GuidelController extends BackendController
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    public function News()
    {
        $user=new UserModel();
        $menu_group=$user->menu_group(Auth::user()->id);
        //share toan view
        View::share('menu_group', $menu_group);
        return view('auth.guide.newsguide');
    }
    public function Library()
    {
        $user=new UserModel();
        $menu_group=$user->menu_group(Auth::user()->id);
        //share toan view
        View::share('menu_group', $menu_group);
        return view('auth.guide.libraryguide');
    }
    public function Product()
    {
        $user=new UserModel();
        $menu_group=$user->menu_group(Auth::user()->id);
        //share toan view
        View::share('menu_group', $menu_group);
        return view('auth.guide.productguide');
    }
    public function Admin()
    {
        $user=new UserModel();
        $menu_group=$user->menu_group(Auth::user()->id);
        //share toan view
        View::share('menu_group', $menu_group);
        return view('auth.guide.adminguide');
    }
    public function Config()
    {
        $user=new UserModel();
        $menu_group=$user->menu_group(Auth::user()->id);
        //share toan view
        View::share('menu_group', $menu_group);
        return view('auth.guide.configguide');
    }
}
