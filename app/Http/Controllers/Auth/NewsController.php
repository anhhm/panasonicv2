<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\BackendController;
use App\NewsModel;
use App\TagsModel;
use App\CatNewsModel;
use Validator;
use Illuminate\Routing\Redirector;
use Gate;
use App\Libraries\Functions;
use Auth;

class NewsController extends BackendController
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new NewsModel);
    }

    public function edit(Request $request, $id=null)
    {
        if ($request->isMethod('post'))
        {
            $rules  =   [
                'title_vn'     =>  'required',
                'content_vn'   =>  'required',
                'detail_vn'    =>  'required',
            ];
            $messages   =   [
                'title_vn.required'        =>  'Trường bắt buộc phải điền',
                'content_vn.required'      =>  'Trường bắt buộc phải điền',
                'detail_vn.required'       =>  'Trường bắt buộc phải điền',
            ];

            $validator  =   Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }else{
                $Functions          = new Functions();
                $id_user            =   Auth::user()->id;
                if($id>0){
                    $news               =   NewsModel::find($id);
                    $news->updator_id   =   $id_user;
                }else{
                    $news               =   new NewsModel;
                    $news->creator_id   =   $id_user;
                }
                $news->active       =   $request->input('active');
                $news->id_cat       =   $request->input('id_cat');
                $news->img_avatar   =   $request->input('avatar_project');
                if ($news->img_avatar == ''){
                    $news->img_avatar   =   'http://localhost/company/panasonicv2/public/shop/images/no-img.jpg';
                }
                $news->title_vn             =   $request->input('title_vn');
                $news->code                 =   $Functions->changeTitle($request->input('title_vn'));
                $news->content_vn           =   $request->input('content_vn');
                $news->detail_vn            =   $request->input('detail_vn');
                $news->metakeyword          =   $request->input('metakeyword');
                $news->metadescription      =   $request->input('metadescription');

                $news->save();
                if($id>0){
                    return redirect('admin/news/list')->with(['flash_level'=>'success','flash_messages'=>'Cập nhật tin tức thành công !!!']);
                }else{
                    return redirect('admin/news/list')->with(['flash_level'=>'success','flash_messages'=>'Thêm tin tức thành công !!!']);
                }
            }
        }
        if($id>0){
            $news = NewsModel::find($id);
            if(!empty($news)){
                $data['news']=$news;
            }else{
                return redirect('admin/news/list')->with(['flash_level'=>'danger','flash_messages'=>'Lỗi không có news !!']);
            }
        }else{
            $data['news']=null;
        }
        $data['tags']       =   TagsModel::all()->toArray();
        $data['cat_news']   =   CatNewsModel::all()->toArray();
        $data['parent']     =   CatNewsModel::select('id','title_vn','parent_id')->get()->toArray();
        $data['Functions']  =   new Functions();

        return view('auth.news.update',compact('data'));
    }
}
