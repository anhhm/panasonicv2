<?php

namespace App\Http\Controllers\Auth;

use App\CatNewsModel;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\BackendController;
use App\Libraries\Functions;
use Validator;
use Illuminate\Routing\Redirector;
use Gate;
use Auth;

class CatNewsController extends BackendController
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new CatNewsModel);
    }

    public function edit(Request $requests, $id = null)
    {
        if($requests->isMethod('post')){
            $rules  =   [
                'title_vn'  =>  'required',
            ];
            $messages   =   [
                'title_vn.required' =>  'Trường bắt buộc phải điền',
            ];
            $validator  =   Validator::make($requests->all(), $rules, $messages);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }else{
                $function   = new Functions();
                $id_user    =   Auth::user()->id;
                if($id > 0){
                    $catnews                =   CatNewsModel::find($id);
                    $catnews->updator_id    =   $id_user;
                }else{
                    $catnews                =   new CatNewsModel();
                    $catnews->creator_id    =   $id_user;
                }
                $catnews->parent_id         =   $requests->input('id_cat');
                $catnews->active            =   $requests->input('active');
                $catnews->title_vn          =   $requests->input('title_vn');
                $catnews->content_vn        =   $requests->input('content_vn');
                $catnews->code              =   $function->changeTitle($requests->input('title_vn'));
                $catnews->save();
                if($id > 0){
                    return redirect('admin/cat_news/list')->with(['flash_level'=>'success', 'flash_messages'=>'Sửa danh mục thành công']);
                }else{
                    return redirect('admin/cat_news/list')->with(['flash_level'=> 'success', 'flash_messages'=>'Thêm danh mục thành công']);
                }
            }
        };

        if($id > 0){
            $catnews = CatNewsModel::find($id);
            if(isset($catnews)){
                $data['catnews']    =   $catnews;
            }else{
                return redirect('admin/cat_news/list')->with(['flash_level'=>'danger', 'flash_messages'=>'Danh mục không tồn tại']);
            }
        }else{
            $data['catnews']    =   null;
        }
        $data['cat_news']    =   CatNewsModel::all()->toArray();
        $data['parent']     =   CatNewsModel::select('id', 'title_vn', 'parent_id')->get()->toArray();
        $data['function']   =   new Functions();
        return view('auth.catnews.update', compact('data'));
    }
}
