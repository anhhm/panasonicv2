<?php

namespace App\Http\Controllers\Auth;

use App\CatProductModel;
use App\Http\Controllers\BackendController;
use Validator;
use Illuminate\Routing\Redirector;
use App\Libraries\Functions;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Lang;

class CatProductController extends BackendController
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new CatProductModel());
    }

    public function edit(Request $requests, $id = null)
    {
        if($requests->isMethod('post')){
            $rules  =   [
                'title_vn'  =>  'required',
            ];
            $messages   =   [
                'title_vn.required' =>  'Trường bắt buộc phải điền',
            ];
            $validator  =   Validator::make($requests->all(), $rules, $messages);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }else{
                $function   =   new Functions();
                $id_user    =   Auth::user()->id;
                if($id > 0){
                    $catproduct                =   CatProductModel::find($id);
                    $catproduct->updator_id    =   $id_user;
                }else{
                    $catproduct                =   new CatProductModel();
                    $catproduct->creator_id    =   $id_user;
                }
                $catproduct->active            =   $requests->input('active');
                $catproduct->title_vn          =   $requests->input('title_vn');
                $catproduct->content_vn        =   $requests->input('content_vn');
                $catproduct->priority          =   $requests->input('priority');
                $catproduct->code              =   $function->changeTitle($requests->input('title_vn'));
                $catproduct->parent_id         =   $requests->input('id_cat');
                $catproduct->banner_cat        =   $requests->input('banner_cat');
                if ($catproduct->banner_cat == ''){
                    $catproduct->banner_cat   =   site_url('uploads/images/no-img.jpg');
                }

                $catproduct->save();
                if($id > 0){
                    return redirect('admin/cat_products/list')->with(['flash_level'=>'success', 'flash_message'=>'Chỉnh sửa danh mục thành công']);
                }else{
                    return redirect('admin/cat_products/list')->with(['flash_level'=> 'success', 'flash_message'=>'Thêm danh mục thành công']);
                }
            }
        };
        if($id > 0){
            $catproduct =   CatProductModel::find($id);
            if(isset($catproduct)){
                $data['catproduct'] =   $catproduct;
            }else{
                return redirect('admin/cat_products/list')->with(['flash_level'=>'danger', 'flash_message'=>'Danh mục không tồn tại']);
            }
        }else{
            $data['catproduct'] =   null;
        }
        $data['cat_products']   =   CatProductModel::all()->toArray();
        $data['parent']         =   CatProductModel::select('id', 'title_vn', 'parent_id')->get()->toArray();
        $data['function']       =   new Functions();
        return view('auth.catproduct.update', compact('data'));
    }
}
