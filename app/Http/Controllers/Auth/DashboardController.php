<?php

namespace App\Http\Controllers\Auth;

use App\UserModel;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use Khill\Lavacharts\Lavacharts;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;


class DashboardController extends BackendController
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getindex()
    {
        //quyền menu
        $user=new UserModel();
        $menu_group=$user->menu_group(Auth::user()->id);

        //share toan view
        View::share('menu_group', $menu_group);

        $data['total_news'] = DB::table('news')->count();
        $data['total_user'] = DB::table('users')->count();

        $data['statistical_news'] =   DB::table('news')
            ->select((DB::raw('count(*) as count, cat_news.title_vn')))
            ->leftjoin('cat_news', 'news.id_cat', '=', 'cat_news.id')
            ->groupBy('cat_news.title_vn')
            ->get()->toArray();

        $data['statistical_products_day'] =   DB::table('news')
            ->select(DB::raw('count(id) as `data`'),DB::raw("CONCAT_WS('-',MONTH(created_at),YEAR(created_at)) as monthyear"))
            ->groupby('monthyear')
            ->get()->toArray();


        return view('Auth.home.home', compact('data'));
    }
}
