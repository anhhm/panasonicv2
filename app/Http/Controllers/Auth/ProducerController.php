<?php

namespace App\Http\Controllers\Auth;

use App\ProducerModel;
use App\Http\Controllers\BackendController;
use Validator;
use Illuminate\Routing\Redirector;
use App\Libraries\Functions;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Lang;

class ProducerController extends BackendController
{
    public function __construct(Redirector $redirect)
    {
        $this->middleware('auth');
        $this->get_slug(request()->segment(2));
        $this->get_table(new ProducerModel());
    }

    public function edit(Request $requests, $id = null)
    {
        if($requests->isMethod('post')){
            $rules  =   [
                'title'  =>  'required',
            ];
            $messages   =   [
                'title.required' =>  'Trường bắt buộc phải điền',
            ];
            $validator  =   Validator::make($requests->all(), $rules, $messages);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }else{
                $function   = new Functions();
                $id_user    =   Auth::user()->id;
                if($id > 0){
                    $producer                =   ProducerModel::find($id);
                    $producer->updator_id    =   $id_user;
                }else{
                    $producer                =   new ProducerModel();
                    $producer->creator_id    =   $id_user;
                }
                $producer->active            =   $requests->input('active');
                $producer->title             =   $requests->input('title');
                $producer->content           =   $requests->input('content');
                $producer->code              =   $function->changeTitle($requests->input('title'));

                $producer->save();
                if($id > 0){
                    return redirect('admin/producer/list')->with(['flash_level'=>'success', 'flash_message'=>'Chỉnh sửa nhà sản xuất thành công']);
                }else{
                    return redirect('admin/producer/list')->with(['flash_level'=> 'success', 'flash_message'=>'Thêm nhà sản xuất thành công']);
                }
            }
        };
        if($id > 0){
            $producer =   ProducerModel::find($id);
            if(isset($producer)){
                $data['producer'] =   $producer;
            }else{
                return redirect('admin/producer/list')->with(['flash_level'=>'danger', 'flash_message'=>'nhà sản xuất không tồn tại']);
            }
        }else{
            $data['producer'] =   null;
        };
        return view('auth.producer.update', compact('data'));
    }
}
