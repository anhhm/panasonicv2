<?php

namespace App\Http\Controllers;

use DB;
use App\NewsModel;
use App\CatNewsModel;
use App\CatProductModel;
use App\ProductModel;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{

    //||--------------------------------------------------------||
    //||-------   Nội dung giới thiệu chi tiết công ty   -------||
    //||--------------------------------------------------------||
    public function index()
    {     
        $data['menu_header']        =   $this->get_view_menu_header();  
        $CatNewsModel               =   new CatNewsModel();
        $arrCatNews = $CatNewsModel->get_arrcatnews_by_parent('9');       
        $data['cat_news']           =   NewsModel::whereIn('id_cat', $arrCatNews)->where('active', '=', '1')->get();
        $data['detail']             =   NewsModel::where('code', '=', 'gioi-thieu-ve-panasonic-viet-nam')->get();
        $data['list_news_policy']   =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();
        return view('home.about.content', compact('data'));
    }

    //||--------------------------------------------------------||
    //||------                Nội dung tin tức             -----||
    //||--------------------------------------------------------||
    public function detail($code = null)
    {     
        $data['menu_header']        =   $this->get_view_menu_header();  
        $CatNewsModel               =   new CatNewsModel();
        $arrCatNews = $CatNewsModel->get_arrcatnews_by_parent('9');       
        $data['cat_news']           =   NewsModel::whereIn('id_cat', $arrCatNews)->where('active', '=', '1')->get();
        $data['detail']             =   NewsModel::where('code', '=', $code)->orderBy('created_at', 'DESC')->get();
        $data['list_news_policy']   =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();

        $data['SEO']                    =   array(
            'keyword'   =>  $data['detail']['0']->metakeyword,
            'content'   =>  $data['detail']['0']->metadescription,
            'title'     =>  $data['detail']['0']->title_vn,
        );

        return view('home.about.content', compact('data'));
    }

    //||--------------------------------------------------------||
    //||------- Load danh mục sản phẩm trên menu header   ------||
    //||--------------------------------------------------------||
    private function get_view_menu_header()
    {
        $catproductmodel            =   new CatProductModel();
        $get_cat_parent             =   $catproductmodel->get_cat_parent(5);
        if (count($get_cat_parent) > 0){
            $button = '';
            foreach ($get_cat_parent as $key => $value) {
                $button .= '<li>
                                <a href="'.site_url('product/list').'/'.$value->code.'">'.$value->title_vn.'</a>';
                $button .=          $this->get_view_submenu_header($value->id);
                $button .='</li>';
            }
            return $button;
        }
    }

    private function get_view_submenu_header($id_cat)
    {
        $catproductmodel    =   new CatProductModel();
        $submenu            =   $catproductmodel->load_SubMenu($id_cat);
        if (count($submenu) > 0) {
            $button = '';
            $button .=      '<ul class="megamenu-2 box-shadow">';
            $button .=      '<li>';
            foreach ($submenu as $key => $value) {
                $button .=          '<a class="mega-title bb" href="'.site_url('product/list').'/'.$value->code.'">'.$value->title_vn.'</a>';
            }
            $button .=      '</li>';
            $button .=      '</ul>';
            return $button;
        }
    }
}
