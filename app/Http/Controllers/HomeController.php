<?php

namespace App\Http\Controllers;
use App;
use DB;
use App\NewsModel;
use App\LibraryModel;
use App\CatProductModel;
use App\ProductModel;
use App\CatLibraryModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
class HomeController extends Controller
{
    //||--------------------------------------------------------||
    //||------     Nội dung thể hiên trên trang chủ        -----||
    //||--------------------------------------------------------||
    public function index(Request $request)
    {
    	$data['list_menu']  		=   $this->get_view_menu();
        $data['menu_header']        =   $this->get_view_menu_header();
    	$catproductmodel 			= 	new CatProductModel();
    	$CatLibraryModel  			=	new CatLibraryModel();

    	$data['banner_slide']  		=   $CatLibraryModel->GetImgOfList(4,3);
    	$data['logo_slide']  		=   $CatLibraryModel->GetImgOfList(5,8);
    	$data['single_Banner']  	=   $CatLibraryModel->GetImgOfList(3,3);
        $data['banner_left']        =   $CatLibraryModel->GetImgOfList(6,1);
        
        $data['list_news']  		=   NewsModel::orderBy('created_at', 'DESC')->limit(5)->get();
        $data['list_news_policy']   =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'DESC')->limit(4)->get();
        $data['parent_cat']			=	$catproductmodel->get_cat_parent(5); 

        $productmodel               =   new ProductModel();
        $data['treotuong']          =   $productmodel->load_product_of_cat_parent(3,8);
        $data['amtran']             =   $productmodel->load_product_of_cat_parent(16,8);
        $data['tudung']             =   $productmodel->load_product_of_cat_parent(22,8);
        $data['trungtam']           =   $productmodel->load_product_of_cat_parent(19,8);

        $data['listProductByCategory']          =   $this->getCatProduct();
        
        return view('home.home.index', compact('data'));
    }

    //||--------------------------------------------------------||
    //||------   Load danh mục sản phẩm trên menu sidebar   -----||
    //||--------------------------------------------------------||
    private function get_view_menu()
    {
    	$catproductmodel 			= 	new CatProductModel();
        $get_cat_parent 			= 	$catproductmodel->get_cat_parent(5);
        if (count($get_cat_parent) > 0){
            $button = '';
            foreach ($get_cat_parent as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name_cat =    str_limit($value->title_vn,30);
                        break;
                    case 'dn':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    case 'hcm':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    default:
                        $name_cat =    str_limit($value->title_vn,30);
                        break;
                }
                $button .= '<li class="treeview">
								<a href="'.site_url('product/list-parent').'/'.$value->code.'">
									<span>'.$name_cat.'</span>';
        		$button .= 			'<span class="pull-right-container">
	                                    <i class="fa fa-angle-left pull-right"></i>
	                                </span>
	                            </a>';
                $button .=		$this->get_view_submenu($value->id);
                $button .= 	'</li>';
            }
            return $button;
        }
    }

    private function get_view_submenu($id_cat)
    {
		$catproductmodel 	= 	new CatProductModel();
        $submenu 			= 	$catproductmodel->load_SubMenu($id_cat);
        if (count($submenu) > 0) {
            $button = '<ul class="treeview-menu">';
	        foreach ($submenu as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name =    str_limit($value->title_vn,70);
                        break;
                    case 'dn':
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),70);
                        break;
                    case 'hcm':
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),70);
                        break;
                    default:
                        $name =    str_limit($value->title_vn,70);
                        break;
                }
                $button .=      '<ul class="treeview-menu">';
	        	$button .= 		'<li>';
	            $button .= 			'<a style="font-weight: bold;" href="'.site_url('product/list').'/'.$value->code.'">'.$name.'</a>';
	            $button .= 		'</li>';
                $button .=      '</ul>';
	        }
            $button .= 	'</ul>';
            return $button;
        }
    }

    //||--------------------------------------------------------||
    //||------        Load sản phẩm cùng 1 danh mục        -----||
    //||--------------------------------------------------------||
    private function load_product_of_cat($id)
    {
    	$catproductmodel 	= 	new CatProductModel();
    	$productmodel 		= 	new ProductModel();
    	$submenu 			= 	$catproductmodel->load_SubMenu($id);
    	$product_list		=	array();
        if (isset($submenu)) {
            foreach ($submenu as $value) {
                $product_list[] =   $productmodel->load_product_of_cat($value->id,4);
            }
        }else{
            $product_list[]     =   $productmodel->load_product_of_cat($id,4);
        }

    	return $product_list;
    }

    //||--------------------------------------------------------||
    //||------  Load danh mục sản phẩm trên menu Header   -----||
    //||--------------------------------------------------------||

    private function get_view_menu_header()
    {
        $catproductmodel            =   new CatProductModel();
        $get_cat_parent             =   $catproductmodel->get_cat_parent(5);
        if (count($get_cat_parent) > 0){
            $button = '';
            foreach ($get_cat_parent as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name_cat =    str_limit($value->title_vn,30);
                        break;
                    case 'dn':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    case 'hcm':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    default:
                        $name_cat =    str_limit($value->title_vn,30);
                        break;
                }
                $button .= '<li>
                                <a href="'.site_url('product/list-parent').'/'.$value->code.'">'.$name_cat.'</a>';
                $button .=          $this->get_view_submenu_header($value->id);
                $button .='</li>';
            }
            return $button;
        }
    }

    private function get_view_submenu_header($id_cat)
    {
        $catproductmodel    =   new CatProductModel();
        $submenu            =   $catproductmodel->load_SubMenu($id_cat);
        if (count($submenu) > 0) {
            $button = '';
            $button .=      '<ul class="megamenu-2 box-shadow">';
            $button .=      '<li>';            
            foreach ($submenu as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name =    str_limit($value->title_vn,30);
                        break;
                    case 'dn':
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    case 'hcm':
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    default:
                        $name =    str_limit($value->title_vn,30);
                        break;
                }
                $button .=          '<a class="mega-title bb" href="'.site_url('product/list').'/'.$value->code.'">'.$name.'</a>';
            }
            $button .=      '</li>';
            $button .=      '</ul>';
            return $button;
        }
    }

    //||--------------------------------------------------------||
    //||------      LOAD 4 DANH MỤC SẢN PHẨM MỚI NHẤT      -----||
    //||--------------------------------------------------------||
    private function getCatProduct()
    {
        $CatProductModel           =   new CatProductModel();      
        $listCat                   =   $CatProductModel->loadCatLimit(4);
        if (count($listCat) > 0){
            $button = '';
            foreach ($listCat as $key => $item) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name_cat =    str_limit($item->title_vn,30);
                        break;
                    case 'dn':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),30);
                        break;
                    case 'hcm':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),30);
                        break;
                    default:
                        $name_cat =    str_limit($item->title_vn,30);
                        break;
                }
                $button .=  '<div class="product box-shadow bg-fff">
                                <div class="product-title bg-1 text-uppercase">
                                    <i class="fa fa-paper-plane-o icon bg-4"></i>
                                    <a href="'.site_url('product/list-parent').'/'.$item->code.'"><h3>'.$name_cat.'</h3></a>
                                </div>
                                <div class="row left left-right-angle">';
                $button .=              $this->getProductBatCat($item->id);
                $button .=      '</div>
                            </div>
                            <div class="banner-area mtb-15">
                                <div class="single-banner">
                                    <a href="#">
                                        <img style="max-height: 190px;" src="'.$item->banner_cat.'" alt="">
                                    </a>
                                </div>
                            </div>';
            }
            return $button;
        }
    }

    //||--------------------------------------------------------||
    //||------        LOAD SẢN PHẨM THEO DANH MỤC          -----||
    //||--------------------------------------------------------||
    private function getProductBatCat($id_cat)
    {
        $CatProductModel    =   new CatProductModel();
        $listProduct        =   $CatProductModel->load_product_by_id($id_cat);
        
        if (count($listProduct) > 0) {
            $button =   '';
            foreach ($listProduct as $key => $item) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $price =    $item->price_mb;
                        $name =    str_limit($item->title_vn,50);
                        break;
                    case 'dn':
                        $price =    $item->price_mt;
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),50);
                        break;
                    case 'hcm':
                        $price =    $item->price_mn;
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),50);
                        break;
                    default:
                        $price =    0;
                        $name =    str_limit($item->title_vn,50);
                        break;
                }
                $button .=      '<div class="product-items col-md-3 col-sm-6">
                                    <div class="product-wrapper bb bl">
                                        <div class="product-img" style="min-height: 105px;">
                                            <a href="'.site_url('product').'/'.$item->code .'">
                                                <img src="'.$item->img_avatar1.'" alt="" class="primary">
                                                <img src="'.$item->img_avatar1.'" alt="" class="secondary">
                                            </a>
                                            <div class="product-icon c-fff hover-bg">
                                                <ul>
                                                    <li><a href="'.site_url('product').'/'.$item->code.'" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                    <li><a href="'.site_url('product').'/'.$item->code.'" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <h3><a href="'.site_url('product').'/'.$item->code.'">'.$name.'</a></h3>
                                            <ul>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                            <span>'. 
                                            number_format($price,0,".",",")
                                            .' VND</span>
                                        </div>
                                    </div>
                                </div>';
            }
            return $button;
        }
    }
}
