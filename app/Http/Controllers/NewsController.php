<?php

namespace App\Http\Controllers;

use DB;
use App\NewsModel;
use App\CatNewsModel;
use App\CatProductModel;
use App\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NewsController extends Controller
{

    //||--------------------------------------------------------||
    //||------           Danh sách các tin tức             -----||
    //||--------------------------------------------------------||
    public function index()
    {
        $data['menu_header']        =   $this->get_view_menu_header();
        $data['cat_news']           =   CatNewsModel::where('active', '=', '1')->orderBy('created_at', 'DESC')->get();
        $CatNewsModel               =   new CatNewsModel();
        $data['list_news']          =   $CatNewsModel->load_all_news_by_active('1',6);
        $data['list_news_policy']   =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'DESC')->limit(4)->get();
        return view('home.news.list', compact('data'));
    }

    //||--------------------------------------------------------||
    //||------      Hiển thi tin tức theo danh mục         -----||
    //||--------------------------------------------------------||
    public function list($code = null)
    {
        $data['menu_header']        =   $this->get_view_menu_header();
        $data['cat_news']           =   CatNewsModel::where('active', '=', '1')->orderBy('created_at', 'DESC')->get();
        $CatNewsModel               =   new CatNewsModel();
        $data['list_news']          =   $CatNewsModel->load_news_by_code($code,6);
        $data['list_news_policy']   =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();
        return view('home.news.list', compact('data'));
    }

    //||--------------------------------------------------------||
    //||------                Nội dung tin tức             -----||
    //||--------------------------------------------------------||
    public function detail($code = null)
    {     
        $data['menu_header']        =   $this->get_view_menu_header();  
        $data['cat_news']           =   CatNewsModel::where('active', '=', '1')->orderBy('created_at', 'DESC')->get();
        $data['detail']             =   NewsModel::where('code', '=', $code)->orderBy('created_at', 'DESC')->get();
        $data['list_news_policy']   =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();

        $data['SEO']                    =   array(
            'keyword'   =>  $data['detail']['0']->metakeyword,
            'content'   =>  $data['detail']['0']->metadescription,
            'title'     =>  $data['detail']['0']->title_vn,
        );

        return view('home.news.content', compact('data'));
    }

    //||--------------------------------------------------------||
    //||------  Load danh mục sản phẩm trên menu Header   -----||
    //||--------------------------------------------------------||

    private function get_view_menu_header()
    {
        $catproductmodel            =   new CatProductModel();
        $get_cat_parent             =   $catproductmodel->get_cat_parent(5);
        if (count($get_cat_parent) > 0){
            $button = '';
            foreach ($get_cat_parent as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name_cat =    str_limit($value->title_vn,30);
                        break;
                    case 'dn':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    case 'hcm':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    default:
                        $name_cat =    str_limit($value->title_vn,30);
                        break;
                }
                $button .= '<li>
                                <a href="'.site_url('product/list-parent').'/'.$value->code.'">'.$name_cat.'</a>';
                $button .=          $this->get_view_submenu_header($value->id);
                $button .='</li>';
            }
            return $button;
        }
    }

    private function get_view_submenu_header($id_cat)
    {
        $catproductmodel    =   new CatProductModel();
        $submenu            =   $catproductmodel->load_SubMenu($id_cat);
        if (count($submenu) > 0) {
            $button = '';
            $button .=      '<ul class="megamenu-2 box-shadow">';
            $button .=      '<li>';            
            foreach ($submenu as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name =    str_limit($value->title_vn,30);
                        break;
                    case 'dn':
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    case 'hcm':
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    default:
                        $name =    str_limit($value->title_vn,30);
                        break;
                }
                $button .=          '<a class="mega-title bb" href="'.site_url('product/list').'/'.$value->code.'">'.$name.'</a>';
            }
            $button .=      '</li>';
            $button .=      '</ul>';
            return $button;
        }
    }
}
