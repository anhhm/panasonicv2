<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CatProductModel;
use App\NewsModel;

class ContactController extends Controller
{

    //||--------------------------------------------------------||
    //||-------    Hiển thị thông tin liên lạc công ty   -------||
    //||--------------------------------------------------------||
    public function index()
    {
    	$data['menu_header']        =   $this->get_view_menu_header();
        $data['list_news_policy']   =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();
        return view('home.contact.index', compact('data'));
    }


    //||--------------------------------------------------------||
    //||------   Load danh mục sản phẩm trên menu header   -----||
    //||--------------------------------------------------------||
    private function get_view_menu_header()
    {
        $catproductmodel            =   new CatProductModel();
        $get_cat_parent             =   $catproductmodel->get_cat_parent(5);
        if (count($get_cat_parent) > 0){
            $button = '';
            foreach ($get_cat_parent as $key => $value) {
                $button .= '<li>
                                <a href="'.site_url('product/list-parent').'/'.$value->code.'">'.$value->title_vn.'</a>';
                $button .=          $this->get_view_submenu_header($value->id);
                $button .='</li>';
            }
            return $button;
        }
    }

    private function get_view_submenu_header($id_cat)
    {
        $catproductmodel    =   new CatProductModel();
        $submenu            =   $catproductmodel->load_SubMenu($id_cat);
        if (count($submenu) > 0) {
            $button = '';
            $button .=      '<ul class="megamenu-2 box-shadow">';
            $button .=      '<li>';
            foreach ($submenu as $key => $value) {
                $button .=          '<a class="mega-title bb" href="'.site_url('product/list').'/'.$value->code.'">'.$value->title_vn.'</a>';
            }
            $button .=      '</li>';
            $button .=      '</ul>';
            return $button;
        }
    }
}
