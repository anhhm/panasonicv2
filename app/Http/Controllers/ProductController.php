<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductModel;
use App\CatProductModel;
use App\TagsModel;
use App\NewsModel;
use DB;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    //||--------------------------------------------------------||
    //||--------          Danh sách sản phẩm            --------||
    //||--------------------------------------------------------||
    public function index()
    {
        $data['menu_header']            =   $this->get_view_menu_header();
        $data['list_product']           =   ProductModel::where('active', '=', 1)->paginate(16);
        $data['list_product_new']       =   ProductModel::orderBy('created_at', 'ASC')->limit(5)->get();
        $data['list_news_policy']       =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();
        $data['list_menu']              =   $this->get_view_menu();
        return view('home.product.list', compact('data'));
    }

    //||--------------------------------------------------------||
    //||--------  Danh sách sản phẩm theo phân loại     --------||
    //||--------------------------------------------------------||
    public function list($code = null)
    {
        $data['menu_header']            =   $this->get_view_menu_header();
        $CatProductModel                =   new CatProductModel();
        $data['list_product']           =   $CatProductModel->load_product_by_code($code);
        $data['list_menu']              =   $this->get_view_menu();
        $data['list_product_new']       =   ProductModel::orderBy('created_at', 'ASC')->limit(5)->get();
        $data['list_news_policy']       =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();
        return view('home.product.list', compact('data'));
    }

    //||--------------------------------------------------------||
    //||-------  Danh sách sản phẩm theo danh mục cha   --------||
    //||--------------------------------------------------------||
    public function list_parent($code = null)
    {
        $data['menu_header']            =   $this->get_view_menu_header();
        $ProductModel                   =   new ProductModel();
        $data['list_product']           =   $ProductModel->check_product($code);
        $data['list_menu']              =   $this->get_view_menu();
        $data['list_product_new']       =   ProductModel::orderBy('created_at', 'ASC')->limit(5)->get();
        $data['list_news_policy']       =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();
        return view('home.product.list', compact('data'));
    }


    //||--------------------------------------------------------||
    //||--------      Nội dung chi tiết sản phẩm        --------||
    //||--------------------------------------------------------||
    public function detail($code = null)
    {
        $data['list_product_new']       =   ProductModel::orderBy('created_at', 'ASC')->limit(5)->get();
        $data['list_menu']              =   $this->get_view_menu();
        $data['menu_header']            =   $this->get_view_menu_header();
        $ProductModel                   =   new ProductModel();
        $data['detail']                 =   $ProductModel->load_detail_product($code);
        $data['list_news_policy']       =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();

        if (isset($data['detail']['0']->title_vn)) {
            $data['SEO']                    =   array(
                'keyword'   =>  $data['detail']['0']->metakeyword,
                'content'   =>  $data['detail']['0']->metadescription,
                'title'     =>  $data['detail']['0']->title_vn,
            );
        
            
            
            $listtags           =   json_decode($data['detail']['0']->tag);
            if (isset($listtags)) {
                foreach ($listtags as $key => $value) {
                    $key                    =   TagsModel::select('id', 'title_vn','code')->where('id', '=', $value)->get()->toArray();
                    $data['listtag'][]      =   $key[0];
                }
            }
            
            $data['list_product_of_cat'] =  ProductModel::select('id', 'title_vn','code','img_avatar1', 'price_mb', 'price_mt', 'price_mn')->where('id_cat', '=', $data['detail']['0']->id_cat)->orderBy('created_at', 'ASC')->limit(5)->get();
        }else{
            return redirect('product');
        }
        
        return view('home.product.detail', compact('data'));
    }


    //||--------------------------------------------------------||
    //||--------   Danh sách sản phẩm theo tìm kiếm     --------||
    //||--------------------------------------------------------||
    public function search(Request $request)
    {
        $data['menu_header']            =   $this->get_view_menu_header();
        $product                        =   $request->get('product');
        $data['list_menu']              =   $this->get_view_menu();
        $data['list_product_new']       =   ProductModel::orderBy('created_at', 'ASC')->limit(5)->get();
        $data['list_product']           =   DB::table('products')->where('title_vn', 'like', '%'.$product.'%')->paginate(16);
        $data['list_news_policy']       =   NewsModel::where('id_cat', '=', 10)->orderBy('created_at', 'ASC')->limit(4)->get();
        return view('home.product.search', compact('data'));
    }

    //||--------------------------------------------------------||
    //||--------     Danh sách sản phẩm theo Tag        --------||
    //||--------------------------------------------------------||
    public function tag(Request $request, $code = null)
    {
        $data['menu_header']            =   $this->get_view_menu_header();
        $product                        =   $request->get('product');
        $data['list_menu']              =   $this->get_view_menu();
        $data['list_product_new']       =   ProductModel::orderBy('created_at', 'ASC')->limit(5)->get();
        $tagModel                       =   new TagsModel();
        $idTags                         =   $tagModel->getIdByCode($code);
        $productModel                   =   new ProductModel();
        $data['list_product']           =   $productModel->load_Product_By_IdTag($idTags[0]->id);
        return view('home.product.search', compact('data'));
    }


    //||--------------------------------------------------------||
    //||-----  Load danh mục sản phẩm trên menu sidebar --------||
    //||--------------------------------------------------------||
    private function get_view_menu()
    {
        $catproductmodel            =   new CatProductModel();
        $get_cat_parent             =   $catproductmodel->get_cat_parent(5);
        if (count($get_cat_parent) > 0){
            $button = '';
            foreach ($get_cat_parent as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name_cat =    str_limit($value->title_vn,25);
                        break;
                    case 'dn':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),25);
                        break;
                    case 'hcm':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),25);
                        break;
                    default:
                        $name_cat =    str_limit($value->title_vn,25);
                        break;
                }
                $button .= '<li>
                                <a title="'.$value->title_vn.'" href="'.site_url('product/list-parent').'/'.$value->code.'">'.$name_cat.'</a>';
                $button .=      '<span class="opener-'.$key.' fa fa-plus pull-right"></span>';
                $button .=      '<ul class="toggle-'.$key.'">';
                $button .=          $this->get_view_submenu($value->id);
                $button .=      '</ul>
                            </li>';
            }
            return $button;
        }
    }

    private function get_view_submenu($id_cat)
    {
        $catproductmodel    =   new CatProductModel();
        $submenu            =   $catproductmodel->load_SubMenu($id_cat);
        if (count($submenu) > 0) {
            $button = '';
            foreach ($submenu as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name_cat =    str_limit($value->title_vn,25);
                        break;
                    case 'dn':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),25);
                        break;
                    case 'hcm':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),25);
                        break;
                    default:
                        $name_cat =    str_limit($value->title_vn,25);
                        break;
                }
                $button .=      '<li>';
                $button .=          '<a href="'.site_url('product/list').'/'.$value->code.'" title="'.$value->title_vn.'">'.$name_cat.'</a>';
                $button .=      '</li>';
            }
            return $button;
        }
    }


    //||--------------------------------------------------------||
    //||------  Load danh mục sản phẩm trên menu Header   -----||
    //||--------------------------------------------------------||

    private function get_view_menu_header()
    {
        $catproductmodel            =   new CatProductModel();
        $get_cat_parent             =   $catproductmodel->get_cat_parent(5);
        if (count($get_cat_parent) > 0){
            $button = '';
            foreach ($get_cat_parent as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name_cat =    str_limit($value->title_vn,30);
                        break;
                    case 'dn':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    case 'hcm':
                        $name_cat =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    default:
                        $name_cat =    str_limit($value->title_vn,30);
                        break;
                }
                $button .= '<li>
                                <a href="'.site_url('product/list-parent').'/'.$value->code.'">'.$name_cat.'</a>';
                $button .=          $this->get_view_submenu_header($value->id);
                $button .='</li>';
            }
            return $button;
        }
    }

    private function get_view_submenu_header($id_cat)
    {
        $catproductmodel    =   new CatProductModel();
        $submenu            =   $catproductmodel->load_SubMenu($id_cat);
        if (count($submenu) > 0) {
            $button = '';
            $button .=      '<ul class="megamenu-2 box-shadow">';
            $button .=      '<li>';            
            foreach ($submenu as $key => $value) {
                switch (Session::get('locale')) {
                    case 'hn':
                        $name =    str_limit($value->title_vn,30);
                        break;
                    case 'dn':
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    case 'hcm':
                        $name =    str_limit(str_replace('Điều hòa', 'Máy lạnh', $value->title_vn),30);
                        break;
                    default:
                        $name =    str_limit($value->title_vn,30);
                        break;
                }
                $button .=          '<a class="mega-title bb" href="'.site_url('product/list').'/'.$value->code.'">'.$name.'</a>';
            }
            $button .=      '</li>';
            $button .=      '</ul>';
            return $button;
        }
    }
}
