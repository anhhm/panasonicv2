<?php

namespace App\Http\Controllers;

use App\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Requests;
use Illuminate\Routing\Redirector;
use DB;
use Auth;
use Gate;

class BackendController extends Controller
{
    private $slug;
    private $table;
    private $controller;

    public function __construct($redirect)
    {
        $this->middleware('auth');
    }

    public function index(Request $request,$name=null,$id=null)
    {
        //quyền menu
        $user=new UserModel();
        $menu_group=$user->menu_group(Auth::user()->id);

        //share toan view
        View::share('menu_group', $menu_group);

        if(empty(Auth::check()) || !$this->checkAccess(request()->segment(3),request()->segment(2)) ){
            return redirect('login');
        }  
        switch($name) {
            case 'edit':
                return $this->edit($request,$id);
                break;
            case 'add':
                return $this->edit($request);
                break;
            case 'delete':
                return  $this->getdelete($id);
                break;
            case 'list':
                return $this->getList();
                break;
            case 'data':
                return $this->getData();
                break;
            default:
                return $this->getList();
        }
    }

    //Lấy Danh sách
    public function getList()
    {
        $init_data=$this->table->init_data($this->checkAccess('action_add',$this->slug));
        $slug=$this->slug;
        return view('Auth.page.table',compact('data','init_data','slug'));
    }

    //Lấy trang
    public function get_slug($slug){
        $this->slug = $slug;
    }

    //Lấy Table
    public function get_table($table){
        $this->table = $table;
    }

    //Lấy dữ liệu
    public function getData()
    {
        $data=$this->table->getData();
        return $data;
    }

    //Xóa id
    public function getdelete($id)
    {
        if($id>0){
            $check = DB::table($this->slug)->where('id', $id)->count();
            if($check!=0){
                DB::table($this->slug)->delete($id);
                return redirect('admin/'.$this->slug.'/list')->with(['flash_level'=>'success','flash_messages'=>'Đã xóa thành công !!!']);
            }else{
                return redirect('admin/'.$this->slug.'/list')->with(['flash_level'=>'danger','flash_messages'=>'Lỗi không xóa được !!']);
            }
        }
    }

    //kiểm quyền tra từng action trang
    public function set_controller($controller)
    {
        $this->controller = $controller;
    }

    //kiểm quyền tra từng action trang
    public function checkAccess($action,$slug){
        if($action=='list'){
            $action='action_view';
        }elseif($action=='add'){
            $action='action_add';
        }elseif($action=='edit'){
            $action='action_edit';
        }elseif($action=='delete'){
            $action='action_delete';
        }elseif($action==null){
            $action='action_view';
        }elseif($action=='data'){
            return true;
        }
        $group=Auth::user()->group_id;

        // echo "<pre>";
        // print_r($group);
        // die();
        if (Gate::denies($action, [$group,$slug])) {
            // abort(403,'Không hiển thị được');
            return false;
        }
        return true;
    }

    // kiểm tra quyền tất cả của trang
    public function checkAccess_all($slug){
        $result=array();
        $group=Auth::user()->group_id;
        if (Gate::denies('action_view', [$group,$slug])) {
            // abort(403,'Không hiển thị được');
            $result['action_view']=false;
        }else{
            $result['action_view']=true;
        }

        if (Gate::denies('action_edit', [$group,$slug])) {
            $result['action_edit']=false;
        }else{
            $result['action_edit']=true;
        }

        if (Gate::denies('action_delete', [$group,$slug])) {
            // abort(403,'Không hiển thị được');
            $result['action_delete']=false;
        }else{
            $result['action_delete']=true;
        }
        return $result;
    }
}
