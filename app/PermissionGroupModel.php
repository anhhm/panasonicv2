<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PermissionGroupModel extends Model
{
    protected $table    =   'permission_group';

    public function permission_role()
    {
        $select =   [
            $this->table.".id as perrmission_group_id", $this->table.".group_id", $this->table.".permission_id",
            $this->table.".action_view", $this->table.".action_edit", $this->table.".action_delete", $this->table.".action_add",
            "group.title as group_name", "permission.title as permission_name","permission.slug"
        ];
        $permission_group   =   DB::table($this->table)
            ->select($select)
            ->leftJoin('group', 'group.id',  '=', $this->table.'.group_id' )
            ->leftJoin('permission', 'permission.id', '=', $this->table.'.permission_id')
            ->orderBy('permission.id', 'ASC')
            ->get();
            return $permission_group;
    }

    public function role_permision_checkid($id)
    {
        $select =
            [$this->table.".id as permission_role_id",$this->table.'.group_id',$this->table.".permission_id",
                $this->table.".action_view",$this->table.".action_edit",$this->table.".action_add",$this->table.".action_delete",
                "group.title as group_name", "permission.title as permission_name","permission.slug"
            ];
        $result = DB::table($this->table)
            ->select($select)
            ->leftJoin('group', 'group.id',  '=', $this->table.'.group_id' )
            ->leftJoin('permission', 'permission.id', '=', $this->table.'.permission_id')
            ->where('group.id','=',$id)
            ->orderBy('permission.title', 'ASC')
            ->get();
        return $result;
    }

    public function checkPermissionRole($permission_id,$group_id)
    {
        $result = DB::table($this->table)
            ->where('permission_id','=',$permission_id)
            ->where('group_id','=',$group_id)
            ->first();
        return $result;
    }
}
