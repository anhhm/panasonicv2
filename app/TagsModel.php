<?php

namespace App;

use DB;
use Yajra\Datatables\Datatables;
use Lang;
class TagsModel extends MyModel
{
    protected $table    =   'tags';
    public $timestamps  =   true;


    public function init_data($action=false)
    {
        return array(
            array(
                'name'          =>  'title',
                'label'         =>  'Tag',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'creator',
                'label'         =>  'Người tạo',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'editor',
                'label'         =>  'Người sửa',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'action',
                'label'         =>  ('<div class="btn-group">
                                         <a style="width: 82px;" href="'.url('admin/tags/add').'" class="btn btn btn-success">Thêm mới</a>
                                    </div>'),
                'orderable'     =>  'false',
                'width'         =>  '5%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
        );
    }

    public function getData($action=array())
    {
        //active
        $result = DB::table($this->table)
            ->leftjoin('users as users_creator', 'users_creator.id', '=', $this->table.'.creator_id')
            ->leftjoin('users as users_editor', 'users_editor.id', '=', $this->table.'.updator_id')
            ->select($this->table.'.title_vn as title',$this->table.'.created_at',$this->table.'.id','users_creator.name as creator','users_editor.name as editor');

        return Datatables::of($result)

            ->addColumn('action', function($user) use ($action) {
                return '<div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Chức năng
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>                       
                             <i class="fa fa-pencil fa-fw" style="margin-left:10px"></i> <a href="'.url('admin/tags/edit/'.$user->id).'" style="    display: inline-block;width: 75%;">Chỉnh sửa</a>
                       </li>
                    </ul>
                  </div>';
            })
            ->filterColumn('title', function($query,$keyword){
                $query->where($this->table.'.title_vn', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('creator', function($query,$keyword){
                $query->where('users_creator.name', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('editor', function($query,$keyword){
                $query->where('users_editor.name', 'like', '%'.$keyword.'%');
            })

            ->make(true);
    }


    public function load_product_by_code($code)
    {
        $result   =   DB::table($this->table)
            ->select('products.title_vn','products.img_avatar1','products.code','products.content_vn','products.created_at','products.id_cat','products.price_mb','products.price_mt','products.price_mn',$this->table.'.id')
            ->join('products', 'products.id_cat', '=', $this->table.'.id')
            ->where($this->table.'.code', '=', $code)
            ->paginate(16);

        return $result;
    }

    public function getIdByCode($code)
    {
        $result = DB::table($this->table)
                ->select($this->table.'.id',$this->table.'.code',$this->table.'.title_vn')
                ->where($this->table.'.code', '=',$code)
                ->get()->toArray();
        return $result;
    }
}
