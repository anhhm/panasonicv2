<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Yajra\Datatables\Datatables;
use Lang;//language

class UserModel extends Model
{
    protected $table    =   "users";
    public $timestamps  =   true;
    public function groupuser()
    {
        return $this->belongsTo('App/UserGroupModel', 'group_id', 'id');
    }
    public function init_data($action = false)
    {
        return array(
            array(
                'name' => 'name',
                'label' => 'Tên admin',
                'orderable' => '',
                'width' => '10%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
            array(
                'name' => 'group_name',
                'label' => 'Nhóm quyền',
                'orderable' => '',
                'width' => '10%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'select',
                )
            ),
            array(
                'name'  => 'created_at',
                'label' => 'Ngày tạo',
                'orderable' => '',
                'width' => '10%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
            array(
                'name'  => 'updated_at',
                'label' => 'Ngày cập nhật',
                'orderable' => '',
                'width' => '10%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
            array(
                'name'  => 'action',
                'label' => ('<div class="btn-group">
                        <a style="width: 82px;" href="'.url('admin/users/add').'" class="btn btn btn-success">Thêm mới</a>
                        </div>'),
                'orderable' => 'false',
                'width' => '5%',
                'sort'  => FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
        );
    }

    public function getData($action=array())
    {
        //active
        $active=$this->active_list;

        $result = DB::table($this->table)
            ->leftjoin('group', $this->table.'.group_id', '=', 'group.id')
            ->select($this->table.'.name',$this->table.'.active',$this->table.'.updated_at',$this->table.'.created_at',$this->table.'.id','group.title as group_name');

        return Datatables::of($result)

            ->addColumn('action', function($user) use ($action) {
                return '<div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Chức năng
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>                       
                             <i class="fa fa-pencil fa-fw" style="margin-left:10px"></i> <a href="'.url('admin/users/edit/'.$user->id).'" style="    display: inline-block;width: 75%;">Chỉnh sửa</a>
                       </li>
                        <li>
                           <i class="fa fa-trash-o  fa-fw" style="margin-left:10px"></i><a onclick="return xacnhanxoa(\'Bạn có muốn xóa không ?\')" href="'.url('admin/users/delete/'.$user->id).'" style="display: inline-block;width: 75%;">Xóa</a>
                        </li>
                    </ul>
                  </div>';
            })

            ->editColumn('active', function ($result) use ($active) {
                return $active[$result->active];
            })
            //cách search $key với $value khác nhau
            ->filterColumn('active', function($query,$keyword) use ($active){
                foreach($active as $key=>$value){
                    if($value==$keyword){
                        $query->where($this->table.'.active', '=', $key);
                        break;
                    }
                }
            })
            ->filterColumn('name', function($query,$keyword){ // có ni search người sửa
                $query->where($this->table.'.name', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('group_name', function($query,$keyword){ // có ni search người sửa
                $query->where('group.title', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('updated_at', function($query,$keyword){ // có ni search người sửa
                $query->where($this->table.'.updated_at', 'like', '%'.$keyword.'%');
            })

            ->filterColumn('created_at', function($query,$keyword){ // có ni search người sửa
                $query->where($this->table.'.created_at', 'like', '%'.$keyword.'%');
            })
            ->make(true);
    }

    public function menu_group($user_id){
        if(isset($user_id) && !empty($user_id)){
            $menu_group = User::select('permission_group.action_view','permission_group.action_edit','permission_group.action_add','permission_group.action_delete','permission.slug')
                ->join('group', 'group.id', '=', 'users.group_id')
                ->join('permission_group', 'permission_group.group_id', '=', 'group.id')
                ->join('permission', 'permission.id', '=', 'permission_group.permission_id')
                ->where('users.id',$user_id)
                ->get()
                ->toArray();
            $result['menu_group']=array();
            foreach($menu_group as $item){
                $result['menu_group'][$item['slug']]=array(
                    'action_view'   => $item['action_view'],
                    'action_edit'   => $item['action_edit'],
                    'action_add'    => $item['action_add'],
                    'action_delete' => $item['action_delete']
                );
            }
            return $result['menu_group'];
        }
    }
}
