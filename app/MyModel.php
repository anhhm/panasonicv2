<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyModel extends Model
{
    public $active_list = array(
        '0' => 'Không',
        '1' => 'Có'
    );

    public $sex_list = array(
        '0' => 'Nam',
        '1' => 'Nữ'
    );
}
