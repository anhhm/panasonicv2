<?php

namespace App;

use DB;
use Yajra\Datatables\Datatables;
use Lang;

class ProductModel extends MyModel
{
    protected $table    =   'products';
    public $timestamps  =   true;

    public function categoryproduct()
    {
        return $this->belongsTo('App\CatProductModel','id_cat','id');
    }

    public function init_data($action=false)
    {
        return array(
            array(
                'name'          =>  'title',
                'label'         =>  'Tên sản phẩm',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'catname',
                'label'         =>  'Danh mục',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' => array(
                    'type'  => 'text',
                )
            ),
            array(
                'name'          =>  'creator',
                'label'         =>  'Người tạo',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'editor',
                'label'         =>  'Người sửa',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'action',
                'label'         =>  ('<div class="btn-group">
                                         <a style="width: 82px;" href="'.url('admin/products/add').'" class="btn btn btn-success">Thêm mới</a>
                                    </div>'),
                'orderable'     =>  'false',
                'width'         =>  '5%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
        );
    }

    public function getData($action=array())
    {
        //active
        $active=$this->active_list;

        $result = DB::table($this->table)
            ->leftjoin('cat_products', $this->table.'.id_cat', '=', 'cat_products.id')
            ->leftjoin('users as users_creator', 'users_creator.id', '=', $this->table.'.creator_id')
            ->leftjoin('users as users_editor', 'users_editor.id', '=', $this->table.'.updator_id')
            ->select($this->table.'.title_vn as title',$this->table.'.active',$this->table.'.created_at',$this->table.'.id','cat_products.title_vn as catname','users_creator.name as creator','users_editor.name as editor');
        return Datatables::of($result)
            ->addColumn('action', function($user) use ($action) {
                return '<div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Chức năng
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>                       
                             <i class="fa fa-pencil fa-fw" style="margin-left:10px"></i> <a href="'.url('admin/products/edit/'.$user->id).'" style="    display: inline-block;width: 75%;">Chỉnh sửa</a>
                       </li>
                        <li>
                           <i class="fa fa-trash-o  fa-fw" style="margin-left:10px"></i><a onclick="return xacnhanxoa(\'Bạn có muốn xóa không ?\')" href="'.url('admin/products/delete/'.$user->id).'" style="display: inline-block;width: 75%;">Xóa</a>
                        </li>
                    </ul>
                  </div>';
            })

            ->editColumn('active', function ($result) use ($active) {
                return $active[$result->active];
            })
            ->filterColumn('title', function($query,$keyword){
                $query->where($this->table.'.title_vn', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('catname', function($query,$keyword){
                $query->where('cat_products.title_vn', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('creator', function($query,$keyword){
                $query->where('users_creator.name', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('editor', function($query,$keyword){
                $query->where('users_editor.name', 'like', '%'.$keyword.'%');
            })
            //cách search $key với $value khác nhau
            ->filterColumn('active', function($query,$keyword) use ($active){
                foreach($active as $key=>$value){
                    if($value==$keyword){
                        $query->where('news.active', '=', $key);
                        break;
                    }
                }
            })
            ->make(true);
    }

    public function load_product_of_cat($id_cat, $limit)
    {
        $result   =   DB::table($this->table)
            ->select($this->table.'.title_vn',$this->table.'.img_avatar1',$this->table.'.code',$this->table.'.content_vn',$this->table.'.created_at',$this->table.'.id_cat',$this->table.'.price_mb',$this->table.'.price_mt',$this->table.'.price_mn','cat_products.id')
            ->join('cat_products', 'cat_products.id', '=', $this->table.'.id_cat')
            ->where($this->table.'.id_cat', '=', $id_cat)
            ->limit($limit)
            ->get()->toArray();
        return $result;
    }

    public function load_product_of_cat_parent($id_cat, $limit)
    {
        $result   =   DB::table($this->table)
            ->select($this->table.'.title_vn',$this->table.'.img_avatar1',$this->table.'.code',$this->table.'.content_vn',$this->table.'.created_at',$this->table.'.id_cat',$this->table.'.price_mb',$this->table.'.price_mt',$this->table.'.price_mn',$this->table.'.id_cat_parent')
            ->where($this->table.'.id_cat_parent', '=', $id_cat)
            ->limit($limit)
            ->get()->toArray();
        return $result;
    }

    public function check_product($code)
    {
        $result   =   DB::table($this->table)
            ->select($this->table.'.title_vn',$this->table.'.img_avatar1',$this->table.'.code',$this->table.'.content_vn',$this->table.'.created_at',$this->table.'.id_cat',$this->table.'.price_mb',$this->table.'.price_mt',$this->table.'.price_mn',$this->table.'.id_cat_parent')
            ->where($this->table.'.id_cat_parent', '=', $this->checkId($code))
            ->paginate(16);
        return $result;
    }

    private function checkId($code)
    {
        $result = DB::table('cat_products')
                    ->select('cat_products.id')
                    ->where('cat_products.code', '=', $code)
                    ->get();
        $result = $result[0]->id;
        return $result;
    }

    public function load_detail_product($code)
    {
        $result   =   DB::table($this->table)
            ->select($this->table.'.title_vn',$this->table.'.img_avatar1',$this->table.'.code',$this->table.'.content_vn',$this->table.'.created_at',$this->table.'.id_cat',$this->table.'.price_mb',$this->table.'.price_mt',$this->table.'.price_mn',$this->table.'.tag',$this->table.'.metadescription',$this->table.'.metakeyword',$this->table.'.model',$this->table.'.guarantee',$this->table.'.model',$this->table.'.detail_vn',$this->table.'.code',$this->table.'.status','cat_products.title_vn as namecat')
            ->join('cat_products', 'cat_products.id', '=', $this->table.'.id_cat')
            ->where($this->table.'.code', '=', $code)
            ->get()->toArray();

        return $result;
    }

    public function load_Product_By_IdTag($id)
    {
        $result =   DB::table($this->table)
            ->select($this->table.'.title_vn',$this->table.'.img_avatar1',$this->table.'.code',$this->table.'.content_vn',$this->table.'.created_at',$this->table.'.id_cat',$this->table.'.price_mb',$this->table.'.price_mt',$this->table.'.price_mn')
            ->where($this->table.'.tag', 'like', '%'.$id.'%')
            ->paginate(16);
        return $result;
    }
}
