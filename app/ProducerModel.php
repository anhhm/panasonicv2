<?php

namespace App;

use DB;
use Yajra\Datatables\Datatables;
use Lang;
class ProducerModel extends MyModel
{
    protected $table    =   'producer';
    public $timestamps  =   true;

    public function product()
    {
        return $this->hasMany('App\ProductModel', 'id_producer', 'id');
    }

    public function init_data($action=false)
    {
        return array(
            array(
                'name'          =>  'title',
                'label'         =>  'Nhà sản xuất',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'creator',
                'label'         =>  'Người tạo',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'editor',
                'label'         =>  'Người sửa',
                'orderable'     =>  '',
                'width'         =>  '10%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
            array(
                'name'          =>  'action',
                'label'         =>  ('<div class="btn-group">
                                         <a style="width: 82px;" href="'.url('admin/producer/add').'" class="btn btn btn-success">Thêm mới</a>
                                    </div>'),
                'orderable'     =>  'false',
                'width'         =>  '5%',
                'sort'          =>  FALSE,
                'searchoptions' =>  array(
                    'type'      =>  'text',
                )
            ),
        );
    }

    public function getData($action=array())
    {
        //active
        $active=$this->active_list;

        $result = DB::table($this->table)
            ->leftjoin('users as users_creator', 'users_creator.id', '=', $this->table.'.creator_id')
            ->leftjoin('users as users_editor', 'users_editor.id', '=', $this->table.'.updator_id')
            ->select($this->table.'.title',$this->table.'.active',$this->table.'.created_at',$this->table.'.id','users_creator.name as creator','users_editor.name as editor');

        return Datatables::of($result)

            ->addColumn('action', function($user) use ($action) {
                return '<div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Chức năng
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>                       
                             <i class="fa fa-pencil fa-fw" style="margin-left:10px"></i> <a href="'.url('admin/producer/edit/'.$user->id).'" style="    display: inline-block;width: 75%;">Chỉnh sửa</a>
                       </li>
                        <li>
                           <i class="fa fa-trash-o  fa-fw" style="margin-left:10px"></i><a onclick="return xacnhanxoa(\'Bạn có muốn xóa không ?\')" href="'.url('admin/producer/delete/'.$user->id).'" style="display: inline-block;width: 75%;">Xóa</a>
                        </li>
                    </ul>
                  </div>';
            })
            ->editColumn('active', function ($result) use ($active) {
                return $active[$result->active];
            })
            ->filterColumn('title', function($query,$keyword){
                $query->where($this->table.'.title_vn', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('creator', function($query,$keyword){
                $query->where('users_creator.name', 'like', '%'.$keyword.'%');
            })
            ->filterColumn('editor', function($query,$keyword){
                $query->where('users_editor.name', 'like', '%'.$keyword.'%');
            })
            //cách search $key với $value khác nhau
            ->filterColumn('active', function($query,$keyword) use ($active){
                foreach($active as $key=>$value){
                    if($value==$keyword){
                        $query->where('news.active', '=', $key);
                        break;
                    }
                }
            })

            ->make(true);
    }
}
