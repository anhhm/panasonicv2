<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\UserGroupModel;
use App\PermissionModel;
use App\PermissionGroupModel;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
            
        $gate->define('action_view', function ($user, $group, $slug) { 
            $data=UserGroupModel::find($group)
                        ->join('permission_group', 'permission_group.group_id', '=', 'group.id')
                        ->join('permission', 'permission.id', '=', 'permission_group.permission_id')
                        ->where('slug','=',$slug)
                        ->where('group.id','=',$group)
                        ->get()
                        ->first()
                        ->toArray();
            if(!empty($data) && isset($data)){
                $kq=PermissionGroupModel::select('action_view')
                        ->where('group_id', '=', $data['group_id'])
                        ->where('permission_id', '=', $data['permission_id'])
                        ->get()->first()->toArray();
                return $kq['action_view'] == 1 ? true : false;
            }
            return false;
        });

        $gate->define('action_add', function ($user, $group, $slug) { 
            $data=UserGroupModel::find($group)
                        ->join('permission_group', 'permission_group.group_id', '=', 'group.id')
                        ->join('permission', 'permission.id', '=', 'permission_group.permission_id')
                        ->where('slug','=',$slug)
                        ->where('group.id','=',$group)
                        ->get()
                        ->first()
                        ->toArray();
            if(!empty($data) && isset($data)){
                $kq=PermissionGroupModel::select('action_add')
                        ->where('group_id', '=', $data['group_id'])
                        ->where('permission_id', '=', $data['permission_id'])
                        ->get()->first()->toArray();
                return $kq['action_add'] == 1 ? true : false;
            }
            return false;
        });

        $gate->define('action_delete', function ($user, $group, $slug) { 
            $data=UserGroupModel::find($group)
                        ->join('permission_group', 'permission_group.group_id', '=', 'group.id')
                        ->join('permission', 'permission.id', '=', 'permission_group.permission_id')
                        ->where('slug','=',$slug)
                        ->where('group.id','=',$group)
                        ->get()
                        ->first()
                        ->toArray();
            if(!empty($data) && isset($data)){
                $kq=PermissionGroupModel::select('action_delete')
                        ->where('group_id', '=', $data['group_id'])
                        ->where('permission_id', '=', $data['permission_id'])
                        ->get()->first()->toArray();
                return $kq['action_delete'] == 1 ? true : false;
            }
            return false;
        });

        $gate->define('action_edit', function ($user, $group, $slug) { 
            $data=UserGroupModel::find($group)
                        ->join('permission_group', 'permission_group.group_id', '=', 'group.id')
                        ->join('permission', 'permission.id', '=', 'permission_group.permission_id')
                        ->where('slug','=',$slug)
                        ->where('group.id','=',$group)
                        ->get()
                        ->first()
                        ->toArray();
            if(!empty($data) && isset($data)){
                $kq=PermissionGroupModel::select('action_edit')
                        ->where('group_id', '=', $data['group_id'])
                        ->where('permission_id', '=', $data['permission_id'])
                        ->get()->first()->toArray();
                return $kq['action_edit'] == 1 ? true : false;
            }
            return false;
        });
    }
}
