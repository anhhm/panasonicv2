@extends('home.about.masterNews')
@section('addCss')
    <link rel="stylesheet" href="{{asset('shop')}}/css/typo.css">
@endsection
@section('contentNews')
    <div class="blog-area">
        <div class="content-box">
        	<div class="product-description">
				@if(isset($data['detail']))
					@foreach($data['detail'] as $item)
						{!! $item->detail_vn !!}
					@endforeach
				@endif
			</div>
		</div>
    </div>
@endsection