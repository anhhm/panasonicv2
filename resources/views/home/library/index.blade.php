@extends('home.master')
@section('content')
	<div class="protfolio-area shop-bg">
		<div class="container">
			<div class="protfolio-taitle mtb-30 text-center">
				<h1>Thư viện</h1>
			</div>
			<div class="protfolio-menu mtb-30 text-center">
				<ul>
					<li class="filter active" data-filter="all">All</li>
					<li class="filter" data-filter=".tosiba">Tosiba</li>
					<li class="filter" data-filter=".lg">LG</li>
				</ul>
			</div>
			<div class="protfolio clear" id="Container">
				@if(isset($data['library']))
					@for($i = 0; $i < 8; $i++)
						<div class="single-protfolio mix tosiba" style="display: inline-block;" data-bound="">
							<div class="protfolio-img">
								<img src="{{asset('shop')}}/images/maxresdefault (1).jpg" alt="">
								<div class="protfolio-content bg-fff text-center ptb-15">
									<h3><a href="">ảnh Toshiba</a></h3>
									<span>20/11/2018</span>
								</div>
								<div class="protfolio-icon">
									<a href=""><i class="fa fa-link"></i></a>
								</div>
							</div>
						</div>
					@endfor
				@endif
				@for($i = 0; $i < 8; $i++)
					<div class="single-protfolio mix lg" style="display: inline-block;" data-bound="">
						<div class="protfolio-img">
							<img src="{{asset('shop')}}/images/LG-V30.jpg" alt="">
							<div class="protfolio-content bg-fff text-center ptb-15">
								<h3><a href="">ảnh LGr</a></h3>
								<span>21/11/2018</span>
							</div>
							<div class="protfolio-icon">
								<a href=""><i class="fa fa-link"></i></a>
							</div>
						</div>
					</div>
				@endfor
			</div>
			<div class="page-numbers text-center ptb-15 mt-30 bt">
				<ul>
					<li><span>1</span></li>
					<li><a href="#">2</a></li>
					<li><a href="#"><i class="fa fa-long-arrow-right"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
@endsection