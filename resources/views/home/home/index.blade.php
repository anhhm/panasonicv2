@extends('home.master')
@section('content')
   @include('home.template.bannerslide')
    <!-- product area start -->
    <div class="product-area ptb-35">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                    <div class="tab-area box-shadow bg-fff">
                        <div class="product-title bg-1 text-uppercase">
                            <i class="fa fa-check-square-o icon bg-4"></i>
                            <!-- <h3>Danh mục sản phẩm</h3> -->
                            <div class="tab-menu floatright hidden-xs" style="width: 95%">
                                <ul>
                                    <!-- <li class="active"><a href="#treotuong" data-toggle="tab">ĐH Treo tường</a></li> -->
                                    @if(isset($data['parent_cat']))
                                        @foreach($data['parent_cat'] as $k => $item)
                                            <li class="@if( $k == 0) active @endif">
                                                <a href="#{{$k}}" data-toggle="tab">
                                                    @if (App::isLocale('hn'))
                                                       {{ $item->title_vn }}
                                                    @endif
                                                    @if (App::isLocale('dn'))
                                                       {!! str_replace('Điều hòa', 'Máy lạnh', $item->title_vn) !!}
                                                    @endif
                                                    @if (App::isLocale('hcm'))
                                                       {!! str_replace('Điều hòa', 'Máy lạnh', $item->title_vn) !!}
                                                    @endif
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            
                            <div role="tabpanel" class="tab-pane active" id="0">
                                <div class="product-active tab-active left left-right-angle">
                                    @if(isset($data['treotuong']))
                                        @foreach( $data['treotuong'] as $item)
                                                <div class="product-wrapper bl">
                                                    <div class="product-img">
                                                        <a href="{{site_url('product')}}/{{ $item->code }}">
                                                            <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                                            <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                                                        </a>
                                                        <div class="product-icon c-fff hover-bg">
                                                            <ul>
                                                                <!-- <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li> -->
                                                                <li><a href="{{site_url('product')}}/{{ $item->code }}" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <span class="sale">Sale</span>
                                                    </div>
                                                    <div class="product-content">
                                                        <h3><a href="{{site_url('product')}}/{{ $item->code }}">
                                                            @if (App::isLocale('hn'))
                                                               {{ str_limit($item->title_vn,70) }}
                                                            @endif
                                                            @if (App::isLocale('dn'))
                                                               {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),70) !!}
                                                            @endif
                                                            @if (App::isLocale('hcm'))
                                                               {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),70) !!}
                                                            @endif
                                                        </a></h3>
                                                        <ul>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                        </ul>
                                                        @if (App::isLocale('hn'))
                                                           <span>{{ number_format($item->price_mb,0,".",",") }} VND</span>
                                                        @endif
                                                        @if (App::isLocale('dn'))
                                                           <span>{{ number_format($item->price_mt,0,".",",") }} VND</span>
                                                        @endif
                                                        @if (App::isLocale('hcm'))
                                                           <span>{{ number_format($item->price_mn,0,".",",") }} VND</span>
                                                        @endif
                                                    </div>
                                                </div>
                                        @endforeach
                                    @endif
                                </div>     
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="1">
                                <div class="product-active tab-active left left-right-angle">
                                    @if(isset($data['amtran']))
                                        @foreach( $data['amtran'] as $item)
                                            <div class="product-wrapper bl">
                                                <div class="product-img">
                                                    <a href="{{site_url('product')}}/{{ $item->code }}">
                                                        <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                                        <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                                                    </a>
                                                    <div class="product-icon c-fff hover-bg">
                                                        <ul>
                                                            <!-- <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li> -->
                                                            <li><a href="{{site_url('product')}}/{{ $item->code }}" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                        </ul>
                                                    </div>
                                                    <span class="sale">Sale</span>
                                                </div>
                                                <div class="product-content">
                                                    <h3>
                                                        <a href="{{site_url('product')}}/{{ $item->code }}">
                                                            @if (App::isLocale('hn'))
                                                               {{ str_limit($item->title_vn,70) }}
                                                            @endif
                                                            @if (App::isLocale('dn'))
                                                               {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),70) !!}
                                                            @endif
                                                            @if (App::isLocale('hcm'))
                                                               {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),70) !!}
                                                            @endif
                                                        </a>
                                                    </h3>
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                    @if (App::isLocale('hn'))
                                                       <span>{{ number_format($item->price_mb,0,".",",") }} VND</span>
                                                    @endif
                                                    @if (App::isLocale('dn'))
                                                       <span>{{ number_format($item->price_mt,0,".",",") }} VND</span>
                                                    @endif
                                                    @if (App::isLocale('hcm'))
                                                       <span>{{ number_format($item->price_mn,0,".",",") }} VND</span>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    
                                </div>                                
                            </div>                        
                            <div role="tabpanel" class="tab-pane fade" id="2">
                                <div class="product-active tab-active left left-right-angle">
                                    @if(isset($data['tudung']))
                                        @foreach( $data['tudung'] as $item)
                                                <div class="product-wrapper bl">
                                                    <div class="product-img">
                                                        <a href="{{site_url('product')}}/{{ $item->code }}">
                                                            <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                                            <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                                                        </a>
                                                        <div class="product-icon c-fff hover-bg">
                                                            <ul>
                                                                <!-- <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li> -->
                                                                <li><a href="{{site_url('product')}}/{{ $item->code }}" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <span class="sale">Sale</span>
                                                    </div>
                                                    <div class="product-content">
                                                        <h3>
                                                            <a href="{{site_url('product')}}/{{ $item->code }}">
                                                                @if (App::isLocale('hn'))
                                                                   {{ str_limit($item->title_vn,70) }}
                                                                @endif
                                                                @if (App::isLocale('dn'))
                                                                   {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),70) !!}
                                                                @endif
                                                                @if (App::isLocale('hcm'))
                                                                   {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),70) !!}
                                                                @endif
                                                            </a>
                                                        </h3>
                                                        <ul>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                        </ul>
                                                        @if (App::isLocale('hn'))
                                                           <span>{{ number_format($item->price_mb,0,".",",") }} VND</span>
                                                        @endif
                                                        @if (App::isLocale('dn'))
                                                           <span>{{ number_format($item->price_mt,0,".",",") }} VND</span>
                                                        @endif
                                                        @if (App::isLocale('hcm'))
                                                           <span>{{ number_format($item->price_mn,0,".",",") }} VND</span>
                                                        @endif
                                                    </div>
                                                </div>
                                        @endforeach
                                    @endif
                                </div>     
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="3">
                                <div class="product-active tab-active left left-right-angle">
                                    @if(isset($data['trungtam']))
                                        @foreach( $data['trungtam'] as $item)
                                                <div class="product-wrapper bl">
                                                    <div class="product-img">
                                                        <a href="{{site_url('product')}}/{{ $item->code }}">
                                                            <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                                            <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                                                        </a>
                                                        <div class="product-icon c-fff hover-bg">
                                                            <ul>
                                                                <!-- <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li> -->
                                                                <li><a href="{{site_url('product')}}/{{ $item->code }}" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <span class="sale">Sale</span>
                                                    </div>
                                                    <div class="product-content">
                                                        <h3>
                                                            <a href="{{site_url('product')}}/{{ $item->code }}">
                                                                @if (App::isLocale('hn'))
                                                                   {{ str_limit($item->title_vn,70) }}
                                                                @endif
                                                                @if (App::isLocale('dn'))
                                                                   {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),70) !!}
                                                                @endif
                                                                @if (App::isLocale('hcm'))
                                                                   {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),70) !!}
                                                                @endif
                                                            </a>
                                                        </h3>
                                                        <ul>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                        </ul>
                                                        @if (App::isLocale('hn'))
                                                           <span>{{ number_format($item->price_mb,0,".",",") }} VND</span>
                                                        @endif
                                                        @if (App::isLocale('dn'))
                                                           <span>{{ number_format($item->price_mt,0,".",",") }} VND</span>
                                                        @endif
                                                        @if (App::isLocale('hcm'))
                                                           <span>{{ number_format($item->price_mn,0,".",",") }} VND</span>
                                                        @endif
                                                    </div>
                                                </div>
                                        @endforeach
                                    @endif
                                </div>     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product area end -->

    <!-- banner-area start -->
    @if(isset($data['single_Banner']))
        <div class="banner-area">
            <div class="container">
                <div class="row">
                    @foreach($data['single_Banner'] as $item)
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-12">
                            <div class="single-banner">
                                <a href="#">
                                    <img src="{{ $item->img_avatar }}" alt="{{ $item->title_vn }}">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <!-- banner-area end -->

    <!-- all-product-area start -->
    <div class="all-product-area mtb-45">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <!-- featured-area start -->
                    <div class="featured-area box-shadow bg-fff">
                        <div class="product-title bg-1 text-uppercase">
                            <i class="fa fa-star-o icon bg-4"></i>
                            <h3>{{ trans('text.machine') }} Panasonic</h3>
                        </div>
                        <div class="sidebar">
                            <ul class="sidebar-menu" data-widget="tree">
                                {!! $data['list_menu'] !!}
                            </ul>
                        </div>
                    </div>

                    <!-- blog-area start -->
                    @if(isset($data['list_news']))
                        <div class="blog-area box-shadow mtb-35 bg-fff">
                            <div class="product-title bg-1 text-uppercase">
                                <i class="fa fa-comments-o icon bg-4"></i>
                                <h3>Tin tức</h3>
                            </div>
                            <div class="feautred-active right left-right-angle">
                                @foreach ($data['list_news'] as $item)
                                    <div class="blog-wrapper">
                                        <div class="blog-img mtb-30">
                                            <a title="{{ $item->title_vn }}" href="{{site_url('news')}}/{{$item->code}}">
                                                <img src="{{$item->img_avatar}}" alt="">
                                            </a>
                                        </div>
                                        <div class="blog-content">
                                            <h3>
                                                <a title="{{ $item->title_vn }}" href="{{site_url('news')}}/{{$item->code}}">
                                                    @if (App::isLocale('hn'))
                                                       {{ str_limit($item->title_vn,30) }}
                                                    @endif
                                                    @if (App::isLocale('dn'))
                                                       {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),30) !!}
                                                    @endif
                                                    @if (App::isLocale('hcm'))
                                                       {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),30) !!}
                                                    @endif
                                                </a>
                                            </h3>
                                            <div class="blog-meta">
                                                <span>{{$item->created_at}}</span>
                                            </div>
                                            <p>
                                                {{ str_limit($item->content_vn, 70) }}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <!-- blog-area end -->

                    <!-- testimonils-area start -->
                    <div class="testimonils-area box-shadow mtb-35 bg-fff" style="margin-bottom: 0px;">
                        <div class="product-title bg-1 text-uppercase">
                            <i class="fa fa-star-o icon bg-4"></i>
                            <h3>Khách hàng</h3>
                        </div>
                        <div class="feautred-active right left-right-angle">
                                <div class="single-testimonil clear">
                                    <div class="testimonil-content p mtb-25">
                                        <p>Nói về độ làm sạch, khả năng xử lý bụi bẩn, diệt khuẩn, khử mùi thì không máy điều hòa nào bằng Aero Series - sản phẩm Nhật Bản rồi. Bạn nào dùng máy lọc không khí rồi sẽ thấy, điều hòa này cho cảm giác không khí sạch và trong lành y như vậy.</p>
                                    </div>
                                    <div class="test-img floatleft">
                                        <!-- <img src="https://devitems.com/html/oneclick-preview/oneclick/img/test/2.jpg" alt=""> -->
                                        <img style="width: 70px !important; height: 70px !important;" width="50" height="50" src="{{asset('shop')}}/images/avatar3.png" alt="">
                                    </div>
                                    <div class="test-info">
                                        <h6>Nguyễn Thị Kim Loan</h6>
                                        <span>Tp.Hà Nội</span>
                                    </div>
                                </div>
                                <div class="single-testimonil clear">
                                    <div class="testimonil-content p mtb-25">
                                        <p>Sản Phẩm tốt, Máy chạy êm, làm lạnh nhanh. Tôi rất hài lòng với phẩm của panasonic.</p>
                                    </div>
                                    <div class="test-img floatleft">
                                        <!-- <img src="https://devitems.com/html/oneclick-preview/oneclick/img/test/2.jpg" alt=""> -->
                                        <img style="width: 70px !important; height: 70px !important;" width="50" height="50" src="{{asset('shop')}}/images/avatar.png" alt="">
                                    </div>
                                    <div class="test-info">
                                        <h6>Bùi Văn Tiệp</h6>
                                        <span>Tp.Hà Nội</span>
                                    </div>
                                </div>

                                <div class="single-testimonil clear">
                                    <div class="testimonil-content p mtb-25">
                                        <p>Máy dùng tốt, nhân viên lắp đặt nhiệt tình. Mình khá hài lòng về chất lượng lẫn dịch vụ.</p>
                                    </div>
                                    <div class="test-img floatleft">
                                        <!-- <img src="https://devitems.com/html/oneclick-preview/oneclick/img/test/2.jpg" alt=""> -->
                                        <img style="width: 70px !important; height: 70px !important;" width="50" height="50" src="{{asset('shop')}}/images/avatar2.png" alt="">
                                    </div>
                                    <div class="test-info">
                                        <h6>Tâm</h6>
                                        <span>Tp.Hồ Chí Minh</span>
                                    </div>
                                </div>

                                <div class="single-testimonil clear">
                                    <div class="testimonil-content p mtb-25">
                                        <p>Máy tốt dùng ok không tốn điện lắm.đúng là tiền nào của đó.mua xài rồi mới cảm nhận thấy tất cả.</p>
                                    </div>
                                    <div class="test-img floatleft">
                                        <!-- <img src="https://devitems.com/html/oneclick-preview/oneclick/img/test/2.jpg" alt=""> -->
                                        <img style="width: 70px !important; height: 70px !important;" width="50" height="50" src="{{asset('shop')}}/images/avatar5.png" alt="">
                                    </div>
                                    <div class="test-info">
                                        <h6>Vo Van Anh</h6>
                                        <span>Tp.Nha Trang</span>
                                    </div>
                                </div>

                                <div class="single-testimonil clear">
                                    <div class="testimonil-content p mtb-25">
                                        <p>mẫu mã máy nhìn đẹp viền kim loại sang trọng, khả năng làm lạnh nhanh và cũng nhiều tính năng ưu việt. Nhìn chung sản phẩm rất ok.</p>
                                    </div>
                                    <div class="test-img floatleft">
                                        <!-- <img src="https://devitems.com/html/oneclick-preview/oneclick/img/test/2.jpg" alt=""> -->
                                        <img style="width: 70px !important; height: 70px !important;" width="50" height="50" src="{{asset('shop')}}/images/avatar04.png" alt="">
                                    </div>
                                    <div class="test-info">
                                        <h6>Nguyễn Văn Nam</h6>
                                        <span>Tp.Hải Phòng</span>
                                    </div>
                                </div>

                                <div class="single-testimonil clear">
                                    <div class="testimonil-content p mtb-25">
                                        <p>Dịch vụ bảo hành tốt, máy bền, đẹp, tiết kiệm điện vì có Inverter. Vote cho em này. Mấy anh giao hàng nhanh chóng và lắp ráp cho mình rất chuyên nghiệp.</p>
                                    </div>
                                    <div class="test-img floatleft">
                                        <!-- <img src="https://devitems.com/html/oneclick-preview/oneclick/img/test/2.jpg" alt=""> -->
                                        <img style="width: 70px !important; height: 70px !important;" width="50" height="50" src="{{asset('shop')}}/images/avatar3.png" alt="">
                                    </div>
                                    <div class="test-info">
                                        <h6>Nguyễn Thị Kim Loan</h6>
                                        <span>Tp.Đà Nẵng</span>
                                    </div>
                                </div>

                                <div class="single-testimonil clear">
                                    <div class="testimonil-content p mtb-25">
                                        <p>Cảm giác rất thoải mái, dễ chịu. Bình thường mình rất hạn chế bật điều hòa sợ khô da nhưng giờ cần thi bật cho mát, ở lì trong nhà cũng không sợ.</p>
                                    </div>
                                    <div class="test-img floatleft">
                                        <!-- <img src="https://devitems.com/html/oneclick-preview/oneclick/img/test/2.jpg" alt=""> -->
                                        <img style="width: 70px !important; height: 70px !important;" width="50" height="50" src="{{asset('shop')}}/images/avatar2.png" alt="">
                                    </div>
                                    <div class="test-info">
                                        <h6>Huỳnh Kim Ngân</h6>
                                        <span>Tp.Hồ Chí Minh</span>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!-- testimonils-area end -->
                    @if(isset($data['banner_left']))
                        <div id="sidebar_left">
                            <div id="sidebar_banner">
                                <div class="sidebar__inner">
                                    @foreach ($data['banner_left'] as $item)                                
                                        <div class="single-banner mtb-35">
                                            <a href="{{ $item->link or '' }}">
                                                <img src="{{ $item->img_avatar }}" alt="">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <!-- product-area start -->
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    {!! $data['listProductByCategory'] !!}
                </div>
                <!-- product-area end -->
            </div>
        </div>
    </div>
    <!-- all-product-area end -->

    <!-- brand-area start -->
    @if(isset($data['logo_slide']))
        <div class="brand-area mb-35">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="brand-active  box-shadow p-15  bg-fff">
                            @foreach ($data['logo_slide'] as $item)
                                <div class="single-brand">
                                    <a href="{{ $item->link or '' }}">
                                        <img src="{{ $item->img_avatar }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- brand-area end -->

@endsection

@section('addjs')
    <script type="text/javascript">
        $('.tab-menu ul > li > a').hover(function() {
            $(this).tab('show');
        });
    </script>
@endsection