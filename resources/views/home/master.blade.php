<html>
    <head>
      <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cửa hàng máy lạnh</title>

        <!-- Deny copy and right click on website for downloading images. -->
        <!-- <style>
            body{
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
            }
        </style>
        <script type=”text/JavaScript”>
            function killCopy(e){
                return false
            }
            function reEnable(){
                return true
            }
            document.onselectstart=new Function (“return false”)
            if (window.sidebar){
                document.onmousedown=killCopy
                document.onclick=reEnable
            }
        </script> -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @include('home.template.seo')
        @yield('addCss')
        @include('home.template.linkcss')
        <script>
            var baseUrl =  '{{url('')}}';
        </script>
    </head>
    <body>
        
        <div class="modal fade modal__header" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title" id="myModalLabel">Chọn khu vực mua hàng</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="width: 100%; margin: 0px auto;">
                            <form class="form-inline" method="post" action="{{ site_url('/locale') }}">
                                <input id="" type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                <div class="form-group" style="margin: 10px;">
                                    <button type="submit" class="btn btn-default ses-hn" name="btn_locale" value="hn">Miền Bắc</button>
                                </div>
                                <div class="form-group" style="margin: 10px;">
                                    <button type="submit" class="btn btn-default ses-dn" name="btn_locale" value="dn">Miền Trung</button>
                                </div>
                                <div class="form-group" style="margin: 10px;">
                                    <button type="submit" class="btn btn-default ses-hcm" name="btn_locale" value="hcm">Miền Nam</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        @include('home.template.header')

        @yield('content')
        
        @include('home.template.footer')
        
        @include('home.template.linkjs')
        @yield('addjs')

        <script>
            $("#ControlSelectLocale").change(function () {
                var locale  =   $(this).val();
                var _token  =   $("input[name=_token]").val();

                $.ajax({
                    url: baseUrl+'/locale',
                    type: 'POST',
                    dataType: 'json',
                    data: {locale: locale , _token : _token },
                    success : function(data){
                    },
                    error : function(data) {

                    },
                    complete : function(data){
                        window.location.reload(true);
                    }

                })
            })
        </script>

        <!-- Load Phone number -->
        <div class="mypage-alo-phone">
            <a href="">
                <div class="animated infinite zoomIn mypage-alo-ph-circle"></div>
                <div class="animated infinite pulse mypage-alo-ph-circle-fill"></div>
                <div class="animated infinite tada mypage-alo-ph-img-circle"></div>
            </a>
        </div>

        <script type="text/javascript">
            $('.modal-on-load').trigger('click');
            // Hide Header on on scroll down
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            var navbarHeight = $('#header').outerHeight();
            $(window).scroll(function(event){
                didScroll = true;
            });

            setInterval(function() {
                if (didScroll) {
                    didScroll = false;
                }
            }, 250);


            $(document).ready(function(){
                @if(Session::has('locale'))
                    $('#myModal').modal('hidden');
                @else
                    $('#myModal').modal('show');
                @endif
                $(window).scroll(function(){
                    var cach_top = $(window).scrollTop();
                    if(cach_top >=1){ 
                        $('.header-bottom').css({'position': 'fixed', 'top': '0px','z-index': '999','width': '100%',});
                    }else{
                        $('.header-bottom').css({'position': 'relative'});
                    }
                });
            });   
        </script>
    </body>
</html>