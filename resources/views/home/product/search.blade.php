@extends('home.product.masterProduct')
@section('contentProduct')
    <div class="tab-area">
        <div class="tab-menu-area bg-fff">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="shop-tab-menu">
                        <ul>
                            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true"><i class="fa fa-th-large"></i></a></li>
                            <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-th-list"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            @if(isset($data['list_product'][0]->title_vn)))
                <div role="tabpanel" class="tab-pane active in" id="tab1">
                    <div class="row">
                        @foreach($data['list_product'] as $item)
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                <div class="product-wrapper bg-fff mb-30" style="height: 260px;">
                                    <div class="product-img" style="height: 160px;">
                                        <a title="{{ $item->title_vn }}"  href="{{site_url('product')}}/{{ $item->code }}">
                                            <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                            <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                                        </a>
                                        <div class="product-icon c-fff hover-bg">
                                            <ul>
                                                <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                <li><a href="{{site_url('product')}}/{{ $item->code }}" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product-content">
                                        <h3><a title="{{ $item->title_vn }}" href="{{site_url('product')}}/{{ $item->code }}">
                                            @if (App::isLocale('hn'))
                                               {{ str_limit(str_replace('máy lạnh', 'Điều hòa', $item->title_vn),50) }}
                                            @endif
                                            @if (App::isLocale('dn'))
                                               {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),50) !!}
                                            @endif
                                            @if (App::isLocale('hcm'))
                                               {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),50) !!}
                                            @endif
                                        </a></h3>
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                        @if (App::isLocale('hn'))
                                           <span>{{ number_format($item->price_mb,0,".",",") }} VND</span>
                                        @endif
                                        @if (App::isLocale('dn'))
                                           <span>{{ number_format($item->price_mt,0,".",",") }} VND</span>
                                        @endif
                                        @if (App::isLocale('hcm'))
                                           <span>{{ number_format($item->price_mn,0,".",",") }} VND</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab2">
                    <div class="row">
                        @foreach($data['list_product'] as $item)
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="product-wrapper bg-fff mb-30 ptb-20">
                                    <div class="product-img shop-product-img">
                                        <a title="{{ $item->title_vn }}"  href="{{site_url('product')}}/{{ $item->code }}">
                                            <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                            <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                                        </a>
                                    </div>
                                    <div class="product-content shop-product-content">
                                        <h3><a title="{{ $item->title_vn }}" href="{{site_url('product')}}/{{ $item->code }}">
                                            @if (App::isLocale('hn'))
                                               {{ str_limit(str_replace('máy lạnh', 'Điều hòa', $item->title_vn),50) }}
                                            @endif
                                            @if (App::isLocale('dn'))
                                               {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),50) !!}
                                            @endif
                                            @if (App::isLocale('hcm'))
                                               {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),50) !!}
                                            @endif
                                        </a></h3>
                                        @if (App::isLocale('hn'))
                                           <span>{{ number_format($item->price_mb,0,".",",") }} VND</span>
                                        @endif
                                        @if (App::isLocale('dn'))
                                           <span>{{ number_format($item->price_mt,0,".",",") }} VND</span>
                                        @endif
                                        @if (App::isLocale('hcm'))
                                           <span>{{ number_format($item->price_mn,0,".",",") }} VND</span>
                                        @endif
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                        <p>{{ $item->content_vn }}</p>
                                        <div class="shop-product-icon c-fff hover-bg">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ">Thêm vào giỏ</a></li>
                                                <li><a href="{{site_url('product')}}/{{ $item->code }}" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <!-- woocommerce-pagination-area -->
                <div class="row">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="woocommerce-pagination text-center hover-bg">
                            {{ $data['list_product']->links() }}
                        </div>
                    </div>
                </div>
            @else
                <div class="note" style="padding: 30px 0px; text-align: center; font-size: 14px; background-color: #fff;">
                    <p>Hiện chúng tôi ngừng kinh doanh sản phẩm này. Chân thành cáo lỗi cùng quý khách vì sự bất tiện.</p>
                </div>
            @endif
        </div>
    </div>         
@endsection