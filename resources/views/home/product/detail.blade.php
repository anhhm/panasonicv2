@extends('home.product.masterProduct')
@section('addCss')
    <link rel="stylesheet" href="{{asset('shop')}}/css/typo.css">
@endsection
@section('contentProduct')
    @if(isset($data['detail']))
       <div class="row">
            <div class="col-lg-5">
                <div class="symple-product mb-20">
                    <div class="single-product-tab  box-shadow mb-20">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="one">
                                <a class="popup" href="{{$data['detail']['0']->img_avatar1}}">
                                    <img src="{{$data['detail']['0']->img_avatar1}}"" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="single-product-menu mb-30 box-shadow">
                        <div class="single-product-active clear">
                            <div class="single-img floatleft">
                                <a href="#" data-toggle="tab">
                                    <img src="{{$data['detail']['0']->img_avatar1}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="symple-product box-shadow bg-fff p-15 mb-30">
                    <h3>{{$data['detail']['0']->title_vn}}</h3>
                    <div class="product-content simple-product-content mb-10">
                        <ul>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                        <br/>
                        
                        @if (App::isLocale('hn'))
                           <span><span>{{ number_format($data['detail']['0']->price_mb,"0",",",".") }} VND</span></span>
                        @endif
                        @if (App::isLocale('dn'))
                           <span><span>{{ number_format($data['detail']['0']->price_mt,"0",",",".") }} VND</span></span>
                        @endif
                        @if (App::isLocale('hcm'))
                           <span><span>{{ number_format($data['detail']['0']->price_mn,"0",",",".") }} VND</span></span>
                        @endif
                    </div>
                    <div class="product_meta">
                        <b>Model:</b> <span>{{$data['detail']['0']->model}}</span><br/>
                        <b>Tình trạng:</b> <span>{{$data['detail']['0']->status}}</span><br/>
                        <b>Bảo hành:</b> <span>{{$data['detail']['0']->guarantee}}</span><br/>
                        
                    </div>
                    <p>{{$data['detail']['0']->content_vn}}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="simple-product-tab box-shadow">
                <div class="simple-product-tab-menu clear">
                    <ul>
                        <li class="active"><a href="#description" data-toggle="tab">Nội dung chính</a></li>
                        <li><a href="#reviews" data-toggle="tab">Nhận xét</a></li>
                    </ul>
                </div>
                <div class="tab-content  bg-fff">
                    <div class="tab-pane active" id="description">
                        <div class="product-description p-20 bt">
                            {!! $data['detail']['0']->detail_vn !!}
                        </div>
                    </div>
                    <div class="tab-pane" id="reviews">
                        <div class="product-reviews p-20 bt">
                            <div class="fb-comments" style="width: 100% !important;" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="3"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    @if(isset($data['list_product_of_cat']))
        <div class="product box-shadow mtb-15 bg-fff">
            <div class="product-title home2-product-title home2-bg-1 text-uppercase">
                <i class="fa fa-star-o icon bg-4"></i>
                <h3>Sản phẩm cùng loại</h3>
            </div>
            <div class="new-product-active left-right-angle home2">
                @foreach($data['list_product_of_cat'] as $item)
                    <div class="product-wrapper bl">
                        <div class="product-img">
                            <a href="{{site_url('product')}}/{{ $item->code }}">
                                <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                            </a>
                            <div class="product-icon c-fff hover-bg">
                                <ul>
                                    <li><a href="{{site_url('product')}}/{{ $item->code }}" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                    <li><a href="{{site_url('product')}}/{{ $item->code }}" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                </ul>
                            </div>
                            <span class="sale">Sale</span>
                        </div>
                        <div class="product-content">
                            <h3><a href="{{site_url('product')}}/{{ $item->code }}">{{ str_limit($item->title_vn, 50) }}</a></h3>
                            <ul>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                            @if (App::isLocale('hn'))
                               <span>{{ number_format($item->price_mb,0,".",",") }} VND</span>
                            @endif
                            @if (App::isLocale('dn'))
                               <span>{{ number_format($item->price_mt,0,".",",") }} VND</span>
                            @endif
                            @if (App::isLocale('hcm'))
                               <span>{{ number_format($item->price_mn,0,".",",") }} VND</span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endsection
@section('addjs')
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=2013036852319830&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@endsection