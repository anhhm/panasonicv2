@extends('home.master')
@section('content')
	<div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="woocommerce-breadcrumb mtb-15">
                        <div class="menu">
                            <ul>
                                <li><a href="{{site_url('home')}}">Trang chủ</a></li>
                                <li class="active"><a href="{{site_url('product')}}">Sản phẩm</a></li>

                                @if(isset($data['detail']))
                                    <li><a href="{{site_url('product/')}}/{{$data['detail']['0']->code}}">{{$data['detail']['0']->title_vn}}</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                     <!-- categories-area start -->
                    <div class="categories-area box-shadow bg-fff">
                        <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                            <i class="fa fa-bookmark icon bg-4"></i>
                            <h3>Loại sản phẩm</h3>
                        </div>
                        <div class="shop-categories-menu p-20">
                            <ul>
                                {!! $data['list_menu'] !!}
                            </ul>
                        </div>
                    </div>
                    
                    <!-- featured-area start -->
                    @if(isset($data['list_product_new']))
                        <div class="featured-area bg-fff box-shadow mtb-20">
                            <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                                <i class="fa fa-bookmark icon bg-4"></i>
                                <h3>Sản phẩm nổi bật</h3>
                            </div>
                            <div class="featured-wrapper p-20">
                                @foreach($data['list_product_new'] as $item)
                                    <div class="product-wrapper single-featured bb mb-10" style="min-height: 90px;">
                                        <div class="product-img  floatleft">
                                            <a href="{{site_url('product/')}}/{{$item->code}}">
                                                <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                                <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                                            </a>
                                        </div>
                                        <div class="product-content floatright">
                                            <h3><a href="{{site_url('product/')}}/{{$item->code}}">
                                                @if (App::isLocale('hn'))
                                                       {{ str_limit(str_replace('Máy lạnh', 'Điều hòa', $item->title_vn),50) }}
                                                    @endif
                                                    @if (App::isLocale('dn'))
                                                       {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),50) !!}
                                                    @endif
                                                    @if (App::isLocale('hcm'))
                                                       {!! str_limit(str_replace('Điều hòa', 'Máy lạnh', $item->title_vn),50) !!}
                                                    @endif
                                            </a></h3>
                                            @if (App::isLocale('hn'))
                                               <span>{{ number_format($item->price_mb,0,".",",") }} VND</span>
                                            @endif
                                            @if (App::isLocale('dn'))
                                               <span>{{ number_format($item->price_mt,0,".",",") }} VND</span>
                                            @endif
                                            @if (App::isLocale('hcm'))
                                               <span>{{ number_format($item->price_mn,0,".",",") }} VND</span>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif


                    @if(isset($data['listtag']))
                        <div class="product-tags-area bg-fff box-shadow mtb-20">
                            <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                                <i class="fa fa-bookmark icon bg-4"></i>
                                <h3>Tags</h3>
                            </div>
                            <div class="tags tag-menu hover-bg p-20">
                                <ul>
                                    @foreach($data['listtag'] as $item)
                                        <li><a href="{{site_url('tag')}}/{{ $item['code'] }}">{{ $item['title_vn'] }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-9  col-md-9 col-sm-12 col-xs-12">
                	@yield('contentProduct')
                </div>
            </div>
        </div>
    </div>
@endsection

