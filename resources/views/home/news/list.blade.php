@extends('home.news.masterNews')
@section('contentNews')
    <div class="blog-area">
        <div class="row">
            @if(isset($data['list_news']))
                @foreach($data['list_news'] as $item)
                    <div class="col-lg-4 col-md-4">
                        <div class="blog-wrapper bg-fff box-shadow mb-30">
                            <div class="blog-img mb-25">
                                <a href="{{site_url('news')}}/{{$item->code}}">
                                    <img style="min-height: 160px ;" src="{{$item->img_avatar}}" alt="">
                                </a>
                            </div>
                            <div class="blog-content">
                                <h3>
                                    <a href="{{site_url('news')}}/{{$item->code}}">
                                        @if (App::isLocale('hn'))
                                           {{ str_limit(str_replace('Máy lạnh', 'Điều hòa', $item->title_vn),50) }}
                                        @endif
                                        @if (App::isLocale('dn'))
                                           {!! str_limit(str_replace('điều hòa', 'máy lạnh', $item->title_vn),50) !!}
                                        @endif
                                        @if (App::isLocale('hcm'))
                                           {!! str_limit(str_replace('điều hòa', 'máy lạnh', $item->title_vn),50) !!}
                                        @endif
                                    </a>
                                </h3>
                                <div class="blog-meta">
                                    <span>{{$item->created_at}}</span>
                                </div>
                                <p>
                                    @if (App::isLocale('hn'))
                                       {{ str_limit(str_replace('máy lạnh', 'điều hòa', $item->content_vn),100) }}
                                    @endif
                                    @if (App::isLocale('dn'))
                                       {!! str_limit(str_replace('điều hòa', 'máy lạnh', $item->content_vn),100) !!}
                                    @endif
                                    @if (App::isLocale('hcm'))
                                       {!! str_limit(str_replace('điều hòa', 'máy lạnh', $item->content_vn),100) !!}
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <!-- woocommerce-pagination-area -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="woocommerce-pagination text-center hover-bg">
                    {{ $data['list_news']->links() }}                        
                </div>
            </div>
        </div>
        <!-- woocommerce-pagination-area -->
    </div>
@endsection