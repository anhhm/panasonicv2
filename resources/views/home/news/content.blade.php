@extends('home.news.masterNews')
@section('addCss')
    <link rel="stylesheet" href="{{asset('shop')}}/css/typo.css">
@endsection
@section('contentNews')
    <div class="blog-area">
        <div class="content-box">
        	<div class="product-description">
            @if(isset($data['detail']))
            	@foreach($data['detail'] as $item)
                @php
                  


                  $keyword  = array('Máy lạnh', 'máy lạnh', 'Máy Lạnh');
                  $links    = array(
                                'Điều hòa', 
                                'điều hòa', 
                                'ĐIỀU HÒA'
                              );
                  $content  = preg_replace(array_map(function($element) {
                      $element = preg_quote($element);
                      return "/\b{$element}\b/i";
                  }, $keyword), $links, $item->detail_vn);
                  echo $content;
                  die();





                @endphp
                @if (App::isLocale('hn'))
                  {{ str_replace('Máy lạnh', 'Điều hòa', $item->detail_vn) }}
                @endif
                @if (App::isLocale('dn'))
                  {!! str_replace('điều hòa', 'máy lạnh', $item->detail_vn) !!}
                @endif
                @if (App::isLocale('hcm'))
                  {!! str_replace('điều hòa', 'máy lạnh', $item->detail_vn) !!}
                @endif
            	@endforeach
            @endif
			</div>
		</div>
    </div>
@endsection