@extends('home.master')
@section('content')
	<div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="woocommerce-breadcrumb mtb-15">
                        <div class="menu">
                            <ul>
                                <li><a href="{{site_url('home')}}">Trang chủ</a></li>
                                <li class="active"><a href="{{site_url('news')}}">Tin tức</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <!-- categories-area start -->
                    <div class="categories-area box-shadow bg-fff">
                        <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                            <i class="fa fa-bookmark icon bg-4"></i>
                            <h3>Danh mục</h3>
                        </div>
                        <div class="shop-categories-menu p-20">
                            <ul>
                                @if(isset($data['cat_news']))
                                    @foreach($data['cat_news'] as $item)
                                        <li><a href="{{site_url('news/list')}}/{{$item->code}}" title="{{ $item->title_vn }}">{{ str_limit($item->title_vn, 28)}}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>

                    <!-- featured-area start -->
                    <!--
                    @if(isset($data['list_product_new']))
                        <div class="featured-area bg-fff box-shadow mtb-30">
                            <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                                <i class="fa fa-bookmark icon bg-4"></i>
                                <h3>Sản phẩm nổi bật</h3>
                            </div>
                            <div class="featured-wrapper p-20">
                                @foreach($data['list_product_new'] as $item)
                                    <div class="product-wrapper single-featured bb mb-10" style="min-height: 90px;">
                                        <div class="product-img  floatleft">
                                            <a href="{{site_url('product')}}/{{$item->code}}">
                                                <img src="{{ $item->img_avatar1 }}" alt="" class="primary">
                                                <img src="{{ $item->img_avatar1 }}" alt="" class="secondary">
                                            </a>
                                        </div>
                                        <div class="product-content floatright">
                                            <h3><a href="{{site_url('product')}}/{{$item->code}}">{{ str_limit($item->title_vn, 30) }}</a></h3>
                                            <span>{{ number_format($item->price_mb,'0','.','.') }} VND</span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    -->
                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
					@yield('contentNews')
                </div>
            </div>
        </div>
    </div>


@endsection