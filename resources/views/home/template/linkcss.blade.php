<!-- All css files are included here. -->
<!-- animate css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/animate.min.css">
<!-- Bootstrap fremwork main css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/bootstrap.min.css">
<!-- font-awesome css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/font-awesome.min.css">
<!-- nivo-slider css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/nivo-slider.css">
<!-- owl carousel css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/owl.carousel.min.css">
<!-- icofont css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/icofont.css">
<!-- meanmenu css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/meanmenu.css">
<!-- jquery-ui css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/jquery-ui.min.css">
<!-- magnific-popup css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/magnific-popup.css">
<!-- percircle css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/percircle.css">
<!-- Theme main style -->
<link rel="stylesheet" href="{{asset('shop')}}/css//style.css?v=1">
<!-- Responsive css -->
<link rel="stylesheet" href="{{asset('shop')}}/css/responsive.css?v=1">

<!-- Modernizr JS -->
<script src="{{asset('shop')}}/js/modernizr-2.8.3.min.js"></script>

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300&subset=latin,vietnamese,latin-ext' rel='stylesheet' type='text/css'>

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- For third-generation iPad with high-resolution Retina display: -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="air-conditioner.png">
 
<!-- For iPhone with high-resolution Retina display: -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="air-conditioner.png">
 
<!-- For first- and second-generation iPad: -->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="air-conditioner.png">
 
<!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
<link rel="apple-touch-icon-precomposed" href="air-conditioner.png">