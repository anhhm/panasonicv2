<header id="header" class="nav-down">
    <div class="header-middle-area ptb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="logo">
                        <a href="{{site_url('home')}}">
                            <img src="{{asset('shop')}}/images/Panasonic-Logo.png" title="Panasonic Vietnam" />
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="search-box">
                        <form action="{{site_url('search')}}" method="get">
                            <input type="text" name="product" placeholder="Tìm kiếm sản phẩm..."/>
                            <button><i title="Tìm kiếm" class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="area-box">
                        <p style="font-size: 13px; font-family: 'Open Sans', sans-serif;">
                            Hiện tại bạn đang ở khu vực  
                            <select class="form-control pull-right" name="locale" id="ControlSelectLocale" style="width: 50%; float: right;">
                                @if (App::isLocale('hn'))
                                    <option value="hn" selected="">Miền bắc</option>
                                @else
                                    <option value="hn">Miền Bắc</option>
                                @endif

                                @if (App::isLocale('dn'))
                                    <option value="dn" selected="">Miền Trung</option>
                                @else
                                    <option value="dn">Miền Trung</option>
                                @endif
                                
                                @if (App::isLocale('hcm'))
                                    <option value="hcm" selected="">Miền Nam</option>
                                @else
                                    <option value="hcm">Miền Nam</option>
                                @endif
                                <input id="" type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            </select>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom bg-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                    <div class="categories-menu text-uppercase click bg-7">
                        <i class="fa fa-list-ul"></i>
                        <span>Danh Mục {{ trans('text.machine') }}</span>
                    </div>
                    <div class="menu-container toggole">
                        <ul>
                            {!! $data['menu_header'] !!}
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-7 hidden-xs hidden-sm">
                    <div class="mainmenu hover-bg dropdown">
                        @include('home.template.menu')
                    </div>
                </div>
            </div>
        </div>
        <div class="mobail-menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 hidden-lg hidden-md col-sm-6">
                        <div class="mobail-menu-active">
                            @include('home.template.menu')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>