<nav>
    <ul>
    	<li class="{{ Request::is('home') ? 'active' : '' }} {{ Request::is('/') ? 'active' : '' }}">
            <a href="{{site_url('home')}}">Trang chủ</a>
        </li>
        <li class="{{ Request::is('about') ? 'active' : '' }}">
            <a href="{{site_url('about')}}/gioi-thieu-ve-panasonic-viet-nam">Giới thiệu</a>
        </li>
        <li class="{{ Request::is('product') || Request::is('product/list/') || Request::is('product/list-parent/') ? 'active' : '' }}">
            <a href="{{site_url('product')}}">Sản phẩm</a>
        </li>
        <li class="{{ Request::is('news') ? 'active' : '' }}">
            <a href="{{site_url('news')}}">Tin tức</a>
        </li>
        <li class="{{ Request::is('contact') ? 'active' : '' }}">
            <a href="{{site_url('contact')}}">Liên hệ</a>
        </li>
    </ul>
</nav>