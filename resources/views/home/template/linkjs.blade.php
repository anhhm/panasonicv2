<!-- Placed js at the end of the document so the pages load faster -->
<!-- jquery latest version -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

<!-- magnific popup js -->
<script src="{{asset('shop')}}/js/jquery.magnific-popup.min.js"></script>
<!-- mixitup js -->

<script src="{{asset('shop')}}/js/jquery.mixitup.min.js"></script>
<!-- jquery-ui price-->
<script src="{{asset('shop')}}/js/jquery-ui.min.js"></script>
<!-- ScrollUp Js -->
<script src="{{asset('shop')}}/js/jquery.scrollUp.min.js"></script>
<!-- nivo slider js -->
<script src="{{asset('shop')}}/js/jquery.nivo.slider.pack.js"></script>
<!-- mobail menu js -->
<script src="{{asset('shop')}}/js/jquery.meanmenu.js"></script>
<!-- Bootstrap framework js -->
<script src="{{asset('shop')}}/js/bootstrap.min.js"></script>
<!-- owl carousel js -->
<script src="{{asset('shop')}}/js/owl.carousel.min.js"></script>
<!-- All js plugins included in this file. -->
<script src="{{asset('shop')}}/js/plugins.js"></script>
<!-- Main js file that contents all jQuery plugins activation. -->
<script src="{{asset('shop')}}/js/main.js"></script>

<!-- Deny right click on website for downloading images. -->
<!-- <script type="text/javascript">
var  message="";function clickIE() {if (document.all) {(message);return   false;}}function clickNS(e) {if   (document.layers||(document.getElementById&&!document.all)) {if   (e.which==2||e.which==3) {(message);return false;}}}if  (document.layers)   {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;document.onselectstart=clickIE}document.oncontextmenu=new   Function("return false")
</script> -->