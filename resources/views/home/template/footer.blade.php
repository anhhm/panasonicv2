<!-- footer-area start -->
<footer class="bg-fff bt">
    <div class="footer-top-area ptb-35 bb">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <div class="footer-logo mb-25">
                            <img src="{{asset('shop')}}/images/Panasonic.png" alt="" />
                        </div>
                        <div class="footer-content">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/panasonic.admin" data-toggle="tooltip" title="Panasonic Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://twitter.com/Panasonic_Admin" data-toggle="tooltip" title="Panasonic Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/panasonicad" data-toggle="tooltip" title="Panasonic Instagram"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://plus.google.com/u/0/112754658988409731399" data-toggle="tooltip" title="Panasonic Google-Plus"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.linkedin.com/in/panasonic-aircon-68a200179/" data-toggle="tooltip" title="Panasonic Linkedin"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15">Liên hệ</h3>
                        <ul>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <b>Miền Bắc</b>
                                        <span>Tầng 2, tòa nhà PVV, KĐT Nam Cường, 234 Hoàng Quốc Việt, Q. Bắc Từ Liêm, Hà Nội</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <b>Miền Trung</b>
                                        <span>Số 150 Bế Văn Đàn, P. Chính Gián, Q. Thanh Khê, Đà Nẵng </span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <b>Miền Nam</b>
                                        <span>Tòa nhà Hải Âu Building, phường 4, Quận Tân Bình, Hồ Chí Minh </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15" style="margin-top: 20px;"></h3>
                        <div class="footer-menu home3-hover">
                            <ul>
                                <li>
                                    <div class="contuct-content">
                                        <div class="contuct-icon">
                                            <i class="fa fa-fax"></i>
                                        </div>
                                        <div class="contuct-info">
                                            @if (App::isLocale('hn'))
                                               <span>024.665.88886</span>
                                            @endif
                                            @if (App::isLocale('dn'))
                                               <span>024.665.888xx</span>
                                            @endif
                                            @if (App::isLocale('hcm'))
                                               <span>024.665.88xxx</span>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="contuct-content">
                                        <div class="contuct-icon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="contuct-info">
                                            <a href="mailto:panasonicad.aircon@gmail.com?Subject=Xin%20chào" target="_top">panasonicad.aircon@gmail.com</a>
                                        </div>
                                    </div>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
               
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15">Quy định khác</h3>
                        <div class="footer-menu">
                            <ul>
                                @if(isset($data['list_news_policy']))
                                    @foreach($data['list_news_policy'] as $item)
                                        <li><a href="{{site_url('news')}}/{{$item->code}}">{{ str_limit($item->title_vn, 40) }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom ptb-20">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="copyright">
                    <span>Copyright &copy; 2018 <a href="#"></a> All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div>
</footer>