<div class="sloder-area">
    @if(isset($data['banner_slide']))
        <div id="slider-active">
            @foreach($data['banner_slide'] as $k => $item)
                <img src="{{$item->img_avatar}}" alt="" title="#active{{$k}}"/>
            @endforeach
        </div>
        @foreach($data['banner_slide'] as $k => $item)
            <div id="active{{$k}}" class="nivo-html-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 hidden-xs ">
                            <div class="slide1-text text-left">
                                <div class="middle-text">
                                    <!-- <div class="cap-title animated text-uppercase">
                                        <h3>{{$item->title_vn}}</h3>
                                    </div>
                                    <div class="cap-dec animated">
                                        
                                    </div> -->
                                    @if(isset($item->link))
                                        <div class="cap-readmore animated">
                                            <a href="{{$item->link}}">Xem chi tiết</a>
                                        </div>  
                                    @endif
                                </div>      
                            </div>              
                        </div>
                    </div>
                </div>
                <div class="slider-progress"></div> 
            </div>
        @endforeach
    @endif
</div>
<!-- slider area end -->