@extends('auth.master')
@section('category','')
@section('action','Chỉnh sửa tin tức')
@section('addCss')
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/select2/select2.css">
    <!-- Page script -->
    <script src="{{asset('lib')}}/ckeditor/ckeditor.js"></script>
    <script src="{{asset('lib/ckfinder/ckfinder.js')}}"></script>
    <script src="{{asset('lib/js/ckfinder_function.js')}}"></script>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! isset($data['news']) ? $data['news']['title'] : null  !!}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#info" data-toggle="tab">Nội dung tin</a></li>
                                    <li><a href="#seo" data-toggle="tab">Seo</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="info">
                                        <div class="form-group">
                                            <label for="exampleInput">Tiêu đề:</label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="Nhập tiêu đề tin tức" value="{!! old('title',isset($data['news']) ? $data['news']['title_vn'] : null ) !!}">
                                            @if($errors->has('title_vn'))
                                                <p style="color:red">{{$errors->first('title_vn')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nội dung vắn tắt:</label>
                                            <textarea class="form-control" class="form-control" name="content_vn" cols="30" rows="4">{!! old('content',isset($data['news']) ? $data['news']['content_vn'] : null ) !!}</textarea>
                                            @if($errors->has('content_vn'))
                                                <p style="color:red">{{$errors->first('content_vn')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Kích hoạt:</label>
                                            <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                                <option value="0" {{ $data['news']['active']=="0"? 'selected':'' }}>Không</option>
                                                <option value="1" {{ $data['news']['active']=="1"? 'selected':'' }}>Có</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Ảnh đại diện:</label>
                                            <input class="form-control" type="text" name="avatar_project" id="avatar_project" value="{!! old('avatar_project',isset($data['news']) ? $data['news']['img_avatar'] : null ) !!}" onclick ="selectFileWithCKFinder($(this))">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Danh mục tin:</label>
                                            <select class="form-control select2" name="id_cat" data-placeholder="chon danh muc" style="width: 100%;">
                                                {{ cat_parent($data['cat_news'],0,"&#187;",isset($data['news']['id_cat'])?$data['news']['id_cat']:0) }}
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nội dung chính:</label>
                                            <textarea class="form-control textarea_editor " name="detail_vn" id="editor1" cols="30" rows="10">{!! old('detail',isset($data['news']) ? $data['news']['detail_vn'] : null ) !!}</textarea>
                                            @if($errors->has('detail_vn'))
                                                <p style="color:red">{{$errors->first('detail_vn')}}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="seo">
                                        <div class="form-group">
                                            <label for="exampleInput">Từ khóa SEO:</label>
                                            <input type="text" name="metakeyword" class="form-control"  placeholder="Nhập từ khóa SEO" value="{!! old('metakeyword',isset($data['news']) ? $data['news']['metakeyword'] : null ) !!}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Nội dung SEO:</label>
                                            <textarea class="form-control" class="form-control" name="metadescription" cols="30" rows="4">{!! old('metadescription',isset($data['news']) ? $data['news']['metadescription'] : null ) !!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInput">Tags tin tức:</label>
                                            <select multiple class="form-control select2" name="tags" data-placeholder="Chọn tags tin tức" style="width: 100%;">
                                                @if(isset($data['tags']))
                                                    @foreach($data['tags'] as $item)
                                                        <option value="{{$item['id']}}">{{$item['title_vn']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary">Thêm</button>
                                    <a type="button" class="btn btn-default" href="{{url('admin/news/list')}}">Trở về</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('addScript')
    <!-- Select2 -->
    <script src="{{asset('backendTemp')}}/plugins/select2/select2.full.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
        CKEDITOR.replace( 'editor1');
        CKEDITOR.replace( 'editor2');
		CKFinder.setupCKEditor( editor );
        var editor = CKEDITOR.replace('introduce',{
            language:'vi',
            filebrowserBrowseUrl :'{{asset('lib/ckfinder/ckfinder.html')}}',
            filebrowserImageBrowseUrl : '{{asset('lib/ckfinder/ckfinder.html?type=Images')}}',
            filebrowserFlashBrowseUrl : '{{asset('lib/ckfinder/ckfinder.html?type=Flash')}}',
            filebrowserUploadUrl : '{{asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')}}',
            filebrowserImageUploadUrl : '{{asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')}}',
            filebrowserFlashUploadUrl : '{{asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')}}',
        });
    </script>
    <!-- end -->
@endsection