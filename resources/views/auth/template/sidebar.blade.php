
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        @if(Auth::check())
            <?php $group_id=Auth::user()->group_id;?>
        @endif
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu</li>
            <li class="active">
                <a href="{{url('admin')}}">
                    <i class="fa fa-dashboard"></i> <span>Trang quản trị</span>
                </a>
            </li>

            <!-- Manager News -->
            @if(isset($menu_group['users']) || isset($menu_group['news']))
                @if($menu_group['users'] || $menu_group['news'])
                    <li class="treeview">
                        @if($menu_group['news']['action_view'] || $menu_group['cat_news']['action_view'])
                            <a>
                                <i class="fa fa-newspaper-o"></i>
                                <span>Quản lý Tin tức</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        @endif
                        <ul class="treeview-menu">
                            @if($menu_group['news']['action_add'])
                                <li><a href="{{url('admin/news/add')}}"><i class="fa fa-edit"></i> Thêm tin tức</a></li>
                            @endif
                            @if($menu_group['news']['action_view'])
                                <li><a href="{{url('admin/news/list')}}"><i class="fa fa-list-alt"></i>Danh sách tin</a></li>
                            @endif
                            @if($menu_group['cat_news']['action_view'])
                                <li><a href="{{url('admin/cat_news/list')}}"><i class="fa  fa-indent"></i> Danh mục tin</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
            @endif

            <!-- Manager Product -->
            @if(isset($menu_group['users']) || isset($menu_group['products']))
                @if($menu_group['users'] || $menu_group['products'])
                    <li class="treeview">
                        @if($menu_group['products']['action_view'] || $menu_group['cat_products']['action_view'])
                            <a>
                                <i class="fa fa-cube"></i>
                                <span>Quản lý sản phẩm</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        @endif
                        <ul class="treeview-menu">
                            @if($menu_group['products']['action_add'])
                                <li><a href="{{url('admin/products/add')}}"><i class="fa fa-edit"></i> Thêm sản phẩm</a></li>
                            @endif
                            @if($menu_group['products']['action_view'])
                                <li><a href="{{url('admin/products/list')}}"><i class="fa fa-cubes"></i>Danh sách sản phẩm</a></li>
                            @endif
                            @if($menu_group['cat_products']['action_view'])
                                <li><a href="{{url('admin/cat_products/list')}}"><i class="fa fa-tasks"></i>Danh mục sản phẩm</a></li>
                            @endif
                            @if($menu_group['producer']['action_view'])
                                <li><a href="{{url('admin/producer/list')}}"><i class="fa fa-bank"></i>Nhà sản xuất</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
            @endif

            @if(isset($menu_group['users']) || isset($menu_group['tags']))
                @if($menu_group['users'] || $menu_group['tags'])
                    @if($menu_group['tags']['action_view'])
                        <li><a href="{{url('admin/tags/list')}}"><i class="fa fa-tags"></i> Quản lý tags </a></li>
                    @endif
                @endif
            @endif


            <!-- Manager Library -->
            @if(isset($menu_group['users']) || isset($menu_group['library']))
                @if($menu_group['users'] || $menu_group['library'])
                    <li class="treeview">
                        @if($menu_group['library']['action_view'] || $menu_group['cat_library']['action_view'])
                            <a>
                                <i class="fa fa-file-photo-o"></i>
                                <span>Quản lý thư viện</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        @endif
                        <ul class="treeview-menu">
                            @if($menu_group['library']['action_view'])
                                <li><a href="{{url('admin/library/list')}}"><i class="fa fa-file-photo-o"></i>Danh sách ảnh thư viện</a></li>
                            @endif
                            @if($menu_group['cat_library']['action_view'])
                                <li><a href="{{url('admin/cat_library/list')}}"><i class="fa fa-film"></i>Danh mục thư viện</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
            @endif


            <!-- Manager users -->
            @if(isset($menu_group['users']) || isset($menu_group['users']))
                @if($menu_group['users'] || $menu_group['users'])
                    <li class="treeview">
                        @if($menu_group['users']['action_view'] || $menu_group['permissiongroup']['action_view'])
                            <a>
                                <i class="fa fa-user"></i>
                                <span>Quản lý Admin</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        @endif
                        <ul class="treeview-menu">
                            @if($menu_group['users']['action_add'])
                                <li><a href="{{url('admin/users/add')}}"><i class="fa fa-user-plus"></i> Thêm Admin</a></li>
                            @endif
                            @if($menu_group['users']['action_view'])
                                <li><a href="{{url('admin/users/list')}}"><i class="fa fa-user-md"></i>Danh sách Admin</a></li>
                            @endif
                            @if($menu_group['permissiongroup']['action_view'])
                                <li><a href="{{url('admin/permissiongroup/list')}}"><i class="fa fa-users"></i>Danh sách nhóm quyền</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
            @endif

            

            <!-- Manager guide -->
            @if(isset($menu_group['users']) || isset($menu_group['guide']))
                @if($menu_group['guide'] || $menu_group['products'])
                    <li class="treeview">
                        @if($menu_group['guide']['action_view'])
                            <a>
                                <i class="fa fa-book"></i>
                                <span>Hướng dẫn sử dụng tool</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        @endif
                        <ul class="treeview-menu">
                            @if($menu_group['guide']['action_view'])
                                <li><a href="{{url('admin/guide/news')}}"><i class="fa fa-hand-o-right"></i> Module tin tức</a></li>
                            @endif
                            @if($menu_group['guide']['action_view'])
                                <li><a href="{{url('admin/guide/library')}}"><i class="fa fa-hand-o-right"></i> Module thư viện</a></li>
                            @endif
                            @if($menu_group['guide']['action_view'])
                                <li><a href="{{url('admin/guide/product')}}"><i class="fa fa-hand-o-right"></i> Module sản phẩm</a></li>
                            @endif
                            @if($menu_group['guide']['action_view'])
                                <li><a href="{{url('admin/guide/admin')}}"><i class="fa fa-hand-o-right"></i> Module Admin</a></li>
                            @endif
                            @if($menu_group['guide']['action_view'])
                                <li><a href="{{url('admin/guide/config')}}"><i class="fa fa-hand-o-right"></i> Module khác</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
            @endif

        </ul>
        <!-- /.sidebar menu -->
    </section>
    <!-- /.sidebar -->
</aside>
