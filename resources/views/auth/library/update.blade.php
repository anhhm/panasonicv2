@extends('auth.master')
@section('category','')
@section('action','Chỉnh sửa hình ảnh')
@section('addCss')
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/select2/select2.css">
    <!-- Page script -->
    <script src="{{asset('lib/ckfinder/ckfinder.js')}}"></script>
    <script src="{{asset('lib/js/ckfinder_function.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('backendTemp/plugins/dropzone/dist/min/dropzone.min.css') }}">
    <style>
        .dropzone {
            border: 2px dashed #0087F7;
            border-radius: 5px;
            background: white;
        }
    </style>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! isset($data['news']) ? $data['news']['title'] : null  !!}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInput">Trạng thái:</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <option value="0" {{ $data['library']['active']=="0"? 'selected':'' }}>Không</option>
                                    <option value="1" {{ $data['library']['active']=="1"? 'selected':'' }}>Có</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput">Danh mục:</label>
                                <select class="form-control select2" name="id_cat" data-placeholder="chọn danh muc" style="width: 100%;">
                                    {{ cat_parent($data['cat_lib'],0,"&#187;",isset($data['library']['id_cat'])?$data['library']['id_cat']:0) }}
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput">Tiêu đề ảnh:</label>
                                <input type="text" name="title_vn" class="form-control"  placeholder="Nhập tiêu đề ảnh" value="{!! old('title',isset($data['library']) ? $data['library']['title_vn'] : null ) !!}">
                                @if($errors->has('title_vn'))
                                    <p style="color:red">{{$errors->first('title_vn')}}</p>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="exampleInput">Link :</label>
                                <input type="text" name="link" class="form-control"  placeholder="Nhập link" value="{!! old('title',isset($data['library']) ? $data['library']['link'] : null ) !!}">
                                @if($errors->has('link'))
                                    <p style="color:red">{{$errors->first('link')}}</p>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="control-label">Ảnh:</label>
                                <div>
                                    <input class="form-control" type="text" name="avatar_project" id="avatar_project" value="{!! old('avatar_project',isset($data['library']) ? $data['library']['img_avatar'] : null ) !!}" onclick ="selectFileWithCKFinder($(this))">
                                    <img width="150" src="{!! isset($data['library']) ? $data['library']['img_avatar'] : null  !!}" alt="">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary">Thêm</button>
                                    <a type="button" class="btn btn-default" href="{{url('admin/library/list')}}">Trở về</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('addScript')
    <!-- Select2 -->
    <script src="{{asset('backendTemp')}}/plugins/select2/select2.full.min.js"></script>
    <script src="{{ asset('backendTemp/plugins/dropzone/dist/min/dropzone.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });

        Dropzone.options.myDropzone= {
            url: '{{ url('admin/library/edit') }}',
            headers: {
                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            autoProcessQueue: true,
            uploadMultiple: true,
            parallelUploads: 5,
            maxFiles: 10,
            maxFilesize: 5,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            dictFileTooBig: 'Image is bigger than 5MB',
            addRemoveLinks: true,
            previewsContainer: null,
            hiddenInputContainer: "body",
        }
    </script>
    <!-- end -->
@endsection