@extends('auth.master')
@section('category',request()->segment(2))
@section('action','Chỉnh sửa người quản trị')
@section('addCss')
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/select2/select2.css">
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! isset($data['user']) ? $data['user']['name'] : null  !!}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post">
                        <div class="box-body">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Admin:</label>
                                <input name="name" type="text" class="form-control"  placeholder="Nhập tên admin" value="{!! isset($data['user']) ? $data['user']['name'] : null  !!}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Email Admin:</label>
                                <input name="email" type="text" class="form-control"  placeholder="Nhập Email admin" value="{!! isset($data['user']) ? $data['user']['email'] : null  !!}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Mật khẩu:</label>
                                <input name="pass" type="password" class="form-control"  placeholder="Nhập mật khẩu" value="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Nhập lại mật khẩu:</label>
                                <input name="repasss" type="password" class="form-control"  placeholder="Nhập lại mật khẩu" value="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tình trạng:</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <option value="0" {{ $data['user']['active']=="0"? 'selected':'' }}>Không</option>
                                    <option value="1" {{ $data['user']['active']=="1"? 'selected':'' }}>Có</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Nhóm Quyền:</label>
                                <select class="form-control select2" name="id_group" data-placeholder="chon quyền quản trị website" style="width: 100%;">
                                    @foreach($data['group_user'] as $item)
                                        <option value="{{$item['id']}}" {{ $data['user']['group_id']== $item['id'] ? 'selected':'' }}>{{$item['title']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Thêm</button>
                                    <a type="button" class="btn btn-default" href="{{url('admin/users/list')}}">Trở về</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('addScript')
    <!-- Select2 -->
    <script src="{{asset('backendTemp')}}/plugins/select2/select2.full.min.js"></script>
    <!-- CK Editor -->
    <script src="{{asset('backendTemp')}}/plugins/ckeditor/ckeditor.js"></script>
    <!-- Page script -->

    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
        CKEDITOR.replace( 'editor1' );
    </script>
    <!-- end -->
@endsection