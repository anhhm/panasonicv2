@extends('auth.master')
@section('category',"nhóm quyền")
@section('action',"Danh sách")
@section('addCss')
    <link rel="stylesheet" href="{{asset('lib/checkbox/dist/css/bootstrap3/bootstrap-switch.css')}}">
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="dataTables-example" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Tên Group</th>
                                    <th>Tên Folder</th>
                                    <th>Quyền Xem</th>
                                    <th>Quyền Thêm</th>
                                    <th>Quyền Sửa</th>
                                    <th>Quyền Xóa</th>
                                    <th>
                                        <a href="{{site_url('admin/permissiongroup/add')}}" class="btn btn-success">Thêm</a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($array as $item)
                                <tr>
                                    <td style="line-height:30px">{{ $item['name'] }}</td>
                                    <td style="line-height:30px">
                                        @foreach($item['permission'] as $kq)
                                            {{ $kq['permission_name']}}</br>
                                        @endforeach
                                    </td>
                                    <td style="line-height:30px">
                                        @foreach($item['permission'] as $kq)
                                            <input class="form-control" type="checkbox" name="product" width="35px" data-size="mini" disabled
                                                    {{ $kq['action_view']==1 ? 'checked' : '' }} >
                                            </br>
                                        @endforeach
                                    </td>
                                    <td style="line-height:30px">
                                        @foreach($item['permission'] as $kq)
                                            <input class="form-control" type="checkbox" name="product"  data-size="mini" disabled
                                                    {{ $kq['action_add']==1 ? 'checked' : '' }} >
                                            </br>
                                        @endforeach
                                    </td>
                                    <td style="line-height:30px">
                                        @foreach($item['permission'] as $kq)
                                            <input class="form-control" type="checkbox" name="product" data-size="mini" disabled
                                                    {{ $kq['action_edit']==1 ? 'checked' : '' }} >
                                            </br>
                                        @endforeach
                                    </td>
                                    <td style="line-height:30px">
                                        @foreach($item['permission'] as $kq)
                                            <input class="form-control" type="checkbox" name="product" data-size="mini" disabled
                                                    {{ $kq['action_delete']==1 ? 'checked' : '' }} >
                                            </br>
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Chức năng
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <i class="fa fa-pencil fa-fw" style="margin-left:10px"></i> <a href="{{url('admin/permissiongroup/edit/'.$item['id'])}}" style="display: inline-block;width: 75%;">Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('addScript')
    <!-- checkbox -->
    <script src="{{asset('lib/checkbox/dist/js/bootstrap-switch.js')}}"></script>
    <script>
        $(function(argument) {
            $('[type="checkbox"]').bootstrapSwitch();
        })
    </script>
@endsection