@extends('auth.master')
@section('category','Nhóm quyền')
@section('action','Chỉnh sửa')
@section('addCss')
    <link rel="stylesheet" href="{{asset('lib/checkbox/dist/css/bootstrap3/bootstrap-switch.css')}}">
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="" method="POST" role="form">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div style="margin:10px 0 20px 0">
                                <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary">Thêm</button>
                                <a type="button" class="btn btn-default" href="{{url('admin/permissiongroup/list')}}">Trở về</a>
                            </div>
                            <div class="form-group">
                                <label for="">Tên nhóm</label>
                                <input type="text" class="form-control" id="" name="group_name" placeholder="Nhập tên nhóm ..." required="" value="{{ isset($group_name) ? $group_name : null}}">
                                <input type="hidden" class="form-control" id="" name="group_id" required="" value="{{ isset($group_id) ? $group_id : null}}">
                            </div>
                            <div class="form-group">
                                <label for="">Mô tả nhóm</label>
                                <textarea class="form-control" class="form-control" name="group_content" cols="30" rows="4">{{ isset($group_content) ? $group_content : null }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput">Trạng thái:</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <option value="0">Không</option>
                                    <option value="1">Có</option>
                                </select>
                            </div>
                            
                            <table id="dataTables-example" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Tên Folder</th>
                                    <th>Quyền Xem</th>
                                    <th>Quyền Thêm</th>
                                    <th>Quyền Sửa</th>
                                    <th>Quyền Xóa</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>{{$item->permission_name}}<input type="hidden" name="{{$item->slug}}[permission_id]" value="{{ $item->permission_id }}"></td>
                                            <td>
                                                <input class="form-control" type="checkbox" name="{{$item->slug}}[view]" data-size="mini"
                                                        {{ $item->action_view==1? 'checked':''}}>
                                            </td>
                                            <td>
                                                <input class="form-control" type="checkbox" name="{{$item->slug}}[add]" data-size="mini"
                                                        {{ $item->action_add==1? 'checked':''}}>
                                            </td>
                                            <td>
                                                <input class="form-control" type="checkbox" name="{{$item->slug}}[edit]" data-size="mini"
                                                        {{ $item->action_edit==1? 'checked':''}}>
                                            </td>
                                            <td>
                                                <input class="form-control" type="checkbox" name="{{$item->slug}}[delete]" data-size="mini"
                                                        {{ $item->action_delete==1? 'checked':''}}>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('addScript')
    <!-- checkbox -->
    <script src="{{asset('lib/checkbox/dist/js/bootstrap-switch.js')}}"></script>
    <script>
        $(function(argument) {
            $('[type="checkbox"]').bootstrapSwitch();
        })
    </script>
@endsection