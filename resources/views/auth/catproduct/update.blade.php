@extends('auth.master')
@section('category','')
@section('action','Chính sửa danh mục sản phẩm')
@section('addCss')
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/select2/select2.css">
    <!-- Page script -->
    <script src="{{asset('lib')}}/ckeditor/ckeditor.js"></script>
    <script src="{{asset('lib/ckfinder/ckfinder.js')}}"></script>
    <script src="{{asset('lib/js/ckfinder_function.js')}}"></script>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! isset($data['news']) ? $data['news']['title'] : null  !!}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInput">Trạng thái</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <option value="0" {{ $data['catproduct']['active']=="0"? 'selected':'' }}>Không</option>
                                    <option value="1" {{ $data['catproduct']['active']=="1"? 'selected':'' }}>Có</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput">Danh mục cha</label>
                                <select class="form-control" name="id_cat" data-placeholder="chon danh muc" style="width: 100%;">
                                    <option value="0">Không thư mục cha nào</option>
                                    {{ cat_parent($data['cat_products'],0,"&#187;",$data['catproduct']['parent_id']) }}
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput">Thứ tự ưu tiên</label>
                                <input type="text" name="priority" class="form-control"  placeholder="Nhập thứ tự ưu tiên" value="{!! old('priority',isset($data['catproduct']) ? $data['catproduct']['priority'] : null ) !!}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInput">Tên danh mục</label>
                                <input type="text" name="title_vn" class="form-control"  placeholder="Nhập tên danh mục" value="{!! old('title',isset($data['catproduct']) ? $data['catproduct']['title_vn'] : null ) !!}">
                                @if($errors->has('title_vn'))
                                    <p style="color:red">{{$errors->first('title_vn')}}</p>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mô tả</label>
                                <textarea class="form-control" class="form-control" name="content_vn" cols="30" rows="4">{!! old('content',isset($data['catproduct']) ? $data['catproduct']['content_vn'] : null ) !!}</textarea>
                                @if($errors->has('content_vn'))
                                    <p style="color:red">{{$errors->first('content_vn')}}</p>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="control-label">Banner danh mục:</label>
                                <input class="form-control" type="text" name="banner_cat" id="banner_cat" value="{!! old('banner_cat',isset($data['catproduct']) ? $data['catproduct']['banner_cat'] : null ) !!}" onclick ="selectFileWithCKFinder($(this))">
                                <img style="margin: 10px 0px;" width="500" src="{!! isset($data['catproduct']) ? $data['catproduct']['banner_cat'] : null  !!}" alt="">
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary">Thêm</button>
                                    <a type="button" class="btn btn-default" href="{{url('admin/cat_products/list')}}">Trở về</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('addScript')
    <!-- Select2 -->
    <script src="{{asset('backendTemp')}}/plugins/select2/select2.full.min.js"></script>
    <!-- Page script -->

    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
    </script>
    <!-- end -->
@endsection