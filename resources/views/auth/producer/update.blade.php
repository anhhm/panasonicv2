@extends('auth.master')
@section('category','')
@section('action','Chỉnh sửa nhà sản xuất')
@section('addCss')
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/select2/select2.css">
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! isset($data['news']) ? $data['news']['title'] : null  !!}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInput">Nhà sản xuất</label>
                                <input type="text" name="title" class="form-control"  placeholder="Nhập nhà sản xuất" value="{!! old('title',isset($data['producer']) ? $data['producer']['title'] : null ) !!}">
                                @if($errors->has('title'))
                                    <p style="color:red">{{$errors->first('title')}}</p>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mô tả</label>
                                <textarea class="form-control" class="form-control" name="content" cols="30" rows="4">{!! old('content',isset($data['producer']) ? $data['producer']['content'] : null ) !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInput">Trạng thái</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <option value="0" {{ $data['producer']['active']=="0"? 'selected':'' }}>Không</option>
                                    <option value="1" {{ $data['producer']['active']=="1"? 'selected':'' }}>Có</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary">Thêm</button>
                                    <a type="button" class="btn btn-default" href="{{url('admin/producer/list')}}">Trở về</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('addScript')
    <!-- Select2 -->
    <script src="{{asset('backendTemp')}}/plugins/select2/select2.full.min.js"></script>
    <!-- Page script -->

    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
    </script>
    <!-- end -->
@endsection