
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>CMS | Website</title>
    <script>
        var baseUrl =  '{{url('')}}';
    </script>
    @yield('addCss')
    @include('Auth.template.linkcss')
    
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @include('Auth.template.header')
        @include('Auth.template.sidebar')
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    @yield('action') @yield('category')
                </h1>

                @if (App::isLocale('vi'))
                    <ol class="breadcrumb">
                        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Trang chính</a></li>
                        <li>Danh sách</li>
                    </ol>
                @endif

                @if (App::isLocale('en'))
                    <ol class="breadcrumb">
                        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li>list</li>
                    </ol>
                @endif
            </section>
            <div class="col-lg-12">
                @if (Session::has('flash_messages'))
                    <div class="alert alert-{!! Session::get('flash_level') !!} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {!! Session::get('flash_messages') !!}
                    </div>
                @endif
            </div>
            @yield('content')

        </div>

        @include('Auth.template.footer')
        <div class="control-sidebar-bg"></div>
    </div>
    @include('Auth.template.linkscript')

    @yield('addScript')
    <script>
        $("#ControlSelectLanguage").change(function () {
            var locale  =   $(this).val();
            var _token  =   $("input[name=_token]").val();

            $.ajax({
                url: baseUrl+'/language',
                type: 'POST',
                dataType: 'json',
                data: {locale: locale , _token : _token },
                success : function(data){
                },
                error : function(data) {

                },
                complete : function(data){
                    window.location.reload(true);
                }

            })
        })
    </script>
</body>
</html>