@extends('auth.master')
@section('category','')
@section('action','')
@section('addCss')
    <style>
        fieldset {
            width: 90%;
            margin: 10px auto !important;
            border: 1px solid #222d32 !important;
            padding: 10px !important;
        }
        fieldset legend {
            width: auto !important;
            color: #3c8dbc !important;
            padding: 0 15px !important;
            font-size: 18px !important;
            font-weight: bold !important;
            border: none !important;
            margin-bottom: 0px !important;
        }
    </style>
@endsection
@section('content')
    <section class="content" style="margin-top: 20px;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Hướng dẫn sử dụng module Quản trị viên</h3>
                    </div>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#admin" data-toggle="tab">Quản lí Admin</a></li>
                            <li><a href="#groupAdmin" data-toggle="tab">Quản lí phân quyền của nhóm admin</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="admin">
                                <div class="content" style="font-size: 14px; font-family: tahoma; line-height: 25px;">
                                    <h5>Chào mừng bạn đến với module quản lí tin tức của hệ thống CMS Website phiên bản 1.0.1</h5>
                                    <p>Sau đây là cách hướng dẫn sử dụng và quản lí tin tức trên website </p>
                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>1. Thêm mới 1 admin vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Có 2 cách để bạn thêm admin vào tool quản lí.</p>
                                    <p>&nbsp;&nbsp;<strong>Cách 1: </strong>Bạn click chuột vào mục thêm tin tức trên menu bên trái</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="{{asset('imageguide/add-admin-1.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Cách 2: </strong>Bạn click chuột vào Danh sách người quản lí trên menu bên trái  để vào trang danh sách admin</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="{{asset('imageguide/add-admin2a.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp; Trên danh sách các admin, bạn click chọn thêm để thêm mới admin vào</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/add-admin-2b.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp; Sau khi thực hiện <b>1 trong 2 bước trên</b> bạn thực hiện điền thông tin trong các trường thên trang.</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/add-admin-2c.jpg')}}" alt="">
                                    </p>
                                    <fieldset class="fieldset-note"><legend>Lưu ý</legend>
                                        <ul>
                                            <li style="text-align: justify;"><b>Tên, Email và Pasword</b> không được bỏ trống.</li>
                                            <li style="text-align: justify;"><b>Trường Danh mục</b> và <b>Trường kích hoạt</b> nếu bạn không chọn thì nó sẽ lấy danh mục đầu tiên là phần khi bạn lưu.</li>
                                            <li style="text-align: justify;"><b>Password và rePasswork</b> phải trùng khớp với nhau.</li>
                                            <li style="text-align: justify;"><b>Email và Password</b> sẽ là trường giúp bạn đăng nhập vào hệ thống nên bạn cần nhớ nhé.</li>
                                        </ul>
                                    </fieldset>

                                    <p>Sau khi đã thêm hết nội dung vào các trường bạn click vào button <b>Thêm</b> ở cuối trang. Nếu bạn không muốn lưu admin này thì click vào trở về</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="{{asset('imageguide/save.jpg')}}" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>2. Chỉnh sửa 1 admin vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để chỉnh sửa 1 admin trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách Người quản trị trên menu bên trái để vào trang danh sách admin</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="{{asset('imageguide/add-admin2a.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các người quản trị, bạn chọn 1 tin sau đó click vào chức năng chọn chỉnh sửa để tiển hành chỉnh sửa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/update-admin.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi chọn người quản trị cần sửa bạn tiến hành sửa tin theo các trường</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/update-admin1.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 4: </strong>Khi đã điền hết thông tin bạn cần chỉnh sửa thì bạn click button <b>Thêm</b> để lưu hoặc click vào button <b>Trở về</b> đẻ không lưu</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="{{asset('imageguide/save.jpg')}}" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>3. Xóa 1 người quản trị vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để Xóa 1 người quản trị trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách người quản trị trên menu bên trái để vào trang danh sách admin</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="{{asset('imageguide/add-admin2a.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các admin, bạn chọn 1 người quản trị sau đó click vào chức năng chọn <b>Xóa</b> để tiển hành xóa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/delete-admin.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi click xóa, hệ thống sẽ hỏi lại bạn 1 lần nữa có xóa hay không ? nếu chắc chắn <b>xóa</b> thì bạn click ok còn không thì bạn chọn hủy</p>
                                    <p style="text-align: center">
                                        <img width="400px" style="margin: 0px auto" src="{{asset('imageguide/delete-news1.jpg')}}" alt="">
                                    </p>
                                    <p>Trên đây là hướng dẫn sử dụng tool quản lí, cảm ơn bạn đã đọc</p>
                                </div>
                            </div>


                            <div class="tab-pane" id="groupAdmin">
                                <div class="content" style="font-size: 14px; font-family: tahoma; line-height: 25px;">
                                    <h5>Chào mừng bạn đến với module quản lí tin tức của hệ thống CMS Website phiên bản 1.0.1</h5>
                                    <p>Sau đây là cách hướng dẫn sử dụng và quản lí và phân quyền nhóm tài khoản quản trị website trên website </p>
                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>1. Thêm mới 1 nhóm admin vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>&nbsp;&nbsp;Bạn click chuột vào Danh sách người quản lí trên menu bên trái  để vào trang danh sách nhóm admin</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="{{asset('imageguide/add-groupadmin2a.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp; Trên danh sách các admin, bạn click chọn thêm để thêm mới nhóm admin vào</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/add-groupadmin-2b.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;Bạn click chuột vào button thêm để thêm nhóm admin</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/add-groupadmin-2c.jpg')}}" alt="">
                                    </p>
                                    <fieldset class="fieldset-note"><legend>Lưu ý</legend>
                                        <ul>
                                            <li style="text-align: justify;"><b>Tên và mô tả nhóm</b> không được bỏ trống.</li>
                                            <li style="text-align: justify;">Ở các <b>Quyền của admin</b> bạn click chọn.</li>
                                        </ul>
                                    </fieldset>

                                    <p>Sau khi đã thêm hết nội dung vào các trường bạn click vào button <b>Thêm</b> ở cuối trang. Nếu bạn không muốn lưu admin này thì click vào trở về</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="{{asset('imageguide/save.jpg')}}" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>2. Chỉnh sửa 1 nhóm admin vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để chỉnh sửa 1 nhóm admin trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách Người quản trị trên menu bên trái để vào trang danh sách admin</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="{{asset('imageguide/add-groupadmin2a.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các nhóm admin, bạn chọn 1 nhóm sau đó click vào chức năng chọn chỉnh sửa để tiển hành chỉnh sửa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/update-groupadmin.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi chọn nhóm cần sửa bạn tiến hành sửa theo các trường</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="{{asset('imageguide/update-groupadmin1.jpg')}}" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 4: </strong>Khi đã điền hết thông tin bạn cần chỉnh sửa thì bạn click button <b>Thêm</b> để lưu hoặc click vào button <b>Trở về</b> để không lưu</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="{{asset('imageguide/save.jpg')}}" alt="">
                                    </p>
                                    <p>Trên đây là hướng dẫn sử dụng tool quản lí, cảm ơn bạn đã đọc</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection