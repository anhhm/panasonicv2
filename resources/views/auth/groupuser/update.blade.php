@extends('admin_template.master')
@section('category',request()->segment(2))
@section('action','Chỉnh sửa')
@section('addCss')
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/select2/select2.css">
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! isset($data['user']) ? $data['user']['name'] : null  !!}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên nhóm quản trị</label>
                                <input type="text" class="form-control"  placeholder="Enter name" value="{!! isset($data['user']) ? $data['user']['name'] : null  !!}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Trạng thái</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    @if (App::isLocale('vi'))
                                        <option value="0" {{ $data['user']['active']=="0"? 'selected':'' }}>Không</option>
                                        <option value="1" {{ $data['user']['active']=="1"? 'selected':'' }}>Có</option>
                                    @endif

                                    @if (App::isLocale('en'))
                                        <option value="0" {{ $data['user']['active']=="0"? 'selected':'' }}>No</option>
                                        <option value="1" {{ $data['user']['active']=="1"? 'selected':'' }}>Yes</option>
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Chọn quyền</label>
                                <select class="form-control select2" name="id_group" data-placeholder="chon quyền quản trị website" style="width: 100%;">
                                    @foreach($data['group_user'] as $item)
                                        <option value="{{$item['id']}}">{{$item['name']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('addScript')
    <!-- Select2 -->
    <script src="{{asset('admin-style')}}/plugins/select2/select2.full.min.js"></script>
    <!-- CK Editor -->
    <script src="{{asset('admin-style')}}/plugins/ckeditor/ckeditor.js"></script>
    <!-- Page script -->

    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
        CKEDITOR.replace( 'editor1' );
    </script>
    <!-- end -->
@endsection