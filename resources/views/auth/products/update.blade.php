@extends('auth.master')
@section('category','')
@section('action','Chỉnh sửa sản phẩm')
@section('addCss')
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/select2/select2.css">
    <!-- Page script -->
    <script src="{{asset('lib')}}/ckeditor/ckeditor.js"></script>
    <script src="{{asset('lib/ckfinder/ckfinder.js')}}"></script>
    <script src="{{asset('lib/js/ckfinder_function.js')}}"></script>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! isset($data['product']) ? $data['product']['title'] : null  !!}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form_add_product" role="form" method="post" action="" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#info" data-toggle="tab">Thông tin chính</a></li>
                                    <li><a href="#num" data-toggle="tab">Thông Số</a></li>
                                    <li><a href="#seo" data-toggle="tab">Seo</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="info">
                                        <div class="form-group">
                                            <label for="exampleInput">Tên Sản Phẩm:</label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="Nhập tên sản phẩm" value="{!! old('title',isset($data['product']) ? $data['product']['title_vn'] : null ) !!}">
                                            @if($errors->has('title_vn'))
                                                <p style="color:red">{{$errors->first('title_vn')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Model:</label>
                                            <input type="text" name="model" class="form-control"  placeholder="Nhập Model" value="{!! old('title',isset($data['product']) ? $data['product']['model'] : null ) !!}">
                                            @if($errors->has('model'))
                                                <p style="color:red">{{$errors->first('model')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Danh mục:</label>
                                            <select class="form-control select2" name="id_cat_parent" data-placeholder="chọn danh muc" style="width: 100%;" onclick="checkCatChilden()">
                                                @foreach($data['cat_product'] as $item)
                                                    <option value="{{$item['id']}}" {{ $data['product']['id_cat_parent']==$item['id']? 'selected':'' }}>{{ $item['title_vn'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Danh mục con:</label>
                                            <select class="form-control" name="id_cat_childen" data-placeholder="chọn danh muc" style="width: 100%;">
                                                {{ cat_parent($data['cat_product_child'],0,"&#187;",isset($data['product']['id_cat'])?$data['product']['id_cat']:0) }}
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Mô tả vắn tắt:</label>
                                            <textarea class="form-control" class="form-control" name="content_vn" cols="30" rows="4">{!! old('content',isset($data['product']) ? $data['product']['content_vn'] : null ) !!}</textarea>
                                            @if($errors->has('content_vn'))
                                                <p style="color:red">{{$errors->first('content_vn')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Mô tả chính:</label>
                                            <textarea class="form-control textarea_editor " name="detail_vn" id="editor1" cols="30" rows="10">{!! old('detail',isset($data['product']) ? $data['product']['detail_vn'] : null ) !!}</textarea>
                                            @if($errors->has('detail_vn'))
                                                <p style="color:red">{{$errors->first('detail_vn')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Hiển thị sản phẩm:</label>
                                            <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                                <option value="1" {{ $data['product']['active']=="1"? 'selected':'' }}>Có</option>
                                                <option value="0" {{ $data['product']['active']=="0"? 'selected':'' }}>Không</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Ảnh đại diện:</label>
                                            <input class="form-control" type="text" name="avatar_project" id="avatar_project" value="{!! old('avatar_project',isset($data['product']) ? $data['product']['img_avatar1'] : null ) !!}" onclick ="selectFileWithCKFinder($(this))">
                                        </div>

                                        <div class="form-group" style="border: 1px solid black; padding: 10px;">
                                            <label class="control-label">Chọn danh sách ảnh:</label>
                                            <div class="input-group hdtuto control-group lst increment" >
                                                <input type="text" name="filenames[]" class="myfrm form-control" onclick ="selectFileWithCKFinder($(this))">
                                                <div class="input-group-btn"> 
                                                    <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                                                </div>
                                            </div>
                                            @if(!empty($data['listimg']))
                                                @foreach($data['listimg'] as $k => $item)
                                                    @if(isset($item))
                                                        <div class="clone">
                                                            <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                                                <input type="text" name="filenames[]" class="myfrm form-control" value="{!! old('filenames',isset($item) ? $item : null ) !!}" onclick ="selectFileWithCKFinder($(this))">
                                                                <div class="input-group-btn"> 
                                                                    <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Xóa</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="clone clonetab hide">
                                                <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                                    <input type="text" name="filenames[]" class="myfrm form-control" onclick ="selectFileWithCKFinder($(this))">
                                                    <div class="input-group-btn"> 
                                                        <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Xóa</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="num">
                                        <div class="form-group">
                                            <label for="exampleInput">VAT:</label>
                                            <input type="text" name="vat" class="form-control"  placeholder="Nhập VAT" value="{!! old('title',isset($data['product']) ? $data['product']['vat'] : null ) !!}">
                                            @if($errors->has('vat'))
                                                <p style="color:red">{{$errors->first('vat')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Giá sản phẩm miền bắc:</label>
                                            <input type="text" name="price_mb" class="form-control"  placeholder="Nhập giá sản phẩm miền bắc" value="{!! old('title',isset($data['product']) ? $data['product']['price_mb'] : null ) !!}">
                                            @if($errors->has('price_mb'))
                                                <p style="color:red">{{$errors->first('price_mb')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Giá sản phẩm miền trung:</label>
                                            <input type="text" name="price_mt" class="form-control"  placeholder="Nhập giá sản phẩm miền trung" value="{!! old('title',isset($data['product']) ? $data['product']['price_mt'] : null ) !!}">
                                            @if($errors->has('price_mt'))
                                                <p style="color:red">{{$errors->first('price_mt')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Giá sản phẩm miền nam:</label>
                                            <input type="text" name="price_mn" class="form-control"  placeholder="Nhập giá sản phẩm miền nam" value="{!! old('title',isset($data['product']) ? $data['product']['price_mn'] : null ) !!}">
                                            @if($errors->has('price_mn'))
                                                <p style="color:red">{{$errors->first('price_mn')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Hãng sản xuất:</label>
                                            <select class="form-control select2" name="producer" data-placeholder="Nhập hãng sản xuất" style="width: 100%;">
                                                @if(isset($data['producer']))
                                                    @foreach($data['producer'] as $item)
                                                        <option value="{{$item['id']}}" {{ $data['product']['id_producer']==$item['id']? 'selected':'' }}>{{$item['title']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Bảo hành:</label>
                                            <input type="text" name="guarantee" class="form-control"  placeholder="Nhập bảo hành" value="{!! old('title',isset($data['product']) ? $data['product']['guarantee'] : null ) !!}">
                                            @if($errors->has('guarantee'))
                                                <p style="color:red">{{$errors->first('guarantee')}}</p>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Tình trạng:</label>
                                            <select class="form-control select2" name="status" data-placeholder="Chọn tình trạng" style="width: 100%;">
                                                <option value="Còn hàng" {{ $data['product']['status']=="Còn hàng"? 'selected':'' }}>Còn hàng</option>
                                                <option value="Hết hàng" {{ $data['product']['status']=="Hết hàng"? 'selected':'' }}>Hết hàng</option>
                                                <option value="Hàng sắp về" {{ $data['product']['status']=="Hàng sắp về"? 'selected':'' }}>Hàng sắp về</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="seo">
                                        <div class="form-group">
                                            <label for="exampleInput">Từ khóa SEO:</label>
                                            <input type="text" name="metakeyword" class="form-control"  placeholder="Nhập từ khóa seo" value="{!! old('metakeyword',isset($data['product']) ? $data['product']['metakeyword'] : null ) !!}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Nội dung SEO:</label>
                                            <textarea class="form-control" class="form-control" name="metadescription" cols="30" rows="4">{!! old('content',isset($data['product']) ? $data['product']['metadescription'] : null ) !!}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput">Tags sản phẩm:</label>
                                            <select multiple class="form-control selecttag" name="tags[]" data-placeholder="Chọn tags" style="width: 100%;">
                                                @if(isset($data['listtag']))
                                                    @foreach($data['listtag'] as $value)
                                                        <option value="{{ $value['id'] }}" selected="selected">{{ $value['title_vn'] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary">Thêm</button>
                                    <a type="button" class="btn btn-default" href="{{url('admin/products/list')}}">Trở về</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('addScript')
    <!-- Select2 -->
    <script src="{{asset('backendTemp')}}/plugins/select2/select2.full.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();

            $(".selecttag").select2({
                tags: true,
                data: [
                    @foreach($data['tags'] as $item)
                        {
                            id: "{{ $item['id'] }}",
                            text: "{{ $item['title_vn'] }}"
                        },
                    @endforeach
                ],
                tokenSeparators: [','], 
                placeholder: "Add your tags here",
                /* the next 2 lines make sure the user can click away after typing and not lose the new tag */
                selectOnClose: true, 
                closeOnSelect: false
            })
        });


        function checkCatChilden(){
            var data = $('form.form_momo').serialize();
            $.ajax({
                type        : 'POST', //kiểu post
                url         : '{{site_url('admin/products/checkcat')}}',
                data        : data,
                success     :  function(data)
                {
                    console.log(data.formhtml);
                    $('#id_cat_childen').html(data.formhtml);
                }
            });
        }
        CKEDITOR.replace( 'editor1');
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".btn-success").click(function(){ 
                var lsthmtl = $(".clonetab").html();
                $(".increment").after(lsthmtl);
            });

            $("body").on("click",".btn-danger",function(){ 
                $(this).parents(".hdtuto control-group lst").remove();
            });
        });
</script>
    <!-- end -->
@endsection