@extends('Auth.master')
@section('category','')
@section('action','Danh sách')
@section('addCss')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/datatables/dataTables.bootstrap.css">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/iCheck/all.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('backendTemp')}}/plugins/select2/select2.min.css">
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="dataTables-example" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    @foreach($init_data as $item)
                                        <th width="{!! $item['width'] ? $item['width']:'' !!}">{!! $item['label'] !!}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    @foreach($init_data as $item)
                                        @if ($item['orderable'] == 'false' )
                                            <th class ='non_searchable'></th>
                                        @else
                                            @if($item['searchoptions']['type'] == 'select' )
                                                <th class="select_table">{!! $item['label'] !!}</th>
                                            @else
                                                <th>{!! $item['label'] !!}</th>
                                            @endif
                                        @endif
                                    @endforeach
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('addScript')
    <!-- DataTables -->
    <script src="{{asset('backendTemp')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('backendTemp')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!-- Select2 -->
    <script src="{{asset('backendTemp')}}/plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="{{asset('backendTemp')}}/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="{{asset('backendTemp')}}/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="{{asset('backendTemp')}}/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- bootstrap time picker -->
    <script src="{{asset('backendTemp')}}/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{asset('backendTemp')}}/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

            //Date range picker
            $('#reservation').daterangepicker();
        });

        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                processing: true,
                serverSide: true,
                ajax : "{{ url('admin/'.$slug.'/data') }}",
                columns: [
                    @foreach($init_data as $k=>$item)
                        { data: "{!! $item['name'] !!}", name: "{!! $item['name'] !!}" {!! $item['orderable'] ? ', orderable :'. $item['orderable'] : '' !!} },
                    @endforeach
                ],
                language : {
                    // "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Vietnamese.json"
                    "url": "{{url('backendTemp/plugins/datatables/lang/Vietnamese.json')}}"
                },
                order :[
                        @foreach($init_data as $key=>$value)
                        @if(isset($value['sort']) && $value['sort']!=false)
                    [ {{$key}}, "{{$value['sort']}}" ],
                    @endif
                    @endforeach
                ]
                ,
                sCharSet : "utf-8",
                initComplete: function () {
                    this.api().columns().every( function () {
                        var column = this;
                        var columnClass = column.footer().className;
                        var text = column.footer().innerHTML;
                        if(columnClass != 'non_searchable'){
                            if(columnClass == 'select_table'){//select
                                var select = $('<select class="form-control"><option value="">'+text+'</option></select>')
                                    .appendTo( $(column.footer()).empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( val ? val : '', true, false ).draw();
                                    });
                                column.data().unique().sort().each( function ( d, j ) {//lấy giá trị cột, dòng
                                    select.append( '<option value="'+d+'">'+d+'</option>' )
                                });
                            }else{//input
                                var input = document.createElement('input');
                                input.className = 'form-control';
                                input.placeholder = text;
                                $(input).appendTo($(column.footer()).empty())
                                    .on('keyup', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search(val ? val : '', true, false).draw();
                                    });
                            }
                        }
                    });
                }
            });
            //xóa ô seach datatable
            setTimeout(function(){
                $(".dataTables_filter").html('');
            }, 50);
        });
    </script>
<!-- end -->
@endsection