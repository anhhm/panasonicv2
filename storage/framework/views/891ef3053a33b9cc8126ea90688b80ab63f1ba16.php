<?php $__env->startSection('content'); ?>
	<div class="wrapper">
		<!-- Header -->
			<div id="header">
				<div id="menu_closed" title="" data-toggle="tooltip" data-original-title="Close Menu">
				    <i class="fa fa-times"></i>
				</div>

				<div class="logo">
					<a href="">
						<img src="<?php echo e(asset('cvonlineTemp')); ?>/images/profile_img11.jpg">
					</a>
				</div>

				<h4 class="slogan">
					Tran Chanh Hiep
				</h4>

				<!-- Menu Left -->
				<nav class="navigation">
					<ul class="menu-header">
						<li class="active">
							<a ref="#home">
								<i class="fa fa-home icon_menus"></i>
								<span>Home</span>
							</a>
						</li>
						<li>
							<a ref="#profile">
								<i class="fa fa-user"></i>
								<span>Profile</span>
							</a>
						</li>
						<li>
							<a ref="#resume">
								<i class="fa fa-tasks"></i>
								<span>Resume</span>
							</a>
						</li>
						<li>
							<a ref="#portfolio">
								<i class="fa fa-suitcase"></i>
								<span>Portfolio</span>
							</a>
						</li>
						<li>
							<a href="<?php echo e(site_url('home')); ?>">
								<i class="fa fa-suitcase"></i>
								<span>Blog</span>
							</a>
						</li>
						<li>
							<a ref="#contact">
								<i class="fa fa-paper-plane"></i>
								<span>Contact</span>
							</a>
						</li>
					</ul>
				</nav>
				<!-- End -->
				
				<!-- Social icon -->
				<ul class="social-icon">
					<li><a class="social-twitter" target="_blank" href="https://twitter.com/chanh_hiep"><i class="fa fa-twitter"></i></a></li>
					<li><a class="social-facebook" target="_blank" href="https://www.facebook.com/hiepga0201"><i class="fa fa-facebook"></i></a></li>
					<li><a class="social-google" target="_blank" href="https://plus.google.com/u/0/104666995977858364723"><i class="fa fa-google-plus"></i></a></li>
				</ul>
				<!-- End -->
				
				<div class="copy_right">
					<p>
						Developed by <a href="http://hiepdev.info/" style="color: #cea525;">Chanh Hiep</a>
					</p>
				</div>
			</div>
		<!-- End Header -->

		<div class="header-mobile">
			<div class="main-header">		
				<div class="logo_wrapper">
					<span class="site_title">
						<a href="" title="Throne">
							<img src="<?php echo e(asset('cvonlineTemp')); ?>/images/profile_img11.jpg" alt="FlexyBlog">
						</a>
					</span>
				</div>
				<a class="nav-btn menu_close" id="btn_open_menu" href="#"><i class="fa fa-bars"></i></a>
			</div>
		</div>

		<!-- Main -->
		<div id="main">
			<div class="bg_video">
				<video id="my-video" preload="auto" autoplay loop id="video">
					<source src="<?php echo e(asset('cvonlineTemp')); ?>/video/bg.mp4" type="video/mp4">
				</video>
			</div>
			<div class="wrapper_main">
				<!-- Page Home -->
				<section id="home">
					<div class="page-content">
						<div class="home_style">
							<div class="home_overview">
								<h1>Hiepdev</h1>
								<em>I am a <span class="typed"></span></em>
								<p>Welcome to website</p>
							</div>
							<div class="home_buttons">
								<a href="https://i.topcv.vn/tranchanhhiep?ref=903016">DOWNLOAD MY CV</a>
							</div>								
						</div>
					</div>
				</section>
				<!-- End Page -->

				<!-- Page Profile -->
				<section id="profile">
					<div class="page-content mCustomScrollbar">
						<div class="profile-style" style="height: 100%;">
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="img-profile"></div>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="detail-profile">
										<h3>hiepdev</h3>
										<p>{$content_cv->introduce}</p>
									</div>
									<div class="infor-profile">
										<ul>
											<li><span><i class="fa fa-user"></i>Name: </span>hiepdev</li>
											<li><span><i class="fa fa-calendar"></i>Birthday: </span>02/01/1993</li>
											<li><span><i class="fa fa-envelope"></i>Email: </span>hiepdev.0201@gmail.com</li>
											<li><span><i class="fa fa-phone"></i>Phone: </span>0984347435</li>
											<li><span><i class="fa fa-globe"></i>Website: </span>hiepdev</li>
											<li><span><i class="glyphicon glyphicon-map-marker"></i>Address: </span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End Page -->
				
				<!-- Page Resume -->
				<section id="resume">
					<div class="page-content mCustomScrollbar">
						<div class="resume-style">
							<div class="title">
								<h3>RESUME – PERSONAL INFO</h3>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="block-experience block">
										<div class="head">
											<h2>Experience</h2>
										</div>
										<ul class="content">
											<?php for($i = 0; $i < 2; $i++): ?>
												<li>
													<div class="item">
														<h5>sdfa
															<span class="duration">
																<i class="fa fa-calendar color"></i>
                                                                2015 - 2018
															</span>
														</h5>
														<h6>
															<span class="fa fa-briefcase"></span>
                                                            sdfsdf
														</h6>
														<p>sdafsadf</p>
													</div>
												</li>
											<?php endfor; ?>
										</ul>
									</div>
                                        <div class="block-education block">
                                            <div class="head">
                                                <h2>Education</h2>
                                            </div>
                                            <ul class="content">
                                                <?php for($i = 0; $i < 2; $i++): ?>
                                                    <li>
                                                        <div class="item">
                                                            <h5>sdfsdf
                                                                <span class="duration">
                                                                    <i class="fa fa-calendar color"></i>
                                                                    2013 - 2014
                                                                </span>
                                                            </h5>
                                                            <h6>
                                                                <span class="fa fa-book"></span>
                                                                dfgdfgsdf
                                                            </h6>
                                                        </div>
                                                    </li>
                                                <?php endfor; ?>
                                            </ul>
                                        </div>

									<div class="block-download block">
										<div class="head">
											<h2>Download My Resume</h2>
										</div>
										<a class="download" target="_blank" href="https://i.topcv.vn/tranchanhhiep?ref=903016">
											<span data-hover="Download My Resume"><i class="fa fa-cloud-download"></i> Download My Resume</span>
										</a>
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="block-skills-tech block">
										<div class="head">
											<h2>Skills</h2>
										</div>

										<div class="skill">
											<?php for($i = 0; $i < 2; $i++): ?>
												<div class="skillbar clear" data-percent="25%">
													<div class="skillbar-title"><span>ádasdas}</span></div>
													<div class="skillbar-bar" style="width: 25%;"></div>
													<div class="skill-bar-percent">25%</div>
												</div>
                                            <?php endfor; ?>
										</div>
									</div>

                                
                                    <div class="block-skills-language block">
                                        <div class="head">
                                            <h2>Lanuage Skills</h2>
                                        </div>

                                        <div class="skill">
                                            <?php for($i = 0; $i < 2; $i++): ?>
                                                <div class="skillbar clear" data-percent="50%">
                                                    <div class="skillbar-title"><span>sdfaf</span></div>
                                                    <div class="skillbar-bar" style="width: 50%;"></div>
                                                    <div class="skill-bar-percent">50%</div>
                                                </div>
                                            <?php endfor; ?>
                                        </div>
                                    </div>

                                    <div class="block-skills-hobbies block">
                                        <div class="head">
                                            <h2>Hobbies And Interests</h2>
                                        </div>

                                        <div class="skill">
                                            <?php for($i = 0; $i < 5; $i++): ?>
                                                <div class="skillbar clear" data-percent="10%">
                                                    <div class="skillbar-title"><span>fsdafasd</span></div>
                                                    <div class="skillbar-bar" style="width: 10%;"></div>
                                                    <div class="skill-bar-percent">10%</div>
                                                </div>
                                            <?php endfor; ?>
                                        </div>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End Page -->

				<!-- Page Portfolio -->
				<section id="portfolio">
					<div class="page-content mCustomScrollbar">
						<div class="portfolio-style">
							<div class="row">
								<div class="col-md-12">
									<ul class="list-project">
                                        <?php for($i = 0; $i < 5; $i++): ?>
                                            <li>
                                                <div class="item-project">
                                                    <img src="<?php echo e(asset('cvonlineTemp')); ?>/images/portfolio-01-349x232.jpg">
                                                    <div class="hover-project">
                                                        <h5>test</h5>
                                                        <p>test</p>
                                                        <ul class="btn-link">
                                                            <li><a class="fancybox-img" href="<?php echo e(asset('cvonlineTemp')); ?>/images/portfolio-01-349x232.jpg"><i class="fa fa-search"></i></a></li>
                                                            <li><a target="_blank" href="#"><i class="fa fa-link"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endfor; ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End Page -->
				
				<!-- Page Contact -->
				<section id="contact">
					<div class="page-content mCustomScrollbar">
						<div class="contact-style">
							<div class="row">
								<div class="col-md-12">
									<div class="map">
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3826.384572672674!2d107.59447527882948!3d16.456053486915938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1505922016172" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End Page -->
			</div>
			<div class="right_main">
				<div class="lx-calendar">
					<h4 class="lx-month">FEB</h4>
					<h4 class="lx-day">20</h4>
					<h4 class="lx-year">2017</h4>
				</div>
				<div class="lx-calendar">
					<h4 class="lx-time">10:12:20</h4>
				</div>
			</div>
		</div>
		<!-- End Main -->
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('cvonline.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>