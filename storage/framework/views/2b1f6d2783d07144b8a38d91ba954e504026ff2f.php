<?php $__env->startSection('content'); ?>
	<div class="main-container">
		<div class="google-map-area mb-50">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.116422421504!2d106.63796011431042!3d10.802394392303942!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529501a110951%3A0x2dd6e82f3f0ed848!2sETown!5e0!3m2!1svi!2s!4v1541308339843" height="450" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="contuct-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="contuct-detail mb-50 p-20 bg-fff box-shadow">
							<div class="contuct-title">
								<h2>Khu vực miền bắc</h2>
							</div>
                            <div class="same">
                                <h5>Địa chỉ: </h5>
                                <p>40B XXXx,Phường XXXX Quận XXXX TP.Hà Nội</p>
                            </div>
                            <div class="same">
                                <h5>EMAIL</h5>
                                <p>XXXX@gmail.com</p>
                            </div>
                            <div class="same">
                                <h5>PHONE NUMBER</h5>
                                <p>01234xxx xxxx</p>
                            </div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="contuct-detail mb-50 p-20 bg-fff box-shadow">
							<div class="contuct-title">
								<h2>Khu vực miền trung</h2>
							</div>
                            <div class="same">
                                <h5>Địa chỉ: </h5>
                                <p>40B XXXx,Phường XXXX Quận XXXX TP.Đà Nẵng</p>
                            </div>
                            <div class="same">
                                <h5>EMAIL</h5>
                                <p>XXXX@gmail.com</p>
                            </div>
                            <div class="same">
                                <h5>PHONE NUMBER</h5>
                                <p>01234xxx xxxx</p>
                            </div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="contuct-detail mb-50 p-20 bg-fff box-shadow">
							<div class="contuct-title">
								<h2>Khu vực miền nam</h2>
							</div>
                            <div class="same">
                                <h5>Địa chỉ: </h5>
                                <p>40B XXXx,Phường XXXX Quận XXXX TP.hồ Chí Minh</p>
                            </div>
                            <div class="same">
                                <h5>EMAIL</h5>
                                <p>XXXX@gmail.com</p>
                            </div>
                            <div class="same">
                                <h5>PHONE NUMBER</h5>
                                <p>01234xxx xxxx</p>
                            </div>
						</div>
					</div>
				</div>
				<!-- <div class="contuct mb-50 bg-fff box-shadow p-20">
					<div class="contuct-title">
						<h2>Contact Us</h2>
					</div>
					<div class="contuct-form form-style">
						<form action="https://devitems.com/html/oneclick-preview/oneclick/contact.html#">
							<span>Your Name (required)</span>
							<input type="text">
							<span>Your Email (required)</span>
							<input type="email">
							<span>Subject</span>
							<input type="text">
							<span>Your Message</span>
							<textarea name="#" id="#" cols="30" rows="10"></textarea>
							<button>send</button>
						</form>
					</div>
				</div> -->
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('addjs'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
        var mapOptions = {
            zoom: 11,
            center: new google.maps.LatLng(40.6700, -73.9400),
            styles: [{ "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#000000" }, { "lightness": 40 }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 21 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 19 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }]
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(40.6700, -73.9400),
            map: map,
            title: 'Brooklyn, NY 11213, USA!'
        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>