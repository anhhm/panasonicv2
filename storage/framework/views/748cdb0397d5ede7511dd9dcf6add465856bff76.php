<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',trans("admin_news.news_header")); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/datepicker/datepicker3.css">
    <!-- Page script -->
    <script src="<?php echo e(asset('lib')); ?>/ckeditor/ckeditor.js"></script>
    <script src="<?php echo e(asset('lib/ckfinder/ckfinder.js')); ?>"></script>
    <script src="<?php echo e(asset('lib/js/ckfinder_function.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo isset($data['news']) ? $data['news']['title'] : null; ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab1">Hồ Sơ</a></li>
                                    <li><a data-toggle="tab" href="#tab2">Công Việc</a></li>
                                    <li><a data-toggle="tab" href="#tab3">Học Tập</a></li>
                                    <li><a data-toggle="tab" href="#tab4">Dự án</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active">
                                        <div class="form-group">
                                            <label class="control-label">Họ Tên: </label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="Nhập hộ tên" value="">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Ngày sinh: </label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="date" class="form-control pull-right datepicker">
                                            </div>
                                            <?php if($errors->has('date')): ?>
                                                <p style="color:red"><?php echo e($errors->first('date')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Avatar:</label>
                                            <input class="form-control" type="text" name="avatar_project" id="avatar_project" value="<?php echo old('avatar_project',isset($data['news']) ? $data['news']['img_avatar'] : null ); ?>" onclick ="selectFileWithCKFinder($(this))">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Email: </label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="Nhập hộ tên" value="">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Website: </label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="Nhập hộ tên" value="">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Số Điện thoại: </label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="Nhập hộ tên" value="">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Địa chỉ: </label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="Nhập hộ tên" value="">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_news.active')); ?>:</label>
                                            <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                                <?php if(App::isLocale('vi')): ?>
                                                    <option value="0">Không</option>
                                                    <option value="1">Có</option>
                                                <?php endif; ?>

                                                <?php if(App::isLocale('en')): ?>
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                <?php endif; ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nội dung:</label>
                                            <textarea class="form-control textarea_editor " name="detail_en" id="editor1" cols="30" rows="10">
                                                <?php if($errors->has('detail_en')): ?>
                                                    <p style="color:red"><?php echo e($errors->first('detail_en')); ?></p>
                                                <?php endif; ?>
                                            </textarea>
                                            </div>
                                    </div>

                                    <div id="tab2" class="tab-pane"></div>

                                    <div id="tab3" class="tab-pane"></div>

                                    <div id="tab4" class="tab-pane"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary"><?php echo e(trans('actionbtn.btnadd')); ?></button>
                                    <a type="button" class="btn btn-default" href="<?php echo e(url('admin/news/list')); ?>"><?php echo e(trans('actionbtn.btnreturn')); ?></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('addScript'); ?>
    <!-- Select2 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.full.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();

            $('.datepicker').datepicker({
                autoclose: true
            });
        });

        CKEDITOR.replace( 'editor1');
        CKEDITOR.replace( 'editor2');
        CKFinder.setupCKEditor( editor );
        var editor = CKEDITOR.replace('introduce',{
            language:'vi',
            filebrowserBrowseUrl :'<?php echo e(asset('lib/ckfinder/ckfinder.html')); ?>',
            filebrowserImageBrowseUrl : '<?php echo e(asset('lib/ckfinder/ckfinder.html?type=Images')); ?>',
            filebrowserFlashBrowseUrl : '<?php echo e(asset('lib/ckfinder/ckfinder.html?type=Flash')); ?>',
            filebrowserUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
            filebrowserImageUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
            filebrowserFlashUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>',
        });

            var htmlexperience    = '<div class="form-group">';         
                htmlexperience    += '<label class="control-label">Công Ty: </label>';
                htmlexperience    += '<div class="controls">';
                    htmlexperience    += '<input type="text" name="name_company[]" id="name_company" class="span11 " placeholder="Công ty" value="" onchange="addEventTab($(this));" />';
                htmlexperience    += '</div>';
            htmlexperience   += '</div>';
            htmlexperience    += '<div class="form-group">';         
                htmlexperience    += '<label class="control-label">Vị trí: </label>';
                htmlexperience    += '<div class="controls">';
                    htmlexperience    += '<input type="text" value="" name="name_job[]" id="name_job" class="span11" placeholder="vị trí" value="" />';
                htmlexperience    += '</div>';
            htmlexperience   += '</div>';
            htmlexperience    += '<div class="form-group">';         
                htmlexperience    += '<label class="control-label">Ngày bắt đầu: </label>';
                htmlexperience    += '<div class="controls">';
                    htmlexperience  += '<div  data-date="12-02-2012" class="input-append date datepicker">';
                        htmlexperience    += '<input type="text" value="" name="date_begin[]" id="date_begin" data-date-format="mm-dd-yyyy" class="span11" >';
                        htmlexperience    += '<span class="add-on"><i class="icon-th"></i></span>';
                    htmlexperience    += '</div>';
                htmlexperience    += '</div>';
            htmlexperience   += '</div>';
            htmlexperience    += '<div class="form-group">';         
                htmlexperience    += '<label class="control-label">Ngày kết thúc: </label>';
                htmlexperience    += '<div class="controls">';
                    htmlexperience    += '<div  data-date="12-02-2012" class="input-append date datepicker">';
                        htmlexperience    += '<input type="text" value="" name="date_end[]" id="date_end" data-date-format="mm-dd-yyyy" class="span11" >';
                        htmlexperience    += '<span class="add-on"><i class="icon-th"></i></span>';
                    htmlexperience    += '</div>';
                htmlexperience    += '</div>';
            htmlexperience   += '</div>';
            htmlexperience    += '<div class="form-group">';         
                htmlexperience    += '<label class="control-label">Mô Tả Công Việc: </label>';
                htmlexperience    += '<div class="controls">';
                    htmlexperience    += '<textarea name="describe[]" class="textarea_editor span11" rows="6" placeholder="Enter text ..."></textarea>';
                htmlexperience    += '</div>';
            htmlexperience   += '</div>';


        // ADD DATA FOR EDUCATION
            var htmleducation     = '<div class="form-group">';         
                htmleducation     += '<label class="control-label">Trường: </label>';
                htmleducation     += '<div class="controls">';
                    htmleducation     += '<input type="text" name="name_school[]" id="name_school" class="span11 " placeholder="Trường" value="" onchange="addEventTab($(this));" />';
                htmleducation     += '</div>';
            htmleducation   += '</div>';
            htmleducation     += '<div class="form-group">';         
                htmleducation     += '<label class="control-label">Ngành: </label>';
                htmleducation     += '<div class="controls">';
                    htmleducation     += '<input type="text" value="" name="name_study[]" id="name_study" class="span11" placeholder="Ngành" value="" />';
                htmleducation     += '</div>';
            htmleducation   += '</div>';
            htmleducation     += '<div class="form-group">';         
                htmleducation     += '<label class="control-label">Ngày bắt đầu: </label>';
                htmleducation     += '<div class="controls">';
                    htmleducation     += '<div  data-date="12-02-2012" class="input-append date datepicker">';
                        htmleducation     += '<input type="text" value="" name="date_begin_study[]" id="date_begin_study" data-date-format="mm-dd-yyyy" class="span11" >';
                        htmleducation     += '<span class="add-on"><i class="icon-th"></i></span>';
                    htmleducation     += '</div>';
                htmleducation     += '</div>';
            htmleducation   += '</div>';
            htmleducation     += '<div class="form-group">';         
                htmleducation     += '<label class="control-label">Ngày kết thúc: </label>';
                htmleducation     += '<div class="controls">';
                    htmleducation     += '<div  data-date="12-02-2012" class="input-append date datepicker">';
                        htmleducation     += '<input type="text" value="" name="date_end_study[]" id="date_end_study" data-date-format="mm-dd-yyyy" class="span11" >';
                        htmleducation     += '<span class="add-on"><i class="icon-th"></i></span>';
                    htmleducation     += '</div>';
                htmleducation     += '</div>';
            htmleducation   += '</div>';
            htmleducation     += '<div class="form-group">';         
                htmleducation     += '<label class="control-label">Mô Tả Công Việc: </label>';
                htmleducation     += '<div class="controls">';
                    htmleducation     += '<textarea name="describe_study[]" class="textarea_editor span11" rows="6" placeholder="Enter text ..."></textarea>';
                htmleducation     += '</div>';
            htmleducation   += '</div>';
    // END

    // ADD DATA FOR PROJECT
        var htmlproject;
                htmlproject   = '<div class="form-group">';         
                    htmlproject   += '<label class="control-label">Tên dự án: </label>';
                    htmlproject   += '<div class="controls">';
                        htmlproject   += '<input type="text" name="name_project[]" id="name_project" class="span11" placeholder="Tên dự án" value="{$item_project.name_project}" onchange="addEventTab($(this));" />';
                    htmlproject   += '</div>';
                htmlproject   += '</div>';
                htmlproject   += '<div class="form-group">';         
                    htmlproject   += '<label class="control-label">Link dự án: </label>';
                    htmlproject   += '<div class="controls">';
                        htmlproject   += '<input type="text" name="link_project[]" id="link_project" class="span11" placeholder="Link dự án" value="{$item_project.link_project}" />';
                    htmlproject   += '</div>';
                htmlproject   += '</div>';

                htmlproject   += '<div class="form-group">';         
                    htmlproject   += '<label class="control-label">Avatar Project: </label>';
                    htmlproject   += '<div class="controls">';
                        htmlproject   += '<input type="text" name="avatar_project[]" id="avatar_project'+i+'" value="{$item_project.avatar_project}" onclick ="selectFileWithCKFinder($(this))">';
                        htmlproject   += '<img src="" alt="" width="50" height="50">';
                    htmlproject   += '</div>';
                htmlproject   += '</div>';

                htmlproject   += '<div class="form-group">';         
                    htmlproject   += '<label class="control-label">Ảnh chi tiết: </label>';
                    htmlproject   += '<div class="controls">';
                        htmlproject   += '<input type="text" name="detailed_pic[]" id="detailed_pic'+i+'" value="{$item_project.detailed_pic}" onclick ="selectFileWithCKFinder($(this))">';
                        htmlproject   += '<img src="" alt="" width="50" height="50">';
                    htmlproject   += '</div>';
                htmlproject   += '</div>';

                htmlproject   += '<div class="form-group">';         
                    htmlproject   += '<label class="control-label">Mô Tả Dự Án: </label>';
                    htmlproject   += '<div class="controls">';
                        htmlproject   += '<textarea name="describe_project[]" class="textarea_editor span11" rows="6" placeholder="Enter text ...">{$item_project.describe_project}</textarea>';
                    htmlproject   += '</div>';
                htmlproject   += '</div>';
    // END

    $("#tab2").append(htmlexperience);
    $("#tab3").append(htmleducation);
    $("#tab4").append(htmlproject);

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>