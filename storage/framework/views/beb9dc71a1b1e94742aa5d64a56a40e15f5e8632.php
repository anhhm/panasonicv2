<?php $__env->startSection('contentNews'); ?>
    <div class="blog-area">
        <div class="row">
            <?php if(isset($data['list_news'])): ?>
                <?php $__currentLoopData = $data['list_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-4 col-md-4">
                        <div class="blog-wrapper bg-fff box-shadow mb-30">
                            <div class="blog-img mb-25">
                                <a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>">
                                    <img style="min-height: 160px ;" src="<?php echo e($item->img_avatar); ?>" alt="">
                                </a>
                            </div>
                            <div class="blog-content">
                                <h3><a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>"><?php echo e(str_limit($item->title_vn, 50)); ?></a></h3>
                                <div class="blog-meta">
                                    <span><?php echo e($item->created_at); ?></span>
                                </div>
                                <p><?php echo e(str_limit($item->content_vn, 100)); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <!-- woocommerce-pagination-area -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="woocommerce-pagination-area bg-fff box-shadow ptb-10 mb-30">
                    <div class="woocommerce-pagination text-center hover-bg">
                        <?php echo e($data['list_news']->links()); ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- woocommerce-pagination-area -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.news.masterNews', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>