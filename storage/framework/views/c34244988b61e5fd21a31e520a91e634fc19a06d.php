<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',trans('actionbtn.title_table')); ?>
<?php $__env->startSection('addCss'); ?>
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/datatables/dataTables.bootstrap.css">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/iCheck/all.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="dataTables-example" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <?php $__currentLoopData = $init_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <th width="<?php echo $item['width'] ? $item['width']:''; ?>"><?php echo $item['label']; ?></th>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <?php $__currentLoopData = $init_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($item['orderable'] == 'false' ): ?>
                                            <th class ='non_searchable'></th>
                                        <?php else: ?>
                                            <?php if($item['searchoptions']['type'] == 'select' ): ?>
                                                <th class="select_table"><?php echo $item['label']; ?></th>
                                            <?php else: ?>
                                                <th><?php echo $item['label']; ?></th>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('addScript'); ?>
    <!-- DataTables -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!-- Select2 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- bootstrap time picker -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

            //Date range picker
            $('#reservation').daterangepicker();
        });

        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                processing: true,
                serverSide: true,
                ajax : "<?php echo e(url('admin/'.$slug.'/data')); ?>",
                columns: [
                    <?php $__currentLoopData = $init_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        { data: "<?php echo $item['name']; ?>", name: "<?php echo $item['name']; ?>" <?php echo $item['orderable'] ? ', orderable :'. $item['orderable'] : ''; ?> },
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],
                language : {
                    // "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Vietnamese.json"
                    "url": "<?php echo e(url('backendTemp/plugins/datatables/lang/Vietnamese.json')); ?>"
                },
                order :[
                        <?php $__currentLoopData = $init_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(isset($value['sort']) && $value['sort']!=false): ?>
                    [ <?php echo e($key); ?>, "<?php echo e($value['sort']); ?>" ],
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ]
                ,
                sCharSet : "utf-8",
                initComplete: function () {
                    this.api().columns().every( function () {
                        var column = this;
                        var columnClass = column.footer().className;
                        var text = column.footer().innerHTML;
                        if(columnClass != 'non_searchable'){
                            if(columnClass == 'select_table'){//select
                                var select = $('<select class="form-control"><option value="">'+text+'</option></select>')
                                    .appendTo( $(column.footer()).empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( val ? val : '', true, false ).draw();
                                    });
                                column.data().unique().sort().each( function ( d, j ) {//lấy giá trị cột, dòng
                                    select.append( '<option value="'+d+'">'+d+'</option>' )
                                });
                            }else{//input
                                var input = document.createElement('input');
                                input.className = 'form-control';
                                input.placeholder = text;
                                $(input).appendTo($(column.footer()).empty())
                                    .on('keyup', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search(val ? val : '', true, false).draw();
                                    });
                            }
                        }
                    });
                }
            });
            //xóa ô seach datatable
            setTimeout(function(){
                $(".dataTables_filter").html('');
            }, 50);
        });
    </script>
<!-- end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>