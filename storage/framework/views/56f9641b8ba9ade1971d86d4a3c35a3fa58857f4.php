
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <?php if(Auth::check()): ?>
            <?php $group_id=Auth::user()->group_id;?>
        <?php endif; ?>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu</li>
            <li class="active">
                <a href="<?php echo e(url('admin')); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo e(trans('admin_menu.dashboard')); ?></span>
                </a>
            </li>

            <!-- Manager News -->
            <?php if(isset($menu_group['users']) || isset($menu_group['news'])): ?>
                <?php if($menu_group['users'] || $menu_group['news']): ?>
                    <li class="treeview">
                        <?php if($menu_group['news']['action_view'] || $menu_group['cat_news']['action_view']): ?>
                            <a>
                                <i class="fa fa-newspaper-o"></i>
                                <span><?php echo e(trans('admin_menu.news')); ?></span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        <?php endif; ?>
                        <ul class="treeview-menu">
                            <?php if($menu_group['news']['action_add']): ?>
                                <li><a href="<?php echo e(url('admin/news/add')); ?>"><i class="fa fa-edit"></i> <?php echo e(trans('admin_menu.addnews')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['news']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/news/list')); ?>"><i class="fa fa-list-alt"></i><?php echo e(trans('admin_menu.listnews')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['cat_news']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/cat_news/list')); ?>"><i class="fa  fa-indent"></i> <?php echo e(trans('admin_menu.catnews')); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endif; ?>

            <!-- Manager Product -->
            <?php if(isset($menu_group['users']) || isset($menu_group['products'])): ?>
                <?php if($menu_group['users'] || $menu_group['products']): ?>
                    <li class="treeview">
                        <?php if($menu_group['products']['action_view'] || $menu_group['cat_products']['action_view']): ?>
                            <a>
                                <i class="fa fa-cube"></i>
                                <span><?php echo e(trans('admin_menu.product')); ?></span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        <?php endif; ?>
                        <ul class="treeview-menu">
                            <?php if($menu_group['products']['action_add']): ?>
                                <li><a href="<?php echo e(url('admin/products/add')); ?>"><i class="fa fa-edit"></i> <?php echo e(trans('admin_menu.addproduct')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['products']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/products/list')); ?>"><i class="fa fa-cubes"></i><?php echo e(trans('admin_menu.listproduct')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['cat_products']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/cat_products/list')); ?>"><i class="fa fa-tasks"></i><?php echo e(trans('admin_menu.catproduct')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['producer']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/producer/list')); ?>"><i class="fa fa-bank"></i><?php echo e(trans('admin_menu.producer')); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endif; ?>

            <?php if(isset($menu_group['users']) || isset($menu_group['tags'])): ?>
                <?php if($menu_group['users'] || $menu_group['tags']): ?>
                    <?php if($menu_group['tags']['action_view']): ?>
                        <li><a href="<?php echo e(url('admin/tags/list')); ?>"><i class="fa fa-tags"></i> <?php echo e(trans('admin_menu.tags')); ?> </a></li>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>


            <!-- Manager Library -->
            <?php if(isset($menu_group['users']) || isset($menu_group['library'])): ?>
                <?php if($menu_group['users'] || $menu_group['library']): ?>
                    <li class="treeview">
                        <?php if($menu_group['library']['action_view'] || $menu_group['cat_library']['action_view']): ?>
                            <a>
                                <i class="fa fa-file-photo-o"></i>
                                <span><?php echo e(trans('admin_menu.library')); ?></span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        <?php endif; ?>
                        <ul class="treeview-menu">
                            <?php if($menu_group['library']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/library/list')); ?>"><i class="fa fa-file-photo-o"></i><?php echo e(trans('admin_menu.listlibrary')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['cat_library']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/cat_library/list')); ?>"><i class="fa fa-film"></i><?php echo e(trans('admin_menu.catlibrary')); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endif; ?>

             <!-- Manager Config -->
            <?php if(isset($menu_group['users']) || isset($menu_group['users'])): ?>
                <?php if($menu_group['users'] || $menu_group['config']): ?>
                    <?php if($menu_group['config']['action_view'] || $menu_group['configbanner']['action_view']): ?>
                        <li class="treeview">
                            <a>
                                <i class="fa fa-cog"></i>
                                <span><?php echo e(trans('admin_menu.config')); ?></span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <?php if($menu_group['config']['action_view']): ?>
                                    <li><a href="<?php echo e(url('admin/config/list')); ?>"><i class="fa fa-file-photo-o"></i><?php echo e(trans('admin_menu.configlist')); ?></a></li>
                                <?php endif; ?>
                                <?php if($menu_group['configbanner']['action_view']): ?>
                                    <li><a href="<?php echo e(url('admin/configbanner/list')); ?>"><i class="fa fa-file-photo-o"></i><?php echo e(trans('admin_menu.configBanner')); ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>


            <!-- Manager users -->
            <?php if(isset($menu_group['users']) || isset($menu_group['users'])): ?>
                <?php if($menu_group['users'] || $menu_group['users']): ?>
                    <li class="treeview">
                        <?php if($menu_group['users']['action_view'] || $menu_group['permissiongroup']['action_view']): ?>
                            <a>
                                <i class="fa fa-user"></i>
                                <span><?php echo e(trans('admin_menu.user')); ?></span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        <?php endif; ?>
                        <ul class="treeview-menu">
                            <?php if($menu_group['users']['action_add']): ?>
                                <li><a href="<?php echo e(url('admin/users/add')); ?>"><i class="fa fa-user-plus"></i> <?php echo e(trans('admin_menu.adduser')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['users']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/users/list')); ?>"><i class="fa fa-user-md"></i><?php echo e(trans('admin_menu.listuser')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['permissiongroup']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/permissiongroup/list')); ?>"><i class="fa fa-users"></i><?php echo e(trans('admin_menu.groupuser')); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endif; ?>

            

            <!-- Manager guide -->
            <?php if(isset($menu_group['users']) || isset($menu_group['guide'])): ?>
                <?php if($menu_group['guide'] || $menu_group['products']): ?>
                    <li class="treeview">
                        <?php if($menu_group['guide']['action_view']): ?>
                            <a>
                                <i class="fa fa-book"></i>
                                <span><?php echo e(trans('admin_menu.guide')); ?></span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        <?php endif; ?>
                        <ul class="treeview-menu">
                            <?php if($menu_group['guide']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/guide/news')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideNews')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['guide']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/guide/library')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideLibrary')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['guide']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/guide/product')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideProduct')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['guide']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/guide/admin')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideAdmin')); ?></a></li>
                            <?php endif; ?>
                            <?php if($menu_group['guide']['action_view']): ?>
                                <li><a href="<?php echo e(url('admin/guide/config')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideConfig')); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endif; ?>

        </ul>
        <!-- /.sidebar menu -->
    </section>
    <!-- /.sidebar -->
</aside>
