<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content" style="margin-top: 20px;">
        <!-- BOX TOTAL -->
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- Total earnings -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo e($data['total_user']); ?></h3>
                        <p>Tổng người quản trị</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- ./total -->
            </div>

            <div class="col-lg-4 col-xs-6">
                <!-- Total Current balance -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo e($data['total_news']); ?></h3>
                        <p>Tổng số Tin tức</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-newspaper-o"></i>
                    </div>
                    <a href="" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- ./total -->
            </div>

            <div class="col-lg-4 col-xs-6">
                <!-- Total Paid -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo e($data['total_news']); ?><sup style="font-size: 20px">%</sup></h3>
                        <p>Quản trị</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- ./total -->
            </div>


        </div>
        <!-- /.row -->

        <!-- BOX CONTENT TOP-->
        <div class="row">
            <section class="col-lg-12">
                <!-- BOX CHART -->
                <div class="nav-tabs-custom" style="padding: 20px 0px; overflow: hidden;">
                    <div class="col-md-4">
                        <canvas id="pie-chart" width="25%" height="25%"></canvas>
                    </div>

                    <div class="col-md-4">
                        <canvas id="chart-lib" width="25%" height="25%"></canvas>
                    </div>

                    
                        
                    
                </div>
                <!-- /.box -->
            </section>
        </div>
        <!-- .box -->

        <!-- BOX CONTENT BOTTOM -->
        <div class="row">
            <section class="col-lg-12 connectedSortable">
                <!-- box tab 3 -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#Today3" data-toggle="tab">Tin tức:</a></li>
                    </ul>
                    <div class="tab-content no-padding">
                        <!-- content tab today -->
                        <div class="tab-pane active" id="today3">
                            <canvas id="chart-news_day" width="25%" height="5%"></canvas>
                        </div>
                    </div>
                </div>
                <!-- ./box -->

            </section>
        </div>
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('addScript'); ?>
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/chart/anime.min.js"></script>
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/chart/Chart.min.js"></script>
    <script>

        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
                labels: [
                        <?php $__currentLoopData = $data['statistical_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            "<?php echo e($item->title_vn); ?>",
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: ["#3e95cd","#8e5ea2"],
                    data: [
                        <?php $__currentLoopData = $data['statistical_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($item->count); ?>,
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    ],
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Statistical News'
                }
            }
        });

        new Chart(document.getElementById("chart-lib"), {
            type: 'pie',
            data: {
                labels: [
                    <?php $__currentLoopData = $data['statistical_library']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        "<?php echo e($item->title_vn); ?>",
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],

                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: ["#3e95cd","#8e5ea2"],
                    data: [
                        <?php $__currentLoopData = $data['statistical_library']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php echo e($item->count); ?>,
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    ],
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Statistical Library'
                }
            }
        });

        new Chart(document.getElementById("chart-product"), {
            type: 'pie',
            data: {
                labels: [
                    <?php $__currentLoopData = $data['statistical_product']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        "<?php echo e($item->title_vn); ?>",
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],

                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: ["#3e95cd","#8e5ea2"],
                    data: [
                        <?php $__currentLoopData = $data['statistical_product']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php echo e($item->count); ?>,
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    ],
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Statistical Product'
                }
            }
        });

        new Chart(document.getElementById("chart-news_day"), {
            type: 'line',
            data: {
                labels: [
                    <?php $__currentLoopData = $data['statistical_news_day']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        "<?php echo e($item->monthyear); ?>",
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],
                datasets: [{
                    label: 'number news',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor:'rgb(255, 99, 132)',
                    fill: false,
                    data: [
                        <?php $__currentLoopData = $data['statistical_news_day']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php echo e($item->data); ?>,
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    ],
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Số lượng tin được đăng theo tháng'
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Number'
                        },
                        ticks: {
                            stepSize: 1,
                        },
                    }]
                }
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>