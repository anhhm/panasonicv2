<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',trans("admin_library.news_header")); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.css">
    <!-- Page script -->
    <script src="<?php echo e(asset('lib/ckfinder/ckfinder.js')); ?>"></script>
    <script src="<?php echo e(asset('lib/js/ckfinder_function.js')); ?>"></script>
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp/plugins/dropzone/dist/min/dropzone.min.css')); ?>">
    <style>
        .dropzone {
            border: 2px dashed #0087F7;
            border-radius: 5px;
            background: white;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo isset($data['news']) ? $data['news']['title'] : null; ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#vn" data-toggle="tab">VietNam</a></li>
                                    <li><a href="#en" data-toggle="tab">English</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="vn">
                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_library.title')); ?>:</label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_title_lib')); ?>" value="<?php echo old('title',isset($data['library']) ? $data['library']['title_vn'] : null ); ?>">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_library.active')); ?>:</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <?php if(App::isLocale('vi')): ?>
                                        <option value="0">Không</option>
                                        <option value="1">Có</option>
                                    <?php endif; ?>

                                    <?php if(App::isLocale('en')): ?>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    <?php endif; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_library.category')); ?>:</label>
                                <select class="form-control select2" name="id_cat" data-placeholder="chon danh muc" style="width: 100%;">
                                    <?php $__currentLoopData = $data['cat_lib']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->title_vn); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label"><?php echo e(trans('admin_library.img_avatar')); ?>:</label>
                                <div>
                                    <input class="form-control" type="text" name="avatar_project" id="avatar_project" value="<?php echo old('avatar_project',isset($data['library']) ? $data['library']['img_avatar'] : null ); ?>" onclick ="selectFileWithCKFinder($(this))">
                                    <img width="150" src="<?php echo isset($data['library']) ? $data['library']['img_avatar'] : null; ?>" alt="">
                                </div>
                            </div>

                            
                                
                                

                                
                            

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary"><?php echo e(trans('actionbtn.btnadd')); ?></button>
                                    <a type="button" class="btn btn-default" href="<?php echo e(url('admin/news/list')); ?>"><?php echo e(trans('actionbtn.btnreturn')); ?></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('addScript'); ?>
    <!-- Select2 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.full.min.js"></script>
    <script src="<?php echo e(asset('backendTemp/plugins/dropzone/dist/min/dropzone.min.js')); ?>"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });

        Dropzone.options.myDropzone= {
            url: '<?php echo e(url('admin/library/edit')); ?>',
            headers: {
                'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
            },
            autoProcessQueue: true,
            uploadMultiple: true,
            parallelUploads: 5,
            maxFiles: 10,
            maxFilesize: 5,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            dictFileTooBig: 'Image is bigger than 5MB',
            addRemoveLinks: true,
            previewsContainer: null,
            hiddenInputContainer: "body",
        }
    </script>
    <!-- end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>