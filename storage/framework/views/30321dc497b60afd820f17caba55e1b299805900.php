<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',trans("admin_news.news_header")); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.css">
    <!-- Page script -->
    <script src="<?php echo e(asset('lib')); ?>/ckeditor/ckeditor.js"></script>
    <script src="<?php echo e(asset('lib/ckfinder/ckfinder.js')); ?>"></script>
    <script src="<?php echo e(asset('lib/js/ckfinder_function.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo isset($data['news']) ? $data['news']['title'] : null; ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                        <div class="box-body">

                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_news.active')); ?>:</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <?php if(App::isLocale('vi')): ?>
                                        <option value="0">Không</option>
                                        <option value="1">Có</option>
                                    <?php endif; ?>

                                    <?php if(App::isLocale('en')): ?>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    <?php endif; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label"><?php echo e(trans('admin_news.img_avatar')); ?>:</label>
                                <input class="form-control" type="text" name="avatar_project" id="avatar_project" value="<?php echo old('avatar_project',isset($data['news']) ? $data['news']['img_avatar'] : null ); ?>" onclick ="selectFileWithCKFinder($(this))">
                            </div>

                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_news.category')); ?>:</label>
                                <select class="form-control select2" name="id_cat" data-placeholder="chon danh muc" style="width: 100%;">
                                    <?php echo e(cat_parent($data['cat_news'],0,"&#187;",isset($data['news']['id_cat'])?$data['news']['id_cat']:0)); ?>

                                </select>
                            </div>

                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#vn" data-toggle="tab">VietNam</a></li>
                                    <li><a href="#eng" data-toggle="tab">English</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="vn">
                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_news.title')); ?>:</label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_title_news')); ?>" value="<?php echo old('title',isset($data['news']) ? $data['news']['title_vn'] : null ); ?>">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo e(trans('admin_news.content')); ?>:</label>
                                            <textarea class="form-control" class="form-control" name="content_vn" cols="30" rows="4">
                                            <?php echo old('content',isset($data['news']) ? $data['news']['content_vn'] : null ); ?>

                                            <?php if($errors->has('content_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('content_vn')); ?></p>
                                            <?php endif; ?>
                                        </textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo e(trans('admin_news.detail')); ?>:</label>
                                            <textarea class="form-control textarea_editor " name="detail_vn" id="editor1" cols="30" rows="10">
                                            <?php echo old('detail',isset($data['news']) ? $data['news']['detail_vn'] : null ); ?>

                                            <?php if($errors->has('detail_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('detail_vn')); ?></p>
                                            <?php endif; ?>
                                        </textarea>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="eng">
                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_news.title')); ?>:</label>
                                            <input type="text" name="title_en" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_title_news')); ?>" value="<?php echo old('title',isset($data['news']) ? $data['news']['title_en'] : null ); ?>">
                                            <?php if($errors->has('title_en')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_en')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo e(trans('admin_news.content')); ?>:</label>
                                            <textarea class="form-control" class="form-control" name="content_en" cols="30" rows="4">
                                            <?php echo old('content',isset($data['news']) ? $data['news']['content_en'] : null ); ?>

                                                <?php if($errors->has('content_en')): ?>
                                                    <p style="color:red"><?php echo e($errors->first('content_en')); ?></p>
                                                <?php endif; ?>
                                        </textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo e(trans('admin_news.detail')); ?>:</label>
                                            <textarea class="form-control textarea_editor " name="detail_en" id="editor2" cols="30" rows="10">
                                            <?php echo old('detail',isset($data['news']) ? $data['news']['detail_en'] : null ); ?>

                                                <?php if($errors->has('detail_en')): ?>
                                                    <p style="color:red"><?php echo e($errors->first('detail_en')); ?></p>
                                                <?php endif; ?>
                                        </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary"><?php echo e(trans('actionbtn.btnadd')); ?></button>
                                    <a type="button" class="btn btn-default" href="<?php echo e(url('admin/news/list')); ?>"><?php echo e(trans('actionbtn.btnreturn')); ?></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('addScript'); ?>
    <!-- Select2 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.full.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
        CKEDITOR.replace( 'editor1');
        CKEDITOR.replace( 'editor2');
		CKFinder.setupCKEditor( editor );
        var editor = CKEDITOR.replace('introduce',{
            language:'vi',
            filebrowserBrowseUrl :'<?php echo e(asset('lib/ckfinder/ckfinder.html')); ?>',
            filebrowserImageBrowseUrl : '<?php echo e(asset('lib/ckfinder/ckfinder.html?type=Images')); ?>',
            filebrowserFlashBrowseUrl : '<?php echo e(asset('lib/ckfinder/ckfinder.html?type=Flash')); ?>',
            filebrowserUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
            filebrowserImageUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
            filebrowserFlashUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>',
        });
    </script>
    <!-- end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>