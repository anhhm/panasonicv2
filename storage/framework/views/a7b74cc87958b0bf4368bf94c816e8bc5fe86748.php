<?php $__env->startSection('content'); ?>
	<div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="woocommerce-breadcrumb mtb-15">
                        <div class="menu">
                            <ul>
                                <li><a href="<?php echo e(site_url('home')); ?>">Trang chủ</a></li>
                                <li class="active"><a href="<?php echo e(site_url('product')); ?>">Sản phẩm</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                     <!-- categories-area start -->
                    <div class="categories-area box-shadow bg-fff">
                        <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                            <i class="fa fa-bookmark icon bg-4"></i>
                            <h3>Loại sản phẩm</h3>
                        </div>
                        <div class="shop-categories-menu p-20">
                            <ul>
                                <?php echo $data['list_menu']; ?>

                            </ul>
                        </div>
                    </div>
                    
                    <!-- featured-area start -->
                    <?php if(isset($data['list_product_new'])): ?>
                        <div class="featured-area bg-fff box-shadow mtb-20">
                            <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                                <i class="fa fa-bookmark icon bg-4"></i>
                                <h3>Sản phẩm nổi bật</h3>
                            </div>
                            <div class="featured-wrapper p-20">
                                <?php $__currentLoopData = $data['list_product_new']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="product-wrapper single-featured bb mb-10">
                                        <div class="product-img  floatleft">
                                            <a href="<?php echo e(site_url('product/')); ?>/<?php echo e($item->code); ?>">
                                                <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                            </a>
                                        </div>
                                        <div class="product-content floatright">
                                            <h3><a href="<?php echo e(site_url('product/')); ?>/<?php echo e($item->code); ?>"><?php echo e(str_limit($item->title_vn, 50)); ?></a></h3>
                                            <span><?php echo e(number_format($item->price_mb,"0",".",".")); ?> VND</span>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if(isset($data['listtag'])): ?>
                        <div class="product-tags-area bg-fff box-shadow mtb-20">
                            <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                                <i class="fa fa-bookmark icon bg-4"></i>
                                <h3>Tags</h3>
                            </div>
                            <div class="tags tag-menu hover-bg p-20">
                                <ul>
                                    <?php $__currentLoopData = $data['listtag']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(site_url('tag')); ?>/<?php echo e($item['code']); ?>"><?php echo e($item['title_vn']); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-lg-9  col-md-9 col-sm-12 col-xs-12">
                	<?php echo $__env->yieldContent('contentProduct'); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>