<div class="sloder-area">
    <?php if(isset($data['banner_slide'])): ?>
        <div id="slider-active">
            <?php $__currentLoopData = $data['banner_slide']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <img src="<?php echo e($item->link_img); ?>" alt="" title="#active<?php echo e($k); ?>"/>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php $__currentLoopData = $data['banner_slide']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div id="active<?php echo e($k); ?>" class="nivo-html-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 hidden-xs ">
                            <div class="slide1-text text-left">
                                <div class="middle-text">
                                    <div class="cap-title animated text-uppercase">
                                        <h3><?php echo e($item->title); ?></h3>
                                    </div>
                                    <div class="cap-dec animated">
                                        <p><?php echo e($item->content); ?></p>
                                    </div>
                                    <?php if(isset($item->link_href)): ?>
                                        <div class="cap-readmore animated">
                                            <a href="<?php echo e($item->link_href); ?>">Xem chi tiết</a>
                                        </div>  
                                    <?php endif; ?>
                                </div>      
                            </div>              
                        </div>
                    </div>
                </div>
                <div class="slider-progress"></div> 
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
</div>
<!-- slider area end -->