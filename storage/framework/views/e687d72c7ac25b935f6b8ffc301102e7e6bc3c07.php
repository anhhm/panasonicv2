<?php $__env->startSection('content'); ?>
   <div class="row">
        <!--colleft-->
                <div class="col-md-8 col-sm-12">
                    <!--post-detail-->
                    <article class="post-detail">
                        <h1>Six big ways MacOS Sierra is going to change your Apple experience</h1>
                        <div class="meta-post">
                            <a href="#">
                                Sue Benson
                            </a>
                            <em></em>
                            <span>
                                22 Sep 2016
                            </span>
                        </div>

                        <p> <span class="dropcap">N</span> ulla quis lorem ut libero malesuada feugiat. Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh.</p>


                        <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Pellentesque in ipsum id orci porta dapibus.</p>


                        <div class="post-detail-img">
                            <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/4.jpg" />
                        </div>
                        <p class="quote">Sed porttitor lectus nibh. Sed porttitor lectus nibh. Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>

                        <p>Curabitur aliquet quam id dui posuere blandit. Sed porttitor lectus nibh. Sed porttitor lectus nibh. Pellentesque in ipsum id orci porta dapibus.</p>

                        <h4>Nulla porttitor accumsan tincidunt.</h4>

                        <p>
                            <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/2.jpg" class="post-detail-img-left" />

                            Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Donec rutrum congue leo eget malesuada. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta. Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim.
                        </p>
                        <p>Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                        <h5 class="text-right font-heading"><strong>Sue Benson</strong> </h5>

                    </article>

                    <!--related post-->
                    <div class="detail-caption">
                        <span>  RELATED POST</span>
                    </div>
                    <section class="spotlight-thumbs spotlight-thumbs-related">
                        <div class="row">
                            <?php for($i = 0; $i < 3; $i++): ?>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="spotlight-item-thumb">
                                        <div class="spotlight-item-thumb-img">
                                            <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/3.jpg">
                                            </a>
                                            <a href="#" class="cate-tag">business</a>
                                        </div>
                                        <h3><a href="#">Donald Trump suggests the DNC was hacked by 'someone sitting on their bed that weighs 400 lbs'</a></h3>
                                        <div class="meta-post">
                                            <a href="#">
                                                Sue Benson
                                            </a>
                                            <em></em>
                                            <span>
                                                22 Sep 2016
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </section>

                    <!--comments-->
                    <div class="detail-caption">
                        <span>  3 COMMENTS</span>
                    </div>
                    <div class="list-comment">
                        <div class="author-info">
                            <div class="author-avatar">
                                <a class="author-link" href="#">
                                    <img src="<?php echo e(asset('teznews')); ?>/images/avatar2.jpg" />
                                </a>
                            </div>
                            <div class="author-text">
                                <div class="author-text-inner">
                                    <h3 class="author-title">
                                        <a class="author-link" href="#">
                                            Super User
                                        </a>
                                        <span>21 Sep 2016 at 7:10am</span>
                                    </h3>
                                    <p>
                                        Curabitur aliquet quam id dui posuere blandit. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit
                                        <a href="#">Reply</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        <?php echo $__env->make('home.template.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>