<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',''); ?>
<?php $__env->startSection('addCss'); ?>
    <style>
        fieldset {
            width: 90%;
            margin: 10px auto !important;
            border: 1px solid #222d32 !important;
            padding: 10px !important;
        }
        fieldset legend {
            width: auto !important;
            color: #3c8dbc !important;
            padding: 0 15px !important;
            font-size: 18px !important;
            font-weight: bold !important;
            border: none !important;
            margin-bottom: 0px !important;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section class="content" style="margin-top: 20px;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Hướng dẫn sử dụng module sản phẩm</h3>
                    </div>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#product" data-toggle="tab">Quản lí sản phẩm</a></li>
                            <li><a href="#cat_product" data-toggle="tab">Quản lí danh mục sản phẩm</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="product">
                                <div class="content" style="font-size: 14px; font-family: tahoma; line-height: 25px;">
                                    <h5>Chào mừng bạn đến với module quản lí sản phẩm của hệ thống CMS Website phiên bản 1.0.1</h5>
                                    <p>Sau đây là cách hướng dẫn sử dụng và quản lí sản phẩm trên website </p>
                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>1. Thêm mới 1 sản phẩm vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Có 2 cách để bạn thêm sản phẩm vào tool quản lí.</p>
                                    <p>&nbsp;&nbsp;<strong>Cách 1: </strong>Bạn click chuột vào mục thêm sản phẩm trên menu bên trái</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-product-1.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Cách 2: </strong>Bạn click chuột vào Danh sách sản phẩm trên menu bên trái  để vào trang danh sách sản phẩm</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-product-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp; Trên danh sách các sản phẩm, bạn click chọn thêm để thêm mới sản phẩm vào</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-product-2b.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp; Sau khi thực hiện <b>1 trong 2 bước trên</b> bạn thực hiện điền thông sản phẩm trong các trường thên trang.</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-product-2c.jpg')); ?>" alt="">
                                    </p>
                                    <fieldset class="fieldset-note"><legend>Lưu ý</legend>
                                        <ul>
                                            <li style="text-align: justify;"><b>Trường tiêu đề bài viết, Mô tả vắn tắt, Mô tả chính</b> không được bỏ trống.</li>
                                            <li style="text-align: justify;"><b>Ảnh đại diện sản phẩm</b> nếu bạn không chọn thì khi lưu nó sẽ lấy hình đại diện mặc định.</li>
                                            <li style="text-align: justify;"><b>Trường Danh mục</b> và <b>Trường kích hoạt</b> nếu bạn không chọn thì nó sẽ lấy danh mục đầu tiên là phần khi bạn lưu.</li>
                                        </ul>
                                    </fieldset>

                                    <p>Sau khi đã thêm hết nội dung vào các trường bạn click vào button <b>Thêm</b> ở cuối trang. Nếu bạn không muốn lưu bài viết này thì click vào <b>trở về</b></p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/save.jpg')); ?>" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>2. Chỉnh sửa 1 sản phẩm vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để chỉnh sửa 1 sản phẩm trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách sản phẩm trên menu bên trái để vào trang danh sách sản phẩm</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-product-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các sản phẩm, bạn chọn 1 sản phẩm sau đó click vào chức năng chọn chỉnh sửa để tiển hành chỉnh sửa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/update-product.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi chọn sản phẩm cần sửa bạn tiến hành sửa sản phẩm theo các trường</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/update-product1.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 4: </strong>Khi đã điền hết thông sản phẩm bạn cần chỉnh sửa thì bạn click button <b>Thêm</b> để lưu hoặc click vào button <b>Trở về</b> đẻ không lưu bài viết</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/save.jpg')); ?>" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>3. Xóa 1 sản phẩm vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để Xóa 1 sản phẩm trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách sản phẩm trên menu bên trái để vào trang danh sách sản phẩm</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-product-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các sản phẩm, bạn chọn 1 sản phẩm sau đó click vào chức năng chọn <b>Xóa</b> để tiển hành xóa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/delete-product.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi click xóa, hệ thống sẽ hỏi lại bạn 1 lần nữa có xóa hay không ? nếu chắc chắn <b>xóa</b> thì bạn click ok còn không thì bạn chọn hủy</p>
                                    <p style="text-align: center">
                                        <img width="400px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/delete-news1.jpg')); ?>" alt="">
                                    </p>
                                    <p>Trên đây là hướng dẫn sử dụng tool quản lí, cảm ơn bạn đã đọc</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="cat_product">
                                <div class="content" style="font-size: 14px; font-family: tahoma; line-height: 25px;">
                                    <h5>Chào mừng bạn đến với module quản lí sản phẩm của hệ thống CMS Laravel phiên bản 1.0.1</h5>
                                    <p>Sau đây là cách hướng dẫn sử dụng và quản lí sản phẩm trên website </p>
                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>1. Thêm mới 1 danh mục sản phẩm vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để thêm mới 1 danh mục sản phẩm bạn thực hiện theo các bước sau.</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách sản phẩm trên menu bên trái  để vào trang danh sách sản phẩm</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catproduct-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong> Trên danh sách các danh mục, bạn click chọn thêm để thêm mới danh mục vào</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catproduct-2b.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp; <strong>Bước 3: </strong>Sau khi thực hiện <b>2 bước trên</b> bạn thực hiện điền thông sản phẩm trong các trường thên trang.</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catproduct-2c.jpg')); ?>" alt="">
                                    </p>
                                    <fieldset class="fieldset-note"><legend>Lưu ý</legend>
                                        <ul>
                                            <li style="text-align: justify;"><b>Trường tên danh mục, Nội dung</b> không được bỏ trống.</li>
                                            <li style="text-align: justify;"><b>Trường Danh mục</b> và <b>Trường trạng thái</b> nếu bạn không chọn thì nó sẽ lấy danh mục đầu tiên là phần khi bạn lưu.</li>
                                        </ul>
                                    </fieldset>

                                    <p>Sau khi đã thêm hết nội dung vào các trường bạn click vào button <b>Thêm</b> ở cuối trang. Nếu bạn không muốn lưu bài viết này thì click vào trở về</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/save.jpg')); ?>" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>2. Chỉnh sửa 1 danh mục sản phẩm vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để chỉnh sửa 1 sản phẩm trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách danh mục trên menu bên trái để vào trang danh sách sản phẩm</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catproduct-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các sản phẩm, bạn chọn 1 danh mục sau đó click vào chức năng chọn chỉnh sửa để tiển hành chỉnh sửa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/update-catproduct.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi chọn danh mục cần sửa bạn tiến hành sửa sản phẩm theo các trường</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/update-catproduct1.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 4: </strong>Khi đã điền hết thông sản phẩm bạn cần chỉnh sửa thì bạn click button <b>Thêm</b> để lưu hoặc click vào button <b>Trở về</b> đẻ không lưu bài viết</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/save.jpg')); ?>" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>3. Xóa 1 danh mục vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để Xóa 1 danh mục trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách danh mục trên menu bên trái để vào trang danh sách sản phẩm</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catproduct-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các danh mục, bạn chọn 1 sản phẩm sau đó click vào chức năng chọn <b>Xóa</b> để tiển hành xóa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/delete-catproduct.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi click xóa, hệ thống sẽ hỏi lại bạn 1 lần nữa có xóa hay không ? nếu chắc chắn <b>xóa</b> thì bạn click ok còn không thì bạn chọn hủy</p>
                                    <p style="text-align: center">
                                        <img width="400px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/delete-news1.jpg')); ?>" alt="">
                                    </p>
                                    <p>Trên đây là hướng dẫn sử dụng tool quản lí, cảm ơn bạn đã đọc</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>