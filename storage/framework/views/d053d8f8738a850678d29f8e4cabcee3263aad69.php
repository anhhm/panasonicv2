<?php $__env->startSection('content'); ?>
   <div id="wrap-body">
        <!--slideshow-->
        <div class="heading-group slideshow color-w">
            <h1>Chào mừng bạn đến với<span>Restaurent</span></h1>
            <video id="my-video" preload="auto" autoplay loop id="video">
                <source src="<?php echo e(asset('restaurant/video/video1.mp4')); ?>" type="video/webm">
            </video>
            <div class="star-group">
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o star-1x"></i>
                <i class="fa fa-star-o"></i>
            </div>
            <a class="btn btn-1" href="#">Read more</a>
        </div>
        <!--/slideshow-->
        <div class="container" id="main-content">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <!--welcome-->
                    <div class="mod-welcome" style="padding: 15px 0px; line-height: 23px;">
                        <div class="title">Chilli Crab<h3>Restaurent</h3></div>
                        <p>Không cần phải đến Singapore mà bạn hãy đến với Nhà Hàng Cua Ớt Singapore để thưởng thức ẩm thực Singapore,
                            nổi tiếng với món cua sốt ớt được chế biến từ nhóm đầu bếp chuyên nghiệp đến từ Singapore cùng với sự phục
                            vụ nhiệt tình ,chu đáo. Nơi có nhiều không gian lí tưởng để tiếp khách với các phòng VIP sang trọng, tiệc
                            liên hoan, sinh nhật, gặp mặt và tất niên cuối năm.
                            <a class="readmore" href="">Xem tiếp</a>
                        </p>
                    </div>
                    <!--/welcome-->
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-4">
                    <!--menu category-->
                    <div class="mn-category color-w">
                        <div class="mn-category-inner">
                            <div class="title"><h3>Thực đơn chính</h3></div>
                            <ul class="list-ul">
                                <?php if(isset($data['main_dishes'])): ?>
                                    <?php $__currentLoopData = $data['main_dishes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <a href=""><?php echo e($k+1); ?>. <?php echo e($item->title_vn); ?></a>
                                            <?php if($item->price == 0): ?>
                                                <span style="font-size: 12px;">Theo thời giá</span>
                                            <?php else: ?>
                                                <span style="font-size: 12px;"><?php echo e(number_format($item->price,0,".",",")); ?> VND</span>
                                            <?php endif; ?>
                                            
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <p>Menu đang cập nhật</p>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <!--/menu category-->
                </div>
                <div class="col-sm-7 col-md-8">
                    <!--gallery-->
                    <div class="gallery-1">
                        <div class="row">
                            <?php if(isset($data['hightlight'])): ?>
                                <?php $__currentLoopData = $data['hightlight']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="gallery-item col-xs-6 col-sm-6 col-md-6">
                                        <a href="#" class="hover-img">
                                            <img src="<?php echo e($item->img_avatar); ?>" alt="image">
                                            <div class="hover-caption color-w">
                                                <h3 style="font-size: 28px;"><?php echo e($item->title_vn); ?></h3>
                                                <?php if($item->price == 0): ?>
                                                    <h4>Theo thời giá</h4>
                                                <?php else: ?>
                                                    <h4><?php echo e(number_format($item->price,0,".",",")); ?> VND</h4>
                                                <?php endif; ?>
                                            </div>
                                        </a>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <p>Dữ liệu trống</p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--/gallery-->
                </div>
            </div>
            <!--populer-->

                <div class="product most-popular">
                    <div class="title"><h3>Món ăn phổ biến</h3></div>
                    <div class="row">
                        <div class="owl" data-items="4" data-itemsDesktop="4" data-itemsDesktopSmall="3" data-itemsTablet="2" data-itemsMobile="1">
                            <?php if(isset($data['popular_dishes'])): ?>
                                <?php $__currentLoopData = $data['popular_dishes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-12">
                                        <div class="thumbnail">
                                            <a href="<?php echo e($item->img_avatar); ?>" class="hover-img image-zoom" style="height: 125px;">
                                                <img src="<?php echo e($item->img_avatar); ?>" alt="image">
                                                <div class="hover-caption color-w"><i class="fa fa-search-plus"></i></div>
                                            </a>
                                            <div class="caption">
                                                <h4><a href=""><?php echo e($item->title_vn); ?></a></h4>
                                                <?php if($item->price == 0): ?>
                                                    <p class="price" style="font-size: 14px; height: 30px;">Theo thời giá</p>
                                                <?php else: ?>
                                                    <p class="price" style="font-size: 14px; height: 30px;"><?php echo e(number_format($item->price,0,".",",")); ?> VND</p>
                                                <?php endif; ?>
                                                
                                                <p style="text-align: left;"><?php echo e($item->content_vn); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <p>Menu đang cập nhật</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <!--testimonial-->
            <div class="heading-group testimonial color-w">
                <h2>Cảm nhận</h2>
                    <div class="star-group">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o star-1x"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                <div class="owl" data-items="1" data-itemsDesktop="1" data-itemsDesktopSmall="1" data-itemsTablet="1" data-itemsMobile="1">
                    <div class="testimonial-item">
                        <figure class="testimonial-img"><img src="<?php echo e(asset('restaurant/images/face-2.jpg')); ?>" alt="image"></figure>
                        <h4>- Susanna Davis -</h4>
                        <p>" Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh."</p>
                    </div>
                    <div class="testimonial-item">
                        <figure class="testimonial-img"><img src="<?php echo e(asset('restaurant/images/face-2.jpg')); ?>" alt="image"></figure>
                        <h4>- Peter Corbett -</h4>
                        <p>" Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh."</p>    
                    </div>
                    <div class="testimonial-item">
                        <figure class="testimonial-img"><img src="<?php echo e(asset('restaurant/images/face-2.jpg')); ?>" alt="image"></figure>
                        <h4>- William Smith -</h4>
                        <p>" Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh. "</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!--blog recent-->
                    <div class="recent_post">
                        <div class="title"><h3>Tin tức</h3></div>
                        <?php if(isset($data['list_news'])): ?>
                            <?php $__currentLoopData = $data['list_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="media media-item">
                                <div class="media-left">
                                    <figure>
                                        <a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>" class="hover-img">
                                            <img src="<?php echo e($item->img_avatar); ?>" alt="image" width="110">
                                        </a>
                                    </figure>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><a href=""><?php echo e($item->title_vn); ?></a></h4>
                                    <div class="date">
                                        <span><?php echo e(date('d-m-Y', strtotime($item->created_at))); ?></span>
                                    </div>
                                    <p><?php echo e($item->content_vn); ?>

                                        <a class="readmore" href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>">Xem tiếp</a>
                                    </p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <p>Tin đang cập nhật</p>
                        <?php endif; ?>
                    </div>
                    <!--blog recent-->
                </div>
                <div class="col-md-6">
                    <!--event-->
                    <div class="event">
                        <div class="title"><h3>Sự kiện</h3></div>
                            <div class="owl" data-items="1" data-itemsDesktop="1" data-itemsDesktopSmall="1" data-itemsTablet="1" data-itemsMobile="1">
                                <?php if(isset($data['list_news'])): ?>
                                    <?php $__currentLoopData = $data['list_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="event-item">
                                            <a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>" class="hover-img">
                                                <img src="<?php echo e($item->img_avatar); ?>" alt="image">
                                            </a>
                                            <div class="media media-item">
                                                <div class="media-left media-middle color-w">
                                                    <?php echo e(date('d-m-Y', strtotime($item->created_at))); ?>

                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><a href=""><?php echo e($item->title_vn); ?></a></h4>
                                                    <p><?php echo e($item->content_vn); ?>

                                                        <a class="readmore" href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>">Xem tiếp</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                    </div>
                    <!--/event-->
                </div>
            </div>
        </div>
    </div>
    <style>
        .hover-img:hover .hover-caption {
            top: 50%;
            transform: translateY(-50%);
        }
        .product .thumbnail{
            height: 310px;
            overflow: hidden;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>