<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content" style="margin-top: 20px;">
        <!-- BOX TOTAL -->
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- Total earnings -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo e($data['total_user']); ?></h3>
                        <p>Tổng người quản trị</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- ./total -->
            </div>

            <div class="col-lg-4 col-xs-6">
                <!-- Total Current balance -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo e($data['total_news']); ?></h3>
                        <p>Tổng số Tin tức</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-newspaper-o"></i>
                    </div>
                    <a href="" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- ./total -->
            </div>

            <div class="col-lg-4 col-xs-6">
                <!-- Total Paid -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo e($data['total_news']); ?><sup style="font-size: 20px">%</sup></h3>
                        <p>Quản trị</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- ./total -->
            </div>


        </div>
        <!-- /.row -->

        <!-- BOX CONTENT TOP-->
        <div class="row">
            <div class="col-lg-4">
                <canvas id="pie-chart" width="25%" height="25%"></canvas>
            </div>

            <div class="col-lg-8">
                <canvas id="chart-news_day"></canvas>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <canvas id="chart-days-in-month"></canvas>
            </div>
        </div>
        <!-- .box -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('addScript'); ?>
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/chart/anime.min.js"></script>
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/chart/Chart.min.js"></script>
    <script>
        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
                labels: [
                        <?php $__currentLoopData = $data['statistical_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            "<?php echo e($item->title_vn); ?>",
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: ["#3e95cd","#8e5ea2"],
                    data: [
                        <?php $__currentLoopData = $data['statistical_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($item->count); ?>,
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    ],
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Phân loại tin tức'
                }
            }
        });

        

        new Chart(document.getElementById("chart-news_day"), {
            type: 'bar',
            data: {
                labels: [
                    <?php $__currentLoopData = $data['statistical_products_day']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        "<?php echo e($item->monthyear); ?>",
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],
                datasets: [
                    {
                        label: 'số lượng sản phẩm',
                        backgroundColor:[
                            "rgba(191, 25, 232, 0.2)",
                            "rgba(191, 25, 232, 0.2)",
                            "rgba(191, 25, 232, 0.2)",
                            "rgba(191, 25, 232, 0.2)",
                            "rgba(255, 99, 132, 0.2)",
                            "rgba(255, 159, 64, 0.2)",
                            "rgba(255, 205, 86, 0.2)",
                            "rgba(75, 192, 192, 0.2)",
                            "rgba(54, 162, 235, 0.2)",
                            "rgba(153, 102, 255, 0.2)",
                            "rgba(201, 203, 207, 0.2)",
                            "rgba(181, 147, 50, 0.2)",
                            "rgba(232, 130, 81, 0.2)",
                        ],
                        borderColor:[
                            "rgb(191, 25, 232)",
                            "rgb(191, 25, 232)",
                            "rgb(191, 25, 232)",
                            "rgb(191, 25, 232)",
                            "rgb(255, 99, 132)",
                            "rgb(255, 159, 64)",
                            "rgb(255, 205, 86)",
                            "rgb(75, 192, 192)",
                            "rgb(54, 162, 235)",
                            "rgb(153, 102, 255)",
                            "rgb(201, 203, 207)",
                            "rgb(181, 147, 50)",
                            "rgb(232, 130, 81)",
                        ],

                        fill: false,
                        borderWidth: 1,
                        type: 'bar',
                        data: [
                            <?php $__currentLoopData = $data['statistical_products_day']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($item->data); ?>,
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        ],
                    },
                    {
                        label: 'số lượng sản phẩm',
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor:'rgb(255, 99, 132)',
                        fill: false,
                        type:"line",
                        data: [
                            <?php $__currentLoopData = $data['statistical_products_day']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($item->data); ?>,
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        ],
                    }
                ]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Số lượng sản phẩm được đăng xem theo tháng'
                },

                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 0,
                        bottom: 0
                    }
                },

                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Number'
                        },
                        ticks: {
                            stepSize: 1,
                        },
                    }]
                }
            }
        });


        $(document).ready(function($) {
            new Chart(document.getElementById("chart-days-in-month"), {
                // The type of chart we want to create
                type: 'bar',

                // The data for our dataset
                data: {
                    // type: 'category',
                    labels: ["1/12","2/12","3/12","4/12","5/12","6/12","7/12","8/12"],
                    datasets: [
                    {
                        label: "Total amount",
                        backgroundColor: 'rgba(225,0,0,0.4)',
                        borderColor: "rgb(231, 53, 253)",
                        borderCapStyle: 'square',
                        pointHoverRadius: 8,
                        pointHoverBackgroundColor: "yellow",
                        pointHoverBorderColor: "brown",
                        data: [0,1,5,4,3,5,2,0],
                        showLine: true, // disable for a single dataset,
                        yAxisID: "y-axis-gravity",
                        fill: false,
                        type: 'line',
                        lineTension: 0.1,
                    },
                    {
                        label: "Total order",
                        backgroundColor: 'rgb(138, 199, 214)',
                        borderColor: 'rgb(138, 199, 214)',
                        pointHoverRadius: 8,
                        pointHoverBackgroundColor: "brown",
                        pointHoverBorderColor: "yellow",
                        data: [0,1,5,7,4,3,9,0],
                        showLine: true, // disable for a single dataset,
                        yAxisID: "y-axis-density",
                        spanGaps: true,
                        lineTension: 0.1,
                        type: 'bar',
                    },

                    ]
                },

                // Configuration options go here
                options: {
                    responsive: true,
                    
                    layout: {
                        padding: {
                            left: 10,
                            right: 10,
                            top: 0,
                            bottom: 0
                        }
                    },
                    scales: {
                        yAxes: [
                        {
                          position: "left",
                          id: "y-axis-density",
                            ticks: {
                                beginAtZero:true,
                                max: 0 + 5,
                                min: 0,
                                stepSize: 2,
                            },
                              scaleLabel: {
                                 display: true,
                                 labelString: 'Total order',
                                 fontSize: 15,

                              }
                        },
                        {
                          position: "right",
                          id: "y-axis-gravity",
                          ticks: {
                                beginAtZero:true,
                            },
                            scaleLabel: {
                                 display: true,
                                 labelString: 'Total amount (VNĐ)',
                                 fontSize: 15
                              }
                        }
                        ]
                    },

                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                if (label) {
                                    label += ': ';
                                }
                                label += format_number(tooltipItem.yLabel);
                                return label;
                            }
                        }
                    }
                }
            });
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>