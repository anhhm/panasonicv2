<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',trans('admin_catproduct.news_header')); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo isset($data['news']) ? $data['news']['title'] : null; ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_catproduct.active')); ?></label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <?php if(App::isLocale('vi')): ?>
                                        <option value="0">Không</option>
                                        <option value="1">Có</option>
                                    <?php endif; ?>

                                    <?php if(App::isLocale('en')): ?>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    <?php endif; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_catproduct.code')); ?></label>
                                <input type="text" disabled name="code_vn" class="form-control" value="<?php echo old('code',isset($data['catproduct']) ? $data['catproduct']['code'] : null ); ?>">
                            </div>

                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#vn" data-toggle="tab">VietNam</a></li>
                                    <li><a href="#en" data-toggle="tab">English</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="vn">
                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_catproduct.title')); ?></label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_title_catproduct')); ?>" value="<?php echo old('title',isset($data['catproduct']) ? $data['catproduct']['title_vn'] : null ); ?>">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo e(trans('admin_catproduct.content')); ?></label>
                                            <textarea class="form-control" class="form-control" name="content_vn" cols="30" rows="4">
                                                <?php echo old('content',isset($data['catproduct']) ? $data['catproduct']['content_vn'] : null ); ?>

                                                <?php if($errors->has('content')): ?>
                                                    <p style="color:red"><?php echo e($errors->first('content')); ?></p>
                                                <?php endif; ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary"><?php echo e(trans('actionbtn.btnadd')); ?></button>
                                    <a type="button" class="btn btn-default" href="<?php echo e(url('admin/catproduct/list')); ?>"><?php echo e(trans('actionbtn.btnreturn')); ?></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('addScript'); ?>
    <!-- Select2 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.full.min.js"></script>
    <!-- Page script -->

    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
    </script>
    <!-- end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>