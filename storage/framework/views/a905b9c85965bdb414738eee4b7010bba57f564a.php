<?php $__env->startSection('content'); ?>
   <div class="row">
        <!--colleft-->
        <div class="col-md-8 col-sm-12">
            <div class="box-caption">
                <span>TECH</span>
            </div>
            <!--list-news-cate-->
            <div class="list-news-cate">
                <article class="news-cate-item  news-cate-item-first">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/6.jpg">
                            </a>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <h3><a href="#">7 essential lessons from agency marketing to startup growth</a></h3>
                            <div class="meta-post">
                                <a href="#">
                                    Sue Benson
                                </a>
                                <em></em>
                                <span>
                                    23 Sep 2016
                                </span>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. At enim sequor utilitatem. Quippe: habes enim a rhetoribus; Callipho ad virtutem nihil adiunxit nisi voluptatem.</p>
                        </div>
                    </div>
                </article>
                <?php for($i = 0; $i < 6; $i++): ?>
                    <article class="news-cate-item">
                        <div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                    <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/3.jpg">
                                </a>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <h3><a href="#">Donald Trump suggests the DNC was hacked by 'someone sitting on their bed that weighs 400 lbs'</a></h3>
                                <div class="meta-post">
                                    <a href="#">
                                        Sue Benson
                                    </a>
                                    <em></em>
                                    <span>
                                        22 Sep 2016
                                    </span>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. At enim sequor utilitatem. Quippe: habes enim a rhetoribus; Callipho ad virtutem nihil adiunxit nisi voluptatem.</p>
                            </div>
                        </div>
                    </article>
                <?php endfor; ?>
            </div>
            <!--paging-->
            <div class="paging">
                <a href="#">Prev</a>
                <a href="#">1</a>
                <a href="#" class="current">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a href="#">Next</a>
            </div>
        </div>

        <?php echo $__env->make('home.template.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>