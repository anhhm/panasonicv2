<link rel="stylesheet" type="text/css" href="<?php echo e(asset('lib/owl-coursel/owl.carousel.css')); ?>" media="screen"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('lib/owl-coursel/owl.theme.css')); ?>" media="screen"/>
<!--magnific popup-->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('lib/magnific-popup/magnific-popup.css')); ?>" media="screen"/>
<link rel="stylesheet" href="<?php echo e(asset('lib/fancybox-master/dist/jquery.fancybox.css')); ?>">
<!--bootstrap-->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('restaurant/css/bootstrap.min.css')); ?>" media="screen"/>
<!--awesome font icons-->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('restaurant/css/font-awesome.min.css')); ?>" media="screen"/>
<!--style-->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('restaurant/css/style.css')); ?>" media="screen"/>
<!--google font-->
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>

