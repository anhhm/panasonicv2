<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>CMS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>CMS</b> website</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <select class="form-control" name="locale" id="ControlSelectLanguage" style="width: 100%; text-align: right; margin: 10px 0px 0px 0px;">
                        <?php if(App::isLocale('vi')): ?>
                            <option value="vi" selected="">Viet Nam</option>
                        <?php else: ?>
                            <option value="vi">Vietnamese</option>
                        <?php endif; ?>

                        <?php if(App::isLocale('en')): ?>
                            <option value="en" selected="">English</option>
                        <?php else: ?>
                            <option value="en">English</option>
                        <?php endif; ?>
                        <input id="" type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                    </select>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo e(asset('backendTemp')); ?>/dist/img/avatar.png" width="160" height="160" class="user-image" alt="User Image">
                        <?php if(Auth::check()): ?>
                            <span class="hidden-xs"><?php echo e(Auth::user()->name); ?></span>
                        <?php endif; ?>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo e(asset('backendTemp')); ?>/dist/img/avatar.png" width="160" height="160" class="img-circle" alt="User Image">
                            <p>
                                <?php if(Auth::check()): ?>
                                    <?php echo e(Auth::user()->name); ?>

                                    <small>Member</small>
                                <?php endif; ?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="<?php echo e(route('logout')); ?>"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                    class="btn btn-default btn-flat"><?php echo e(trans('actionbtn.btnsingout')); ?></a>

                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                    <?php echo csrf_field(); ?>
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
