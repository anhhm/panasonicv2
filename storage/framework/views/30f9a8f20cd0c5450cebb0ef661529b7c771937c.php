<nav>
    <ul>
    	<li class="<?php echo e(Request::is('home') ? 'active' : ''); ?> <?php echo e(Request::is('/') ? 'active' : ''); ?>">
            <a href="<?php echo e(site_url('home')); ?>">Trang chủ</a>
        </li>
        <li class="<?php echo e(Request::is('about') ? 'active' : ''); ?>">
            <a href="<?php echo e(site_url('about')); ?>">Giới thiệu</a>
        </li>
        <li class="<?php echo e(Request::is('product') || Request::is('product/list/') ? 'active' : ''); ?>">
            <a href="<?php echo e(site_url('product')); ?>">Sản phẩm</a>
        </li>
        <li class="<?php echo e(Request::is('news') ? 'active' : ''); ?>">
            <a href="<?php echo e(site_url('news')); ?>">Tin tức</a>
        </li>
        <li class="<?php echo e(Request::is('contact') ? 'active' : ''); ?>">
            <a href="<?php echo e(site_url('contact')); ?>">Liên hệ</a>
        </li>
    </ul>
</nav>