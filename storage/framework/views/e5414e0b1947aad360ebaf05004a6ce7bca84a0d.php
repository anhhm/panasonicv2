<?php $__env->startSection('content'); ?>
   <div class="row">
        <!--colleft-->
        <div class="col-md-8 col-sm-12">
            <div class="box-caption">
                <span>spotlight</span>
            </div>
            <!--sportlight-->
            <section class="owl-carousel owl-spotlight">
                <?php for($i = 0; $i < 3; $i++): ?>
                    <div>
                        <article class="spotlight-item">
                            <div class="spotlight-img">
                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/1.jpg" class="img-responsive" />
                                <a href="<?php echo e(site_url('news/list')); ?>" class="cate-tag">Computing</a>
                            </div>
                            <div class="spotlight-item-caption">
                                <h2 class="font-heading">
                                    <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                        Windows 10 Now Running on 400 Million Active Devices, Says Microsoft
                                    </a>
                                </h2>
                                <div class="meta-post">
                                    <a href="#">
                                        Ashley Ford
                                    </a>
                                    <em></em>
                                    <span>
                                        26 Sep 2016
                                    </span>
                                </div>
                                <p>Microsoft had set an ambitious goal for itself of having Windows 10 running on 1 billion active devices by FY18 at the launch of its software platform. However, back in July, the company acknowledged that due to lack of traction in its smartphone business, it might take longer to achieve this target</p>
                            </div>
                        </article>
                    </div>
                <?php endfor; ?>
            </section>

            <!--spotlight-thumbs-->
            <section class="spotlight-thumbs">
                <div class="row">
                    <?php for($i = 0; $i < 3; $i++): ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="spotlight-item-thumb">
                                <div class="spotlight-item-thumb-img">
                                    <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                        <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/3.jpg" />
                                    </a>
                                    <a href="<?php echo e(site_url('news/list')); ?>" class="cate-tag">business</a>
                                </div>
                                <h3><a href="#">Donald Trump suggests the DNC was hacked by 'someone sitting on their bed that weighs 400 lbs'</a></h3>
                                <div class="meta-post">
                                    <a href="#">
                                        Sue Benson
                                    </a>
                                    <em></em>
                                    <span>
                                        22 Sep 2016
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </section>

            <!--box-news-->
            <section class="box-news">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="box-caption">
                            <span> BUSINESS</span>
                        </div>
                        <article class="news-two-large">
                            <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/6.jpg" />
                            </a>
                            <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">7 essential lessons from agency marketing to startup growth</a></h3>
                            <div class="meta-post">
                                <a href="#">
                                    Sue Benson
                                </a>
                                <em></em>
                                <span>
                                    23 Sep 2016
                                </span>
                            </div>
                            <p>Based on my own experience of going from a digital agency to a start-up, these are the principles I would have taught myself, ...</p>
                        </article>
                        <ul class="list-news-popular">
                            <?php for($i = 0; $i < 3; $i++): ?>
                                <li>
                                    <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                        <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/8.jpg">
                                    </a>
                                    <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">Netflix Speeds Jumped 51% This Year</a></h3>
                                    <div class="meta-post">
                                        <a href="#">
                                            Ashley Ford

                                        </a>
                                        <em></em>
                                        <span>
                                            24 Aug  2016
                                        </span>
                                    </div>
                                </li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="box-caption">
                            <span> COMPUTING</span>
                        </div>
                        <article class="news-two-large">
                            <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/7.jpg" />
                            </a>
                            <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">400 million machines are now running Windows 10</a></h3>
                            <div class="meta-post">
                                <a href="#">
                                    Ashley Ford
                                </a>
                                <em></em>
                                <span>
                                    26 Sep 2016
                                </span>
                            </div>
                            <p>Microsoft is revealing today that 400 million active machines are now running Windows 10. The latest update on usage comes just three months</p>
                        </article>
                        <ul class="list-news-popular">
                            <?php for($i = 0; $i < 3; $i++): ?>
                                <li>
                                    <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                        <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/12.jpg">
                                    </a>
                                    <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">Mozilla trolls the EU’s nonsensical copyright laws with classic memes</a></h3>
                                    <div class="meta-post">
                                        <a href="#">
                                            Super User
                                        </a>
                                        <em></em>
                                        <span>
                                            26 Sep 2016
                                        </span>
                                    </div>
                                </li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            </section>

            <!--box-news-->
            <section class="box-news">
                <div class="box-caption">
                    <span> REVIEW</span>
                </div>
                <div class="owl-carousel owl-review">
                    <?php for($i = 0; $i < 3; $i++): ?>
                        <div>
                            <article class="review-item">
                                <div class="review-img">
                                    <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/16.jpg" class="img-responsive" />
                                </div>
                                <div class="review-item-caption">
                                    <h3 class="font-heading">
                                        <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                            Microsoft talks Project Scorpio and the end of the console generation
                                        </a>
                                    </h3>
                                    <div class="meta-post">
                                        <a href="#">
                                            Super User
                                        </a>
                                        <em></em>
                                        <span>
                                            19 Sep 2016
                                        </span>
                                    </div>
                                </div>
                            </article>
                        </div>
                    <?php endfor; ?>
                </div>
            </section>

            <!--box-news-->
            <section class="box-news">
                <div class="box-caption">
                    <span> Mobile</span>
                </div>
                <article class="news-two-large news-three-large">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/4.jpg" />
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3 class="title-fix-margin"><a href="#">Six big ways MacOS Sierra is going to change your Apple experience</a></h3>
                            <div class="meta-post">
                                <a href="#">
                                    Marion  Craig
                                </a>
                                <em></em>
                                <span>
                                    21 Sep 2016
                                </span>
                            </div>
                            <p>Lacinia faucibus consectetuer vestibulum elit quisque at et ultrices sed libero. Ut nam eu nunc enim curabitur non metus eu turpis eget. Aenean laoreet vitae morbi vestibulum vestibulum sociis tellus interdum cras.</p>
                        </div>
                    </div>
                </article>

                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul class="list-news-popular">
                            <?php for($i = 0; $i < 3; $i++): ?>
                                <li>
                                    <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                        <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/13.jpg">
                                    </a>
                                    <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">Tumblr brings Apple’s ‘Live Photos’ to the web</a></h3>
                                    <div class="meta-post">
                                        <a href="">
                                            Super User
                                        </a>
                                        <em></em>
                                        <span>
                                            15 Sep 2016
                                        </span>
                                    </div>
                                </li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul class="list-news-popular">
                            <?php for($i = 0; $i < 3; $i++): ?>
                                <li>
                                    <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                        <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/9.jpg">
                                    </a>
                                    <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">Uber wants to build planes to beat city traffic</a></h3>
                                    <div class="meta-post">
                                        <a href="#">
                                            Karla   Walters
                                        </a>
                                        <em></em>
                                        <span>
                                            04 Sep  2016
                                        </span>
                                    </div>
                                </li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            </section>
        </div>

        <?php echo $__env->make('home.template.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>