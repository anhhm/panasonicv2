<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>restaurant</title>
      <link rel="icon" href="favicon.ico">
      <!--owl carousel-->

        <?php echo $__env->yieldContent('addCss'); ?>
        <?php echo $__env->make('home.template.linkcss', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </head>
    <body>
        <?php echo $__env->make('home.template.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->make('home.template.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('addjs'); ?>
        <?php echo $__env->make('home.template.linkjs', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body>
</html>