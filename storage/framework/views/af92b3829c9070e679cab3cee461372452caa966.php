<?php $__env->startSection('content'); ?>
    <!--wrap-content-->
    <div id="wrap-body">
        <!--banner-->
        <div class="heading-group page-name color-w">
            <h1>Nội dung</h1>
            <ul class="becku">
                <li><a href="">Home</a></li>
                <li><a href="">blog</a></li>
                <li>blog detail</li>
            </ul>
            <div class="star-group">
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o star-1x"></i>
                <i class="fa fa-star-o"></i>
            </div>
        </div>
        <!--/banner-->
        <div class="container" id="main-content">
            <div class="row">
                <div class="col-md-8">  
                    <!--blog detail-->
                    <?php if(isset($data['detail'])): ?>
                        <div class="blog blog-detail">
                            <div class="thumbnail">
                                <img src="<?php echo e($data['detail'][0]->img_avatar); ?>" alt="image">
                                <div class="date">
                                    <span><?php echo e($data['detail'][0]->created_at); ?>}</span>
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($data['detail'][0]->title_vn); ?></h2>
                                    <div class="block_detail">
                                        <?php echo $data['detail'][0]->detail_vn; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!--/blog detail-->
                </div>
                <div class="col-md-4">
                    <!--relate blog-->
                    <div class="blog relate-blog">
                        <!--blog-item-->
                        <?php if(isset($data['list_cat'])): ?>
                            <?php $__currentLoopData = $data['list_cat']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="thumbnail">
                                    <a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>" class="hover-img">
                                        <img src="<?php echo e($item->img_avatar); ?>" alt="image">
                                    </a>
                                    <div class="date">
                                        <span><?php echo e($item->created_at); ?></span>
                                    </div>
                                    <div class="caption">
                                        <h3><a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                        <p>
                                            <?php echo e($item->content_vn); ?>

                                            <a class="readmore" href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>">Xem tiếp</a>
                                        </p>

                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <p>chưa có tin liên quan</p>
                        <?php endif; ?>
                        <!--/blog-item-->
                    </div>
                    <!-- /relate blog-->
                </div>
            </div>
        </div>
    </div>
    <!--/wrap-content-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>