
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <?php if(Auth::check()): ?>
            <?php $group_id=Auth::user()->group_id;?>
        <?php endif; ?>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu</li>
            <li class="active">
                <a href="<?php echo e(url('admin')); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo e(trans('admin_menu.dashboard')); ?></span>
                </a>
            </li>
            
            <li class="treeview">
                <a>
                    <i class="fa fa-newspaper-o"></i>
                    <span><?php echo e(trans('admin_menu.news')); ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('admin/news/add')); ?>"><i class="fa fa-edit"></i> <?php echo e(trans('admin_menu.addnews')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/news/list')); ?>"><i class="fa fa-list-alt"></i><?php echo e(trans('admin_menu.listnews')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/cat_news/list')); ?>"><i class="fa  fa-indent"></i> <?php echo e(trans('admin_menu.catnews')); ?></a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a>
                    <i class="fa fa-file-picture-o"></i>
                    <span><?php echo e(trans('admin_menu.library')); ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('admin/library/list')); ?>"><i class="fa fa-file-picture-o"></i><?php echo e(trans('admin_menu.listlibrary')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/cat_library/list')); ?>"><i class="fa fa-indent"></i> <?php echo e(trans('admin_menu.catlibrary')); ?></a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a>
                    <i class="fa fa-cube"></i>
                    <span><?php echo e(trans('admin_menu.product')); ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('admin/products/add')); ?>"><i class="fa fa-edit"></i> <?php echo e(trans('admin_menu.addproduct')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/products/list')); ?>"><i class="fa fa-cube"></i><?php echo e(trans('admin_menu.listproduct')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/cat_products/list')); ?>"><i class="fa fa-cubes"></i> <?php echo e(trans('admin_menu.catproduct')); ?></a></li>
                </ul>
            </li>
            
            
                
                    
                    
                    
                        
                    
                
                
                    
                
            
            
            <li class="treeview">
                <a href="<?php echo e(url('admin/user/list')); ?>">
                    <i class="fa fa-user"></i>
                    <span><?php echo e(trans('admin_menu.user')); ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('admin/users/add')); ?>"><i class="fa fa-user-plus"></i> <?php echo e(trans('admin_menu.adduser')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/users/list')); ?>"><i class="fa fa-user-md"></i><?php echo e(trans('admin_menu.listuser')); ?></a></li>
                    
                </ul>
            </li>
            <li class="treeview">
                <a>
                    <i class="fa fa-book"></i>
                    <span><?php echo e(trans('admin_menu.guide')); ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('admin/guide/news')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideNews')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/guide/library')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideLibrary')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/guide/product')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideProduct')); ?></a></li>
                    <li><a href="<?php echo e(url('admin/guide/admin')); ?>"><i class="fa fa-hand-o-right"></i> <?php echo e(trans('admin_menu.guideAdmin')); ?></a></li>

                </ul>
            </li>

        </ul>
        <!-- /.sidebar menu -->
    </section>
    <!-- /.sidebar -->
</aside>
