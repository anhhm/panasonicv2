<?php $__env->startSection('category',request()->segment(2)); ?>
<?php $__env->startSection('action',trans('admin_user.user_header')); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo isset($data['user']) ? $data['user']['name'] : null; ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post">
                        <div class="box-body">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo e(trans('admin_user.name')); ?>:</label>
                                <input name="name" type="text" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.place_name')); ?>" value="<?php echo isset($data['user']) ? $data['user']['name'] : null; ?>">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo e(trans('admin_user.email')); ?>:</label>
                                <input name="email" type="text" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.place_email')); ?>" value="<?php echo isset($data['user']) ? $data['user']['email'] : null; ?>">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo e(trans('admin_user.pass')); ?>:</label>
                                <input name="pass" type="password" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.place_password')); ?>" value="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo e(trans('admin_user.repass')); ?>:</label>
                                <input name="repasss" type="password" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.place_repassword')); ?>" value="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo e(trans('admin_user.active')); ?>:</label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <?php if(App::isLocale('vi')): ?>
                                        <option value="0" <?php echo e($data['user']['active']=="0"? 'selected':''); ?>>Không</option>
                                        <option value="1" <?php echo e($data['user']['active']=="1"? 'selected':''); ?>>Có</option>
                                    <?php endif; ?>

                                    <?php if(App::isLocale('en')): ?>
                                        <option value="0" <?php echo e($data['user']['active']=="0"? 'selected':''); ?>>No</option>
                                        <option value="1" <?php echo e($data['user']['active']=="1"? 'selected':''); ?>>Yes</option>
                                    <?php endif; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo e(trans('admin_user.group')); ?>:</label>
                                <select class="form-control select2" name="id_group" data-placeholder="chon quyền quản trị website" style="width: 100%;">
                                    <?php $__currentLoopData = $data['group_user']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item['id']); ?>" <?php echo e($data['user']['group_id']== $item['id'] ? 'selected':''); ?>><?php echo e($item['title']); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a type="button" class="btn btn-default" href="<?php echo e(url('admin/user/list')); ?>">Trở về</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('addScript'); ?>
    <!-- Select2 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.full.min.js"></script>
    <!-- CK Editor -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/ckeditor/ckeditor.js"></script>
    <!-- Page script -->

    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
        CKEDITOR.replace( 'editor1' );
    </script>
    <!-- end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>