<!-- Online Fonts -->
<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700&subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
<!-- Vender -->
<link href="<?php echo e(asset('teznews')); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo e(asset('teznews')); ?>/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo e(asset('teznews')); ?>/css/normalize.min.css" rel="stylesheet" />
<link href="<?php echo e(asset('teznews')); ?>/css/owl.carousel.min.css" rel="stylesheet" />
<!-- Main CSS (SCSS Compile) -->
<link href="<?php echo e(asset('teznews')); ?>/css/main.css" rel="stylesheet" />
