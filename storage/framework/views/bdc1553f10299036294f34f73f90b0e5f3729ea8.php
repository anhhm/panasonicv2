<?php $__env->startSection('content'); ?>
   <!--wrap-content-->
    <div id="wrap-body">
        <!--banner-->
        <div class="heading-group page-name color-w">
            <h1>Thư viện</h1>
            <ul class="becku">
                <li><a href="#">Home</a></li>
                <li>Blog</li>
            </ul>
            <div class="star-group">
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o star-1x"></i>
                <i class="fa fa-star-o"></i>
            </div>
        </div>
        <!--/banner-->
        <div class="container" id="main-content">
            <div class="blog list-blog gallery">
                <div class="row">
                        <?php if(isset($data['list_img'])): ?>
                            <?php $__currentLoopData = $data['list_img']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-6">
                                    <div class="thumbnail" style="height: 400px; overflow: hidden;">
                                        <a data-fancybox="{$item->code}" rel="{$item->code}" href="<?php echo e($item->img_avatar); ?>" class="hover-img">
                                            <img style="height: 100%;" src="<?php echo e($item->img_avatar); ?>" alt="image">
                                        </a>
                                        <div class="date">
                                            <span><?php echo e(date('d-m-Y', strtotime($item->img_avatar))); ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <p>Danh sách trống</p>
                        <?php endif; ?>
                    <!--blog-->
                </div>
            </div>
        </div>
    </div>
    <!--/wrap-content-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>