<meta property="og:title" content="<?php echo e(isset($data['SEO']['title']) ? $data['SEO']['title'] : 'Nhà phân phối sản phẩm điều hòa Panasonic hàng đầu việt nam'); ?>" />  
<meta property="og:site_name" content="Nhà phân phối sản phẩm điều hòa Panasonic hàng đầu việt nam" />  
<meta property="og:description" content="<?php echo e(isset($data['SEO']['content']) ? $data['SEO']['content'] : 'Máy lạnh điều hòa'); ?>" />
<meta name="keywords" content="<?php echo e(isset($data['SEO']['keyword']) ? $data['SEO']['keyword'] : 'Máy lạnh điều hòa'); ?> "/>
<meta name="description" content="<?php echo e(isset($data['SEO']['content']) ? $data['SEO']['content'] : 'Máy lạnh điều hòa'); ?>"/>
<link rel="image_src" href="" />
<meta property="og:image" href="" />