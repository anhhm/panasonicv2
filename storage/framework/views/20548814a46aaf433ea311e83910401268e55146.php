<?php $__env->startSection('category',"nhóm quyền"); ?>
<?php $__env->startSection('action',"Danh sách"); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('lib/checkbox/dist/css/bootstrap3/bootstrap-switch.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="dataTables-example" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Tên Group</th>
                                    <th>Tên Folder</th>
                                    <th>Quyền Xem</th>
                                    <th>Quyền Thêm</th>
                                    <th>Quyền Sửa</th>
                                    <th>Quyền Xóa</th>
                                    <th>
                                        <a href="<?php echo e(site_url('admin/permissiongroup/add')); ?>" class="btn btn-success">Thêm</a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="line-height:30px"><?php echo e($item['name']); ?></td>
                                    <td style="line-height:30px">
                                        <?php $__currentLoopData = $item['permission']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php echo e($kq['permission_name']); ?></br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </td>
                                    <td style="line-height:30px">
                                        <?php $__currentLoopData = $item['permission']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <input class="form-control" type="checkbox" name="product" width="35px" data-size="mini" disabled
                                                    <?php echo e($kq['action_view']==1 ? 'checked' : ''); ?> >
                                            </br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </td>
                                    <td style="line-height:30px">
                                        <?php $__currentLoopData = $item['permission']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <input class="form-control" type="checkbox" name="product"  data-size="mini" disabled
                                                    <?php echo e($kq['action_add']==1 ? 'checked' : ''); ?> >
                                            </br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </td>
                                    <td style="line-height:30px">
                                        <?php $__currentLoopData = $item['permission']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <input class="form-control" type="checkbox" name="product" data-size="mini" disabled
                                                    <?php echo e($kq['action_edit']==1 ? 'checked' : ''); ?> >
                                            </br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </td>
                                    <td style="line-height:30px">
                                        <?php $__currentLoopData = $item['permission']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <input class="form-control" type="checkbox" name="product" data-size="mini" disabled
                                                    <?php echo e($kq['action_delete']==1 ? 'checked' : ''); ?> >
                                            </br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Chức năng
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <i class="fa fa-pencil fa-fw" style="margin-left:10px"></i> <a href="<?php echo e(url('admin/permissiongroup/edit/'.$item['id'])); ?>" style="display: inline-block;width: 75%;">Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('addScript'); ?>
    <!-- checkbox -->
    <script src="<?php echo e(asset('lib/checkbox/dist/js/bootstrap-switch.js')); ?>"></script>
    <script>
        $(function(argument) {
            $('[type="checkbox"]').bootstrapSwitch();
        })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>