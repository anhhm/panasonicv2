<?php $__env->startSection('content'); ?>
    <!--wrap-content-->
    <div id="wrap-body">
        <!-- banner -->
        <div class="heading-group page-name color-w">
            <h1>Thực đơn</h1>
            <ul class="becku">
                <li><a href="#">Home</a></li>
                <li>menu</li>
            </ul>
            <div class="star-group">
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o star-1x"></i>
                <i class="fa fa-star-o"></i>
            </div>
        </div>
        <!-- /banner -->
        <div class="container" id="main-content">
            <!-- resaurent menu -->
            <div class="mn">
                <?php echo $data['list_menu']; ?>

            </div>
            <!-- /resaurent menu -->
        </div>
    </div>
    <!--wrap-content-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>