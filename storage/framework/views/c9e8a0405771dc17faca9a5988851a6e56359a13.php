<!--header-->
    <header class="header">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="#" class="logo">
                    <img alt="Logo" src="<?php echo e(asset('teznews')); ?>/images/logo.png" />
                </a>
            </div>
            <div class="col-md-6 col-md-offset-3 col-sm-8  text-right col-xs-12 hidden-xs">
                <div class="owl-carousel owl-special">
                    <div>
                        <div class="special-news">
                            <a href="#">
                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/17.jpg" />
                            </a>
                            <h3><a href="#">Apple iPhone 7 review </a></h3>
                            <div class="meta-post">
                                <a href="#">
                                    Ashley Ford
                                </a>
                                <em></em>
                                <span>
                                    21 Sep 2016
                                </span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="special-news">
                            <a href="#">
                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/18.jpg" />
                            </a>
                            <h3><a href="#">YouTube Go is a new app for offline viewing and sharing </a></h3>
                            <div class="meta-post">
                                <a href="#">
                                    Super User
                                </a>
                                <em></em>
                                <span>
                                    25 Sep 2016
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>