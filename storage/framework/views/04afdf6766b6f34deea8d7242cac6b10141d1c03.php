<header id="header" class="nav-down">
    <div class="header-middle-area ptb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="logo">
                        <a href="<?php echo e(site_url('home')); ?>">
                            <img src="<?php echo e(asset('shop')); ?>/images/Panasonic-Logo.png" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6 col-sm-8 col-xs-12">
                    <div class="search-box">
                        <form action="<?php echo e(site_url('search')); ?>" method="get">
                            <input type="text" name="product" placeholder="Tìm kiếm sản phẩm..."/>
                            <button><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom bg-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                    <div class="categories-menu text-uppercase click bg-7">
                        <i class="fa fa-list-ul"></i>
                        <span>Danh Mục điều hòa</span>
                    </div>
                    <div class="menu-container toggole">
                        <ul>
                            <?php echo $data['menu_header']; ?>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-7 hidden-xs hidden-sm">
                    <div class="mainmenu hover-bg dropdown">
                        <?php echo $__env->make('home.template.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobail-menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 hidden-lg hidden-md col-sm-6">
                        <div class="mobail-menu-active">
                            <?php echo $__env->make('home.template.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>