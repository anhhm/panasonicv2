<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',''); ?>
<?php $__env->startSection('addCss'); ?>
    <style>
        fieldset {
            width: 90%;
            margin: 10px auto !important;
            border: 1px solid #222d32 !important;
            padding: 10px !important;
        }
        fieldset legend {
            width: auto !important;
            color: #3c8dbc !important;
            padding: 0 15px !important;
            font-size: 18px !important;
            font-weight: bold !important;
            border: none !important;
            margin-bottom: 0px !important;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section class="content" style="margin-top: 20px;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Hướng dẫn sử dụng module thư viện</h3>
                    </div>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#news" data-toggle="tab">Quản lí hình ảnh</a></li>
                            <li><a href="#cat_news" data-toggle="tab">Quản lí danh mục hình ảnh</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="news">
                                <div class="content" style="font-size: 14px; font-family: tahoma; line-height: 25px;">
                                    <h5>Chào mừng bạn đến với module quản lí tin tức của hệ thống CMS Laravel phiên bản 1.0.1</h5>
                                    <p>Sau đây là cách hướng dẫn sử dụng và quản lí tin tức trên website </p>
                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>1. Thêm mới 1 hình ảnh vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để thêm m1 hình ảnh vào tool quản lí bạn làm theo các bước sau.</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào mục <b>danh sách ảnh</b> trên menu bên trái</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-library-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các hình ảnh bạn click chọn thêm để thêm hình ảnh vào</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-library-2b.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi thực hiện <b>2 bước trên</b> bạn thực hiện điền thông tin trong các trường thên ảnh.</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-library-2c.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 4: </strong>Khi bạn click vào box ảnh đại diện sẽ xuất hiện khung upload ảnh.
                                        Tại đây bạn click vào <b>Tạo thư mục con</b> để tạo fordel quản lí ảnh của mình ( hãy nghĩ răng mỗi folder là 1 danh sách các ảnh cùng 1 danh mục )</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/upload-librarya.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 5: </strong>Sau khi bạn tạo folder ảnh bạn đặt tên ( tên nên trùng với danh mục ảnh để tiện cho việc quản lí của bạn)</p>
                                    <p style="text-align: center">
                                        <img width="300px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/upload-libraryb.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 6: </strong>Sau khi dặt tên folder bạn click vào tên folder mình tạo trên khung để tiến hành upload ảnh vào</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/upload-libraryc.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 7: </strong>Sau khi <b>Tải lên</b> hình ảnh bạn chọn dưới máy tính của mình lên., hệ thống sẽ lưu lại file ảnh của bạn trên website </p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/upload-libraryd.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 8: </strong>sau khi đã upload ảnh bận click vào ảnh mình chọn. Như vây bạn đã hoàn thành việc upload ảnh.</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/upload-library2.jpg')); ?>" alt="">
                                    </p>
                                    <fieldset class="fieldset-note"><legend>Lưu ý</legend>
                                        <ul>
                                            <li style="text-align: justify;"><b>Tiêu đề và ảnh đại diện</b> không được bỏ trống.</li>
                                            <li style="text-align: justify;"><b>Trường Danh mục</b> và <b>Trường kích hoạt</b> nếu bạn không chọn thì nó sẽ lấy danh mục đầu tiên là phần khi bạn lưu.</li>
                                            <li style="text-align: justify;">Mỗi <b>thư mục con</b> có thể <b>chứa tối da 10 hình ảnh</b>.</li>
                                        </ul>
                                    </fieldset>

                                    <p>Sau khi đã thêm hết nội dung vào các trường bạn click vào button <b>Thêm</b> ở cuối trang. Nếu bạn không muốn lưu bài viết này thì click vào <b>trở về</b></p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/save.jpg')); ?>" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>2. Chỉnh sửa 1 Ảnh vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để chỉnh sửa 1 ảnh trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách ảnh trên menu bên trái để vào trang danh sách ảnh</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-library-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các ảnh, bạn chọn 1 ảnh sau đó click vào chức năng chọn chỉnh sửa để tiển hành chỉnh sửa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/update-library.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi chọn ảnh cần sửa bạn tiến hành sửa tin theo các trường</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/update-library1.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 4: </strong>Khi bạn click vào box ảnh đại diện để chỉnh sửa sẽ xuất hiện khung upload ảnh.
                                        Tại đây bạn chọn folder chứa ảnh or nếu chưa có bạn tạo mới vào. bạn tiến hành upload ảnh tương tự các bước 4, 5, 6, 7, 8</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/upload-libraryd.jpg')); ?>" alt="">
                                    </p>
                                    <p>Sau khi đã thêm hết nội dung vào các trường bạn click vào button <b>Thêm</b> ở cuối trang. Nếu bạn không muốn lưu bài viết này thì click vào <b>trở về</b></p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/save.jpg')); ?>" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>3. Xóa 1 Ảnh vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để Xóa 1 ảnh trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách ảnh trên menu bên trái để vào trang danh sách tin</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-library-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các ảnh, bạn chọn 1 ảnh sau đó click vào chức năng chọn <b>Xóa</b> để tiển hành xóa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/delete-library.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi click xóa, hệ thống sẽ hỏi lại bạn 1 lần nữa có xóa hay không ? nếu chắc chắn <b>xóa</b> thì bạn click ok còn không thì bạn chọn hủy</p>
                                    <p style="text-align: center">
                                        <img width="400px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/delete-news1.jpg')); ?>" alt="">
                                    </p>
                                    <p>Trên đây là hướng dẫn sử dụng tool quản lí, cảm ơn bạn đã đọc</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="cat_news">
                                <div class="content" style="font-size: 14px; font-family: tahoma; line-height: 25px;">
                                    <h5>Chào mừng bạn đến với module quản lí tin tức của hệ thống CMS Website phiên bản 1.0.1</h5>
                                    <p>Sau đây là cách hướng dẫn sử dụng và quản lí tin tức trên website </p>
                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>1. Thêm mới 1 danh mục ảnh vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để thêm mới 1 danh mục Hình ảnh bạn thực hiện theo các bước sau.</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách tin tức trên menu bên trái  để vào trang danh sách ảnh</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catlib-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong> Trên danh sách các danh mục ảnh, bạn click chọn thêm để thêm mới danh mục vào</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catlib-2b.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp; <strong>Bước 3: </strong>Sau khi thực hiện <b>2 bước trên</b> bạn thực hiện điền thông tin trong các trường thên trang.</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catlib-2c.jpg')); ?>" alt="">
                                    </p>
                                    <fieldset class="fieldset-note"><legend>Lưu ý</legend>
                                        <ul>
                                            <li style="text-align: justify;"><b>Trường tên danh mục, Nội dung</b> không được bỏ trống.</li>
                                            <li style="text-align: justify;"><b>Trường Danh mục</b> và <b>Trường trạng thái</b> nếu bạn không chọn thì nó sẽ lấy danh mục đầu tiên là phần khi bạn lưu.</li>
                                        </ul>
                                    </fieldset>

                                    <p>Sau khi đã thêm hết nội dung vào các trường bạn click vào button <b>Thêm</b> ở cuối trang. Nếu bạn không muốn lưu danh mục này thì click vào trở về</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/save.jpg')); ?>" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>2. Chỉnh sửa 1 danh mục ảnh vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để chỉnh sửa 1 tin tức trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách danh mục trên menu bên trái để vào trang danh sách ảnh</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catlib-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các danh mục ảnh, bạn chọn 1 danh mục sau đó click vào chức năng chọn chỉnh sửa để tiển hành chỉnh sửa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/update-catnews.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi chọn danh mục cần sửa bạn tiến hành sửa tin theo các trường</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/update-catlib1.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 4: </strong>Khi đã điền hết thông tin bạn cần chỉnh sửa thì bạn click button <b>Thêm</b> để lưu hoặc click vào button <b>Trở về</b> đẻ không lưu bài viết</p>
                                    <p style="text-align: center">
                                        <img width="200px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/save.jpg')); ?>" alt="">
                                    </p>

                                    <h5 style="text-align: justify; background-color: #00ff66; padding: 5px 10px;">
                                        <strong>3. Xóa 1 danh mục vào tool quản lí.</strong><br>
                                    </h5>
                                    <p>Để Xóa 1 danh mục trong tool quản lí bạn tiến hành làm theo các bước sau đây:</p>
                                    <p>&nbsp;&nbsp;<strong>Bước 1: </strong>Bạn click chuột vào Danh sách danh mục trên menu bên trái để vào trang danh sách danh mục ảnh</p>
                                    <p style="text-align: center">
                                        <img style="margin: 0px auto" src="<?php echo e(asset('imageguide/add-catlib-2a.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 2: </strong>Trên danh sách các danh mục, bạn chọn 1 tin sau đó click vào chức năng chọn <b>Xóa</b> để tiển hành xóa</p>
                                    <p style="text-align: center">
                                        <img width="800px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/delete-catlib.jpg')); ?>" alt="">
                                    </p>
                                    <p>&nbsp;&nbsp;<strong>Bước 3: </strong>Sau khi click xóa, hệ thống sẽ hỏi lại bạn 1 lần nữa có xóa hay không ? nếu chắc chắn <b>xóa</b> thì bạn click ok còn không thì bạn chọn hủy</p>
                                    <p style="text-align: center">
                                        <img width="400px" style="margin: 0px auto" src="<?php echo e(asset('imageguide/delete-news1.jpg')); ?>" alt="">
                                    </p>
                                    <p>Trên đây là hướng dẫn sử dụng tool quản lí, cảm ơn bạn đã đọc</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>