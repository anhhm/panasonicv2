<!-- All css files are included here. -->
<!-- animate css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/animate.min.css">
<!-- Bootstrap fremwork main css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/bootstrap.min.css">
<!-- font-awesome css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/font-awesome.min.css">
<!-- nivo-slider css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/nivo-slider.css">
<!-- owl carousel css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/owl.carousel.min.css">
<!-- icofont css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/icofont.css">
<!-- meanmenu css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/meanmenu.css">
<!-- jquery-ui css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/jquery-ui.min.css">
<!-- magnific-popup css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/magnific-popup.css">
<!-- percircle css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/percircle.css">
<!-- Theme main style -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css//style.css?v=1">
<!-- Responsive css -->
<link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/responsive.css?v=1">

<!-- Modernizr JS -->
<script src="<?php echo e(asset('shop')); ?>/js/modernizr-2.8.3.min.js"></script>