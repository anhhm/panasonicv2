<?php $__env->startSection('content'); ?>
	<div id="wrap-body">
		<!--banner-->
		<div class="heading-group page-name color-w">
			<h1>Liên hệ</h1>
			<ul class="becku">
				<li><a href="#">Home</a></li>
				<li>Contact</li>
			</ul>
			<div class="star-group">
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o star-1x"></i>
				<i class="fa fa-star-o"></i>
			</div>
		</div>
		<!-- /banner-->
		<div class="container" id="main-content">
			<div class="row">
				<div class="col-md-6">
					<!--contact-->
					<div class="contact">
						<div class="addr">
							<div class="title"><h3>Liên hệ</h3></div>
							<p>Không cần phải đến Singapore mà bạn hãy đến với Nhà Hàng Cua Ớt Singapore để thưởng thức ẩm thực Singapore,
								nổi tiếng với món cua sốt ớt được chế biến từ nhóm đầu bếp chuyên nghiệp đến từ Singapore cùng với sự phục vụ nhiệt tình ,chu đáo.
								Nơi có nhiều không gian lí tưởng để tiếp khách với các phòng VIP sang trọng, tiệc liên hoan, sinh nhật, gặp mặt và tất niên cuối năm.&nbsp;</p>
							<ul class="list-ul">
								<li><i class="fa fa-home"></i>Lô 2-A10 Võ Văn Kiệt, An Hải Đông, Sơn Trà, Đà Nẵnga</li>
								<li><i class="fa fa-phone"></i>0966 778 117</li>
								<li><i class="fa fa-envelope"></i>danang@chilli-crab.com</li>
							</ul>
						</div>
						<form action="" method="POST">
							<div class="title"><h3>Form liên hệ</h3></div>
							<div class="form-group">
								<input type="text" class="form-control form-item" name="name" placeholder="Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control form-item" name="phone" placeholder="Phone">
							</div>
							<div class="form-group">
								<input type="text" class="form-control form-item" name="email" placeholder="Email">
							</div>
							<div class="form-group">
								<textarea class="form-control form-item" rows="3" name="content" placeholder="Content"></textarea>
							</div>
							<button type="submit" class="btn btn-1">Gửi</button>
						</form>
					</div>
					<!--/contact-->
				</div>
				<div class="col-md-6">
					<!--googlemap-->
					<div id="googlemap">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3834.0608341834695!2d108.23537112380372!3d16.06233266019499!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7d4c2104a3d18659!2zTmjDoCBow6BuZyBDdWEg4buadCBTaW5nYXBvcmU!5e0!3m2!1svi!2s!4v1514001314972" height="870" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>