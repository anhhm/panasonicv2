<!-- Placed js at the end of the document so the pages load faster -->
<!-- jquery latest version -->
<script src="<?php echo e(asset('shop')); ?>/js/jquery-1.12.4.min.js"></script>


<!-- magnific popup js -->
<script src="<?php echo e(asset('shop')); ?>/js/jquery.magnific-popup.min.js"></script>
<!-- mixitup js -->

<script src="<?php echo e(asset('shop')); ?>/js/jquery.mixitup.min.js"></script>
<!-- jquery-ui price-->
<script src="<?php echo e(asset('shop')); ?>/js/jquery-ui.min.js"></script>
<!-- ScrollUp Js -->
<script src="<?php echo e(asset('shop')); ?>/js/jquery.scrollUp.min.js"></script>
<!-- nivo slider js -->
<script src="<?php echo e(asset('shop')); ?>/js/jquery.nivo.slider.pack.js"></script>
<!-- mobail menu js -->
<script src="<?php echo e(asset('shop')); ?>/js/jquery.meanmenu.js"></script>
<!-- Bootstrap framework js -->
<script src="<?php echo e(asset('shop')); ?>/js/bootstrap.min.js"></script>
<!-- owl carousel js -->
<script src="<?php echo e(asset('shop')); ?>/js/owl.carousel.min.js"></script>
<!-- All js plugins included in this file. -->
<script src="<?php echo e(asset('shop')); ?>/js/plugins.js"></script>
<!-- Main js file that contents all jQuery plugins activation. -->
<script src="<?php echo e(asset('shop')); ?>/js/main.js"></script>
