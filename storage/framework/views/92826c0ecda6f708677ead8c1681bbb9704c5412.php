<?php $__env->startSection('content'); ?>
	<div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="woocommerce-breadcrumb mtb-15">
                        <div class="menu">
                            <ul>
                                <li><a href="<?php echo e(site_url('home')); ?>">Trang chủ</a></li>
                                <li class="active"><a href="<?php echo e(site_url('news')); ?>">Giới thiệu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <!-- categories-area start -->
                    <div class="categories-area box-shadow bg-fff">
                        <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                            <i class="fa fa-bookmark icon bg-4"></i>
                            <h3>Tin cùng chuyên mục</h3>
                        </div>
                        <div class="shop-categories-menu p-20">
                            <ul>
                                <?php if(isset($data['cat_news'])): ?>
                                    <?php $__currentLoopData = $data['cat_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>" title="<?php echo e($item->title_vn); ?>"><?php echo e(str_limit($item->title_vn, 28)); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>

                    <!-- featured-area start -->
                    <?php if(isset($data['list_product_new'])): ?>
                        <div class="featured-area bg-fff box-shadow mtb-30">
                            <div class="product-title home2-bg-1 text-uppercase home2-product-title">
                                <i class="fa fa-bookmark icon bg-4"></i>
                                <h3>Sản phẩm nổi bật</h3>
                            </div>
                            <div class="featured-wrapper p-20">
                                <?php $__currentLoopData = $data['list_product_new']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="product-wrapper single-featured bb mb-10">
                                        <div class="product-img  floatleft">
                                            <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                            </a>
                                        </div>
                                        <div class="product-content floatright">
                                            <h3><a href="#"><?php echo e(str_limit($item->title_vn, 20)); ?></a></h3>
                                            <span><?php echo e(number_format($item->price_mb,'0','.','.')); ?> VND</span>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<?php echo $__env->yieldContent('contentNews'); ?>
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>