<!-- footer-area start -->
<footer class="bg-fff bt">
    <div class="footer-top-area ptb-35 bb">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <div class="footer-logo mb-25">
                            <img src="<?php echo e(asset('shop')); ?>/images/Panasonic.png" alt="" />
                        </div>
                        <div class="footer-content">
                            <p>It's extremely customizable, easy to use and</p>
                            <ul>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Twetter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Instagram"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Google-Plus"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15">Liên hệ</h3>
                        <ul>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <span>XXX. xxxx. xxxxx</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-fax"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <span>0123xxxxxxx</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <span>xxxx@gmail.com</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15">Thông tin</h3>
                        <div class="footer-menu home3-hover">
                            <ul>
                                <li><a href="blog.html">Tin tức</a></li>
                                <li><a href="shop.html">Giới thiệu về cửa hàng</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
               
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15">Quy định khác</h3>
                        <div class="footer-menu">
                            <ul>
                                <li><a href="#">Quy định đổi trả</a></li>
                                <li><a href="#">Quy định mua hàng</a></li>
                                <li><a href="#">Điều khoản sử dụng</a></li>
                                <li><a href="#">Chính sách bảo mật</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom ptb-20">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="copyright">
                    <span>Copyright &copy; 2018 <a href="#"></a> All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div>
</footer>