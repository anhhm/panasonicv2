
<!-- header-->
    <header id="wrap-header">
        <div class="container">
            <!--menu + logo-->
            <nav class="navbar navbar-default menu-nav">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!--logo-->
                        <a href="#" class="navbar-brand logo">
                            <img src="<?php echo e(asset('restaurant/images/logo.png')); ?>" alt="image" width="150">
                        </a>
                        <!--/logo-->
                        <div id="language">
                            <a id="lang_vn" href="{base_url('home/language/')}?lang=vi&page={$page}" title="Tiếng Việt">Tiếng Việt</a>
                            <a id="lang_en" href="{base_url('home/language/')}?lang=en&page={$page}" title="English">English</a>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php echo $__env->make('home.template.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </nav>  
            <!--/menu + logo-->
        </div>  
    </header>
<!--/header-->