<?php $__env->startSection('contentProduct'); ?>
    <div class="tab-area">
        <div class="tab-menu-area bg-fff">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="shop-tab-menu">
                        <ul>
                            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true"><i class="fa fa-th-list"></i></a></li>
                            <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-th-list"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="woocommerce-ordering text-center">
                        <select name="orderby">
                            <option value="menu_order" selected="selected">Mặc định</option>
                            <option value="rating">Sắp xếp theo đánh giá giảm dần</option>
                            <option value="date">Sắp xếp theo sản phẩm mới</option>
                            <option value="price">Giá từ thấp đến cao</option>
                            <option value="price-desc">Giá từ cao đến thấp</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <?php if(isset($data['list_product'])): ?>
                <div role="tabpanel" class="tab-pane active in" id="tab1">
                    <div class="row">
                        <?php $__currentLoopData = $data['list_product']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                <div class="product-wrapper bg-fff mb-30" style="height: 260px;">
                                    <div class="product-img" style="height: 160px;">
                                        <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                        </a>
                                        <div class="product-icon c-fff hover-bg">
                                            <ul>
                                                <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product-content">
                                        <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                        <span><?php echo e(number_format($item->price_mb,'0','.',',')); ?> VND</span>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab2">
                    <div class="row">
                        <?php $__currentLoopData = $data['list_product']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="product-wrapper bg-fff mb-30 ptb-20">
                                    <div class="product-img shop-product-img">
                                        <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                        </a>
                                    </div>
                                    <div class="product-content shop-product-content">
                                        <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                        <span><?php echo e(number_format($item->price_mb,'0','.',',')); ?> VND</span>
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                        <p><?php echo e($item->content_vn); ?></p>
                                        <div class="shop-product-icon c-fff hover-bg">
                                            <ul>
                                                <li class="active"><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ">Thêm vào giỏ</a></li>
                                                <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            <?php else: ?>
                <p>Sản phẩm đang xử lí</p>
            <?php endif; ?>
        </div>
    </div>         
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.product.masterProduct', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>