<?php $__env->startSection('category',request()->segment(2)); ?>
<?php $__env->startSection('action','Chỉnh sửa'); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('lib/checkbox/dist/css/bootstrap3/bootstrap-switch.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data for Selected Period </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="" method="POST" role="form">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                            <div style="margin:10px 0 20px 0">
                                <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary">Lưu</button>
                                <a type="button" class="btn btn-default" href="<?php echo e(url('admin/permissiongroup/list')); ?>">Trở về</a>
                            </div>
                            <div class="form-group">
                                <label for="">Tên nhóm</label>
                                <input type="text" class="form-control" id="" name="group_name" placeholder="Nhập tên nhóm ..." required="" value="<?php echo e(isset($group_name)?$group_name:null); ?>">
                                <input type="hidden" class="form-control" id="" name="group_id placeholder="Nhập tên nhóm ..." required="" value="<?php echo e(isset($group_id)?$group_id:null); ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Mô tả nhóm</label>
                                <textarea class="form-control" class="form-control" name="group_content" cols="30" rows="4">
                                    <?php echo e(isset($group_content) ? $group_content : null); ?>

                                </textarea>
                            </div>
                            <table id="dataTables-example" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Tên Folder</th>
                                    <th>Quyền Xem</th>
                                    <th>Quyền Thêm</th>
                                    <th>Quyền Sửa</th>
                                    <th>Quyền Xóa</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($item->permission_name); ?><input type="hidden" name="<?php echo e($item->slug); ?>[permission_id]" value="<?php echo e($item->permission_id); ?>"></td>
                                            <td>
                                                <input class="form-control" type="checkbox" name="<?php echo e($item->slug); ?>[view]" data-size="mini"
                                                        <?php echo e($item->action_view==1? 'checked':''); ?>>
                                            </td>
                                            <td>
                                                <input class="form-control" type="checkbox" name="<?php echo e($item->slug); ?>[add]" data-size="mini"
                                                        <?php echo e($item->action_add==1? 'checked':''); ?>>
                                            </td>
                                            <td>
                                                <input class="form-control" type="checkbox" name="<?php echo e($item->slug); ?>[edit]" data-size="mini"
                                                        <?php echo e($item->action_edit==1? 'checked':''); ?>>
                                            </td>
                                            <td>
                                                <input class="form-control" type="checkbox" name="<?php echo e($item->slug); ?>[delete]" data-size="mini"
                                                        <?php echo e($item->action_delete==1? 'checked':''); ?>>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('addScript'); ?>
    <!-- checkbox -->
    <script src="<?php echo e(asset('lib/checkbox/dist/js/bootstrap-switch.js')); ?>"></script>
    <script>
        $(function(argument) {
            $('[type="checkbox"]').bootstrapSwitch();
        })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>