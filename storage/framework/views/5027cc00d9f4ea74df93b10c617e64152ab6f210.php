<!--menu-->
    <nav class="menu font-heading">
        <div class="menu-icon hidden-lg hidden-md">
            <i class="fa fa-navicon"></i>
            <span>MENU</span>
        </div>
        <ul class="hidden-sm hidden-xs">
            <li>
                <a href="<?php echo e(site_url('home')); ?>">Home </a>
            </li>
            <li>
                <a href="<?php echo e(site_url('news/list')); ?>">Game Online</a>
            </li>
            <li class="mega">
                <a href="<?php echo e(site_url('news/list')); ?>">Reviews<span></span></a>
                <!--Mega menu-->
                <div class="mega-menu">
                    <h3>
                        Latest Post
                    </h3>
                    <div class="row">
                        <?php for($i = 0; $i < 4; $i++): ?>
                            <div class="col-md-3">
                                <div class="mega-col">
                                    <div class="mega-item">
                                        <div class="mega-item-img">
                                            <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                                                <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/3.jpg" />
                                            </a>
                                            <a href="" class="cate-tag">Business</a>
                                        </div>
                                        <p><a href="article.html">Donald Trump suggests the DNC was hacked...</a></p>
                                        <div class="meta-post">
                                            <a href="#">
                                                Sue	Benson
                                            </a>
                                            <em></em>
                                            <span>
                                                22 Sep 2016
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
            </li>
            <li>
                <a href="<?php echo e(site_url('news/list')); ?>">Video </a>
            </li>
            <li>
                <a href="<?php echo e(site_url('news/list')); ?>">Computing</a>
            </li>
            <li>
                <a href="<?php echo e(site_url('news/list')); ?>">Mobile</a>
            </li>
            <li>
                <a href="<?php echo e(site_url('news/list')); ?>">Tech </a>
            </li>
            <li>
                <a href="#">Page <span></span></a>
                <ul class="submenu">
                    <li>
                        <a href="<?php echo e(site_url('cvonline')); ?>">About me</a>
                    </li>
                    <li>
                        <a href="<?php echo e(site_url('contact')); ?>">Contact us</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>