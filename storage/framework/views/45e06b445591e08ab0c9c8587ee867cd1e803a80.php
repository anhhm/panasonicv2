
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>CMS | Website</title>
    <script>
        var baseUrl =  '<?php echo e(url('')); ?>';
    </script>
    <?php echo $__env->yieldContent('addCss'); ?>
    <?php echo $__env->make('Auth.template.linkcss', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <?php echo $__env->make('Auth.template.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('Auth.template.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    <?php echo $__env->yieldContent('action'); ?> <?php echo $__env->yieldContent('category'); ?>
                </h1>

                <?php if(App::isLocale('vi')): ?>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Trang chính</a></li>
                        <li>Danh sách</li>
                    </ol>
                <?php endif; ?>

                <?php if(App::isLocale('en')): ?>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo e(url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li>list</li>
                    </ol>
                <?php endif; ?>
            </section>
            <div class="col-lg-12">
                <?php if(Session::has('flash_messages')): ?>
                    <div class="alert alert-<?php echo Session::get('flash_level'); ?> alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo Session::get('flash_messages'); ?>

                    </div>
                <?php endif; ?>
            </div>
            <?php echo $__env->yieldContent('content'); ?>

        </div>

        <?php echo $__env->make('Auth.template.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="control-sidebar-bg"></div>
    </div>
    <?php echo $__env->make('Auth.template.linkscript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->yieldContent('addScript'); ?>
    <script>
        $("#ControlSelectLanguage").change(function () {
            var locale  =   $(this).val();
            var _token  =   $("input[name=_token]").val();

            $.ajax({
                url: baseUrl+'/language',
                type: 'POST',
                dataType: 'json',
                data: {locale: locale , _token : _token },
                success : function(data){
                },
                error : function(data) {

                },
                complete : function(data){
                    window.location.reload(true);
                }

            })
        })
    </script>
</body>
</html>