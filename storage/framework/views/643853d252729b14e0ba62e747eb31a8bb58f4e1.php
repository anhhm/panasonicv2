<!-- footer -->
    <footer id="wrap-footer" class="color-w">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="#" class="logo">
                            <img src="<?php echo e(asset('restaurant/images/logo.png')); ?>" alt="image">
                        </a>
                        <ul>
                            <li>Địa chỉ<span class="pull-right">300 E-Block Building, Malaysia</span></li>
                            <li>Số điện thoại<span class="pull-right">+880 1973 833 508</span></li>
                            <li>Email<span class="pull-right">ndktheme@gmail.com</span></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <h4>Thời gian mở cửa</h4>
                        <ul>
                            <li>Chủ nhật:<span class="pull-right">Closed</span></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <h4>Thư viện</h4>
                        <ul class="gallery-2">
                            <li class="gallery-item">
                                <a href="<?php echo e(asset('restaurant/images/thumbs-large/2.jpg')); ?>" class="hover-img image-zoom">
                                    <img src="<?php echo e(asset('restaurant/images/2.jpg')); ?>" alt="image">
                                    <div class="hover-caption color-w"><i class="fa fa-search-plus"></i></div>
                                </a>
                            </li>
                            <li class="gallery-item">
                                <a href="<?php echo e(asset('restaurant/images/thumbs-large/4.jpg')); ?>" class="hover-img image-zoom">
                                    <img src="<?php echo e(asset('restaurant/images/4.jpg')); ?>" alt="image">
                                    <div class="hover-caption color-w"><i class="fa fa-search-plus"></i></div>
                                </a>
                            </li>
                            <li class="gallery-item">
                                <a href="<?php echo e(asset('restaurant/images/thumbs-large/5.jpg')); ?>" class="hover-img image-zoom">
                                    <img src="<?php echo e(asset('restaurant/images/5.jpg')); ?>" alt="image">
                                    <div class="hover-caption color-w"><i class="fa fa-search-plus"></i></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        
                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ant</p>
                        <ul class="list-inline social-default social-bg-gray">
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-google-plus"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-youtube-play"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
            
                
                
            
        
    </footer>
    <!--/footer-->