<html>
    <head>
      <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cửa hàng máy lạnh</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $__env->make('home.template.seo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('addCss'); ?>
        <?php echo $__env->make('home.template.linkcss', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </head>
    <body>
        <?php echo $__env->make('home.template.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo $__env->yieldContent('content'); ?>
        
        <?php echo $__env->make('home.template.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <?php echo $__env->make('home.template.linkjs', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('addjs'); ?>

        <!-- Load Phone number -->
        <div class="mypage-alo-phone">
            <a href="">
                <div class="animated infinite zoomIn mypage-alo-ph-circle"></div>
                <div class="animated infinite pulse mypage-alo-ph-circle-fill"></div>
                <div class="animated infinite tada mypage-alo-ph-img-circle"></div>
            </a>
        </div>

        <script type="text/javascript">

            $('.modal-on-load').trigger('click');
            // Hide Header on on scroll down
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            var navbarHeight = $('#header').outerHeight();
            $(window).scroll(function(event){
                didScroll = true;
            });

            setInterval(function() {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 250);

            function hasScrolled() {
                var st = $(this).scrollTop();
                console.log(st);
                if(Math.abs(lastScrollTop - st) <= delta)
                    return;
                if(st > 3410 && st < 4600) {
                    $('#sidebar_left').addClass('banner_run').removeClass('banner_foo');
                }
                else if(st < 3410){
                    $('#sidebar_left').removeClass('banner_run').removeClass('banner_foo');
                }
                else if(st > 4600){
                    $('#sidebar_left').removeClass('banner_run').addClass('banner_foo');
                }
                lastScrollTop = st;
            }


            $(document).ready(function(){
                $(window).scroll(function(){
                    var cach_top = $(window).scrollTop();
                    if(cach_top >=1){ 
                        $('#header').css({'position': 'fixed', 'top': '0px','z-index': '999','width': '100%','background': '#f4f4f4'});
                        $('#header .header-middle-area').css({'padding': '0px'});
                    }else{
                        $('#header').css({'position': 'relative'});
                        $('#header .header-middle-area').css({'padding': '30px'});
                    }
                });
            });   
        </script>

    </body>
</html>