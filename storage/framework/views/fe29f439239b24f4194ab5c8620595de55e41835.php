<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/typo.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentProduct'); ?>
    <?php if(isset($data['detail'])): ?>
       <div class="row">
            <div class="col-lg-5">
                <div class="symple-product mb-20">
                    <div class="single-product-tab  box-shadow mb-20">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="one">
                                <a class="popup" href="<?php echo e($data['detail']['0']['img_avatar1']); ?>">
                                    <img src="<?php echo e($data['detail']['0']['img_avatar1']); ?>"" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="single-product-menu mb-30 box-shadow">
                        <div class="single-product-active clear">
                            <div class="single-img floatleft">
                                <a href="#" data-toggle="tab">
                                    <img src="<?php echo e($data['detail']['0']['img_avatar1']); ?>" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="symple-product box-shadow bg-fff p-15 mb-30">
                    <h3><?php echo e($data['detail']['0']['title_vn']); ?></h3>
                    <div class="product-content simple-product-content mb-10">
                        <ul>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                        <br/>
                        <span><span><?php echo e(number_format($data['detail']['0']['price_mb'],"0",",",".")); ?> VND</span></span>
                    </div>
                    <div class="product_meta">
                        <b>VAT:</b> <span><?php echo e($data['detail']['0']['vat']); ?></span><br/>
                        <b>Model:</b> <span><?php echo e($data['detail']['0']['model']); ?></span><br/>
                        <b>Tình trạng:</b> <span><?php echo e($data['detail']['0']['status']); ?></span><br/>
                        <b>Bảo hành:</b> <span><?php echo e($data['detail']['0']['guarantee']); ?></span><br/>
                        
                    </div>
                    <p><?php echo e($data['detail']['0']['content_vn']); ?></p>
                    <!-- <div class="simple-product-form contuct-form mtb-20">
                        <form action="">
                            <input min="1" max="1000" name="quantity" value="48" type="number">
                            <button>Thêm vào giỏ</button>
                        </form>
                    </div> -->
                    <div class="simple-product-icon c-fff hover-bg pb-20 mb-10 bb">
                        <ul>
                            <li><a href="#" data-toggle="tooltip" title="" data-original-title="Browser Wishlist"><i class="fa fa-heart-o"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="" data-original-title="Compare"><i class="fa fa-comments"></i></a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="simple-product-tab box-shadow">
                <div class="simple-product-tab-menu clear">
                    <ul>
                        <li class="active"><a href="#description" data-toggle="tab">Description</a></li>
                        <li><a href="#reviews" data-toggle="tab">Reviews (3)</a></li>
                    </ul>
                </div>
                <div class="tab-content  bg-fff">
                    <div class="tab-pane active" id="description">
                        <div class="product-description p-20 bt">
                            <?php echo $data['detail']['0']['detail_vn']; ?>

                        </div>
                    </div>
                    <div class="tab-pane" id="reviews">
                        <div class="product-reviews p-20 bt">
                            <div class="fb-comments" style="width: 100% !important;" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="3"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <?php if(isset($data['list_product_of_cat'])): ?>
        <div class="product box-shadow mtb-15 bg-fff">
            <div class="product-title home2-product-title home2-bg-1 text-uppercase">
                <i class="fa fa-star-o icon bg-4"></i>
                <h3>Sản phẩm cùng loại</h3>
            </div>
            <div class="new-product-active left-right-angle home2">
                <?php $__currentLoopData = $data['list_product_of_cat']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="product-wrapper bl">
                        <div class="product-img">
                            <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                            </a>
                            <div class="product-icon c-fff hover-bg">
                                <ul>
                                    <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                    <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                </ul>
                            </div>
                            <span class="sale">Sale</span>
                        </div>
                        <div class="product-content">
                            <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e(str_limit($item->title_vn, 50)); ?></a></h3>
                            <ul>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                            <span><?php echo e(number_format($item->price_mb,'0',',','.')); ?> VNĐ</span>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('addjs'); ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=2013036852319830&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.product.masterProduct', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>