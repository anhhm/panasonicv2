<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta property="og:title" content="CV - Trần Chánh Hiệp" /> 
      <meta property="og:site_name" content="CV - Trần Chánh Hiệp" /> 
      <meta property="og:description" content="CV - Trần Chánh Hiệp" />
      <link rel="image_src" href="demo/banner-share.jpg" />
      <meta property="og:image" href="<?php echo e(asset('cvonlineTemp')); ?>demo/banner-share.jpg" />

      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <title>CV Online Tran Chanh Hiep</title>
      <link rel="shortcut icon" type="text/css" href="favicon.ico" />

      <!-- CSS | Google Fonts -->
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

      <link rel="stylesheet" href="<?php echo e(asset('lib')); ?>/font-awesome/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo e(asset('cvonlineTemp')); ?>/css/prettyPhoto.css">
      <link rel="stylesheet" href="<?php echo e(asset('cvonlineTemp')); ?>/css/animate.min.css">
      <link rel="stylesheet" href="<?php echo e(asset('cvonlineTemp')); ?>/library/bootstrap/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo e(asset('lib')); ?>/fancybox-master/dist/jquery.fancybox.css">
      <link rel="stylesheet" href="<?php echo e(asset('lib')); ?>/mCustomScrollbar/jquery.mCustomScrollbar.css">
      <link rel="stylesheet" href="<?php echo e(asset('cvonlineTemp')); ?>/css/style.css">

      <script src="<?php echo e(asset('cvonlineTemp')); ?>/js/modernizr.custom.js"></script>
    </head>
    <body>
    
      <?php echo $__env->yieldContent('content'); ?>

    <!-- JQuery -->
    <script src="<?php echo e(asset('cvonlineTemp')); ?>/js/jquery-1.12.4.min.js"></script>   
    <script src="<?php echo e(asset('cvonlineTemp')); ?>/js/jquery.mixitup.min.js"></script>
    <script src="<?php echo e(asset('cvonlineTemp')); ?>/library/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo e(asset('lib')); ?>/fancybox-master/dist/jquery.fancybox.js"></script> 
    <script src="<?php echo e(asset('lib')); ?>/typed/typed.min.js"></script>
    <script src="<?php echo e(asset('lib')); ?>/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script> 
    <script src="<?php echo e(asset('cvonlineTemp')); ?>/js/tab.js"></script> 
    <script src="<?php echo e(asset('cvonlineTemp')); ?>/js/main.js"></script>
    </body>
</html>