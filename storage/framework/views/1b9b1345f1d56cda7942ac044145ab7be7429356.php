<!--jquery-->
<script src="<?php echo e(asset('restaurant/js/jquery-2.2.4.min.js')); ?>"></script>
<!--bootstrap-->
<script src="<?php echo e(asset('restaurant/js/bootstrap.min.js')); ?>"></script>
<!--owl carousel-->
<script src="<?php echo e(asset('lib/owl-coursel/owl.carousel.js')); ?>"></script>
<!--magnific popup-->
<script src="<?php echo e(asset('lib/fancybox-master/dist/jquery.fancybox.js')); ?>"></script>
<script src="<?php echo e(asset('lib/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
<!--script-->
<script src="<?php echo e(asset('restaurant/js/script.js')); ?>"></script>