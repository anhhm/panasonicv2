<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',trans("admin_product.news_header")); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.css">
    <!-- Page script -->
    <script src="<?php echo e(asset('lib')); ?>/ckeditor/ckeditor.js"></script>
    <script src="<?php echo e(asset('lib/ckfinder/ckfinder.js')); ?>"></script>
    <script src="<?php echo e(asset('lib/js/ckfinder_function.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo isset($data['product']) ? $data['product']['title'] : null; ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#info" data-toggle="tab">Thông tin chính</a></li>
                                    <li><a href="#num" data-toggle="tab">Thông Số</a></li>
                                    <li><a href="#seo" data-toggle="tab">Seo</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="info">
                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.title')); ?>:</label>
                                            <input type="text" name="title_vn" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_title_news')); ?>" value="<?php echo old('title',isset($data['product']) ? $data['product']['title_vn'] : null ); ?>">
                                            <?php if($errors->has('title_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.model')); ?>:</label>
                                            <input type="text" name="model" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_model')); ?>" value="<?php echo old('title',isset($data['product']) ? $data['product']['model'] : null ); ?>">
                                            <?php if($errors->has('model')): ?>
                                                <p style="color:red"><?php echo e($errors->first('model')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.category')); ?>:</label>
                                            <select class="form-control select2" name="id_cat" data-placeholder="chon danh muc" style="width: 100%;">
                                                <?php echo e(cat_parent($data['cat_product'],0,"&#187;",isset($data['product']['id_cat'])?$data['product']['id_cat']:0)); ?>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo e(trans('admin_product.content')); ?>:</label>
                                            <textarea class="form-control" class="form-control" name="content_vn" cols="30" rows="4"><?php echo old('content',isset($data['product']) ? $data['product']['content_vn'] : null ); ?></textarea>
                                            <?php if($errors->has('content_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('content_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo e(trans('admin_product.detail')); ?>:</label>
                                            <textarea class="form-control textarea_editor " name="detail_vn" id="editor1" cols="30" rows="10"><?php echo old('detail',isset($data['product']) ? $data['product']['detail_vn'] : null ); ?></textarea>
                                            <?php if($errors->has('detail_vn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('detail_vn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.active')); ?>:</label>
                                            <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                                <?php if(App::isLocale('vi')): ?>
                                                    <option value="1" <?php echo e($data['product']['active']=="1"? 'selected':''); ?>>Có</option>
                                                    <option value="0" <?php echo e($data['product']['active']=="0"? 'selected':''); ?>>Không</option>
                                                <?php endif; ?>

                                                <?php if(App::isLocale('en')): ?>
                                                    <option value="1" <?php echo e($data['product']['active']=="1"? 'selected':''); ?>>Yes</option>
                                                    <option value="0" <?php echo e($data['product']['active']=="0"? 'selected':''); ?>>No</option>
                                                <?php endif; ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label"><?php echo e(trans('admin_product.img_avatar')); ?>:</label>
                                            <input class="form-control" type="text" name="avatar_project" id="avatar_project" value="<?php echo old('avatar_project',isset($data['product']) ? $data['product']['img_avatar1'] : null ); ?>" onclick ="selectFileWithCKFinder($(this))">
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="num">
                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.vat')); ?>:</label>
                                            <input type="text" name="vat" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_vat')); ?>" value="<?php echo old('title',isset($data['product']) ? $data['product']['vat'] : null ); ?>">
                                            <?php if($errors->has('vat')): ?>
                                                <p style="color:red"><?php echo e($errors->first('vat')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.price_mb')); ?>:</label>
                                            <input type="text" name="price_mb" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_priceMB')); ?>" value="<?php echo old('title',isset($data['product']) ? $data['product']['price_mb'] : null ); ?>">
                                            <?php if($errors->has('price_mb')): ?>
                                                <p style="color:red"><?php echo e($errors->first('price_mb')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.price_mt')); ?>:</label>
                                            <input type="text" name="price_mt" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_priceMT')); ?>" value="<?php echo old('title',isset($data['product']) ? $data['product']['price_mt'] : null ); ?>">
                                            <?php if($errors->has('price_mt')): ?>
                                                <p style="color:red"><?php echo e($errors->first('price_mt')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.price_mn')); ?>:</label>
                                            <input type="text" name="price_mn" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_priceMN')); ?>" value="<?php echo old('title',isset($data['product']) ? $data['product']['price_mn'] : null ); ?>">
                                            <?php if($errors->has('price_mn')): ?>
                                                <p style="color:red"><?php echo e($errors->first('price_mn')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.producer')); ?>:</label>
                                            <select class="form-control select2" name="producer" data-placeholder="<?php echo e(trans('admin_placeholder.placeholder_producer')); ?>" style="width: 100%;">
                                                <?php if(isset($data['producer'])): ?>
                                                    <?php $__currentLoopData = $data['producer']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($item['id']); ?>" <?php echo e($data['product']['id_producer']==$item['id']? 'selected':''); ?>><?php echo e($item['title']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.guarantee')); ?>:</label>
                                            <input type="text" name="guarantee" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_guarantee')); ?>" value="<?php echo old('title',isset($data['product']) ? $data['product']['guarantee'] : null ); ?>">
                                            <?php if($errors->has('guarantee')): ?>
                                                <p style="color:red"><?php echo e($errors->first('guarantee')); ?></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.status')); ?>:</label>
                                            <select class="form-control select2" name="status" data-placeholder="<?php echo e(trans('admin_placeholder.placeholder_status')); ?>" style="width: 100%;">
                                                <option value="Còn hàng" <?php echo e($data['product']['status']=="Còn hàng"? 'selected':''); ?>>Còn hàng</option>
                                                <option value="Hết hàng" <?php echo e($data['product']['status']=="Hết hàng"? 'selected':''); ?>>Hết hàng</option>
                                                <option value="Hàng sắp về" <?php echo e($data['product']['status']=="Hàng sắp về"? 'selected':''); ?>>Hàng sắp về</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="seo">
                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.metatagkeyword')); ?>:</label>
                                            <input type="text" name="metakeyword" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_metakeyword')); ?>" value="<?php echo old('metakeyword',isset($data['product']) ? $data['product']['metakeyword'] : null ); ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.metadescription')); ?>:</label>
                                            <textarea class="form-control" class="form-control" name="metadescription" cols="30" rows="4"><?php echo old('content',isset($data['product']) ? $data['product']['metadescription'] : null ); ?></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInput"><?php echo e(trans('admin_product.tags')); ?>:</label>
                                            <select multiple class="form-control selecttag" name="tags[]" data-placeholder="<?php echo e(trans('admin_placeholder.placeholder_tags')); ?>" style="width: 100%;">
                                                <?php if(isset($data['listtag'])): ?>
                                                    <?php $__currentLoopData = $data['listtag']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($value['id']); ?>" selected="selected"><?php echo e($value['title_vn']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary"><?php echo e(trans('actionbtn.btnadd')); ?></button>
                                    <a type="button" class="btn btn-default" href="<?php echo e(url('admin/products/list')); ?>"><?php echo e(trans('actionbtn.btnreturn')); ?></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('addScript'); ?>
    <!-- Select2 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.full.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();

            $(".selecttag").select2({
                tags: true,
                data: [
                    <?php $__currentLoopData = $data['tags']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        {
                            id: "<?php echo e($item['id']); ?>",
                            text: "<?php echo e($item['title_vn']); ?>"
                        },
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                ],
                tokenSeparators: [','], 
                placeholder: "Add your tags here",
                /* the next 2 lines make sure the user can click away after typing and not lose the new tag */
                selectOnClose: true, 
                closeOnSelect: false
            })
        });
        CKEDITOR.replace( 'editor1');
        CKEDITOR.replace( 'editor2');
		CKFinder.setupCKEditor( editor );
        var editor = CKEDITOR.replace('introduce',{
            language:'vi',
            filebrowserBrowseUrl :'<?php echo e(asset('lib/ckfinder/ckfinder.html')); ?>',
            filebrowserImageBrowseUrl : '<?php echo e(asset('lib/ckfinder/ckfinder.html?type=Images')); ?>',
            filebrowserFlashBrowseUrl : '<?php echo e(asset('lib/ckfinder/ckfinder.html?type=Flash')); ?>',
            filebrowserUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
            filebrowserImageUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
            filebrowserFlashUploadUrl : '<?php echo e(asset('lib/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>',
        });
    </script>
    <!-- end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>