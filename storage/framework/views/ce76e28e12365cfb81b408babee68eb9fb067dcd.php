<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('shop')); ?>/css/typo.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentNews'); ?>
    <div class="blog-area">
        <div class="content-box">
        	<div class="product-description">
				<?php if(isset($data['detail'])): ?>
					<?php $__currentLoopData = $data['detail']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php echo $item->detail_vn; ?>

					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
			</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.about.masterNews', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>