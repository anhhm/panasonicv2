<?php $__env->startSection('content'); ?>
	<div id="wrap-body">
		<!--banner-->
		<div class="heading-group page-name color-w">
			<h1>Giới thiệu</h1>
			<ul class="becku">
				<li><a href="#">restaurant</a></li>
				<li>about</li>
			</ul>
			<div class="star-group">
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o star-1x"></i>
				<i class="fa fa-star-o"></i>
			</div>
		</div>
		<!-- /banner-->
		<div class="container" id="main-content">
			<div class="row">
				<div class="col-md-6">
					<!-- about-->
					<div class="about">
						<div class="title"><h3>Giới thiệu</h3></div>
						<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</p>
						<br/><img src="<?php echo e(asset('restaurant/images/event-2.jpg')); ?>" alt="image"><br/>
						<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
					</div>
					<!-- /about-->
				</div>
				<div class="col-md-6">
					<!--team-->
					<div class="team">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<div class="thumbnail">
									<figure class="hover-img">
										<img src="<?php echo e(asset('restaurant/images/event-2.jpg')); ?>" alt="image">
									</figure>
									<div class="caption">
										<h4>Jone Lee <span>/ Chef</span></h4>
										<p>Lorem ipsum dolor sit amet conse ctetur</p>
										<ul class="list-inline social-default">
											<li><a href="#" class="fa fa-facebook"></a></li>
											<li><a href="#" class="fa fa-google-plus"></a></li>
											<li><a href="#" class="fa fa-twitter"></a></li>
											<li><a href="#" class="fa fa-youtube-play"></a></li>
										</ul>
									</div>
								</div>	
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="thumbnail">
									<figure class="hover-img">
										<img src="<?php echo e(asset('restaurant/images/default.png')); ?>" alt="image">
									</figure>
									<div class="caption">
										<h4>Jone Lee <span>/ Chef</span></h4>
										<p>Lorem ipsum dolor sit amet conse ctetur</p>
										<ul class="list-inline social-default">
											<li><a href="#" class="fa fa-facebook"></a></li>
											<li><a href="#" class="fa fa-google-plus"></a></li>
											<li><a href="#" class="fa fa-twitter"></a></li>
											<li><a href="#" class="fa fa-youtube-play"></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="thumbnail">
									<figure class="hover-img">
										<img src="<?php echo e(asset('restaurant/images/default.png')); ?>" alt="image">
									</figure>
									<div class="caption">
										<h4>Jone Lee <span>/ Chef</span></h4>
										<p>Lorem ipsum dolor sit amet conse ctetur</p>
										<ul class="list-inline social-default">
											<li><a href="#" class="fa fa-facebook"></a></li>
											<li><a href="#" class="fa fa-google-plus"></a></li>
											<li><a href="#" class="fa fa-twitter"></a></li>
											<li><a href="#" class="fa fa-youtube-play"></a></li>
										</ul>
									</div>
								</div>	
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="thumbnail">
									<figure class="hover-img">
										<img src="<?php echo e(asset('restaurant/images/default.png')); ?>" alt="image">
									</figure>
									<div class="caption">
										<h4>Jone Lee <span>/ Chef</span></h4>
										<p>Lorem ipsum dolor sit amet conse ctetur</p>
										<ul class="list-inline social-default">
											<li><a href="#" class="fa fa-facebook"></a></li>
											<li><a href="#" class="fa fa-google-plus"></a></li>
											<li><a href="#" class="fa fa-twitter"></a></li>
											<li><a href="#" class="fa fa-youtube-play"></a></li>
										</ul>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<!--/team-->
				</div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>