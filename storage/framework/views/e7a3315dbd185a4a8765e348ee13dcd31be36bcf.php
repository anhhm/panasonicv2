<?php $__env->startSection('content'); ?>
   <?php echo $__env->make('home.template.bannerslide', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- product area start -->
    <div class="product-area ptb-35">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                    <div class="tab-area box-shadow bg-fff">
                        <div class="product-title bg-1 text-uppercase">
                            <i class="fa fa-check-square-o icon bg-4"></i>
                            <!-- <h3>Danh mục sản phẩm</h3> -->
                            <div class="tab-menu floatright hidden-xs" style="width: 95%">
                                <ul>
                                    <!-- <li class="active"><a href="#treotuong" data-toggle="tab">ĐH Treo tường</a></li> -->
                                    <?php if(isset($data['parent_cat'])): ?>
                                        <?php $__currentLoopData = $data['parent_cat']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li class="<?php if( $k == 0): ?> active <?php endif; ?>"><a href="#<?php echo e($k); ?>" data-toggle="tab"><?php echo e($item->title_vn); ?></a></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            
                            <div role="tabpanel" class="tab-pane active" id="0">
                                <div class="product-active tab-active left left-right-angle">
                                    <?php if(isset($data['treotuong'])): ?>
                                        <?php $__currentLoopData = $data['treotuong']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="product-wrapper bl">
                                                    <div class="product-img">
                                                        <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                                        </a>
                                                        <div class="product-icon c-fff hover-bg">
                                                            <ul>
                                                                <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                                <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <span class="sale">Sale</span>
                                                    </div>
                                                    <div class="product-content">
                                                        <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                                        <ul>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                        </ul>
                                                        <span><?php echo e(number_format($item->price_mb,0,".",",")); ?> VND</span>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>                                
                            </div>
                            
                            <div role="tabpanel" class="tab-pane fade" id="1">
                                <div class="product-active tab-active left left-right-angle">
                                    <?php if(isset($data['amtran'])): ?>
                                        <?php $__currentLoopData = $data['amtran']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="product-wrapper bl">
                                                    <div class="product-img">
                                                        <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                                        </a>
                                                        <div class="product-icon c-fff hover-bg">
                                                            <ul>
                                                                <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                                <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <span class="sale">Sale</span>
                                                    </div>
                                                    <div class="product-content">
                                                        <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                                        <ul>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                        </ul>
                                                        <span><?php echo e(number_format($item->price_mb,0,".",",")); ?> VND</span>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>     
                            </div>
                            
                            <div role="tabpanel" class="tab-pane fade" id="2">
                                <div class="product-active tab-active left left-right-angle">
                                    <?php if(isset($data['Multi'])): ?>
                                        <?php $__currentLoopData = $data['Multi']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="product-wrapper bl">
                                                    <div class="product-img">
                                                        <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                                        </a>
                                                        <div class="product-icon c-fff hover-bg">
                                                            <ul>
                                                                <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                                <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <span class="sale">Sale</span>
                                                    </div>
                                                    <div class="product-content">
                                                        <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                                        <ul>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                        </ul>
                                                        <span><?php echo e(number_format($item->price_mb,0,".",",")); ?> VND</span>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>     
                            </div>
                        
                            <div role="tabpanel" class="tab-pane fade" id="3">
                                <div class="product-active tab-active left left-right-angle">
                                    <?php if(isset($data['tudung'])): ?>
                                        <?php $__currentLoopData = $data['tudung']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="product-wrapper bl">
                                                    <div class="product-img">
                                                        <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                            <img src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                                        </a>
                                                        <div class="product-icon c-fff hover-bg">
                                                            <ul>
                                                                <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                                <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <span class="sale">Sale</span>
                                                    </div>
                                                    <div class="product-content">
                                                        <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                                        <ul>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                        </ul>
                                                        <span><?php echo e(number_format($item->price_mb,0,".",",")); ?> VND</span>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product area end -->

    <!-- banner-area start -->
    <?php if(isset($data['single_Banner'])): ?>
        <div class="banner-area">
            <div class="container">
                <div class="row">
                    <?php $__currentLoopData = $data['single_Banner']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-4  col-md-4 col-sm-6 col-xs-12">
                            <div class="single-banner">
                                <a href="#">
                                    <img src="<?php echo e($item->img_avatar); ?>" alt="<?php echo e($item->title_vn); ?>">
                                </a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <!-- banner-area end -->

    <!-- all-product-area start -->
    <div class="all-product-area mtb-45">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <!-- featured-area start -->
                    <div class="featured-area box-shadow bg-fff">
                        <div class="product-title bg-1 text-uppercase">
                            <i class="fa fa-star-o icon bg-4"></i>
                            <h3>Điều hòa Panasonic</h3>
                        </div>
                        <div class="sidebar">
                            <ul class="sidebar-menu" data-widget="tree">
                                <?php echo $data['list_menu']; ?>

                            </ul>
                        </div>
                    </div>

                    <!-- blog-area start -->
                    <?php if(isset($data['list_news'])): ?>
                        <div class="blog-area box-shadow mtb-35 bg-fff">
                            <div class="product-title bg-1 text-uppercase">
                                <i class="fa fa-comments-o icon bg-4"></i>
                                <h3>Tin tức</h3>
                            </div>
                            <div class="feautred-active right left-right-angle">
                                <?php $__currentLoopData = $data['list_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="blog-wrapper">
                                        <div class="blog-img mtb-30">
                                            <a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>">
                                                <img src="<?php echo e($item->img_avatar); ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="blog-content">
                                            <h3><a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>"><?php echo e(str_limit($item->title_vn, 50)); ?></a></h3>
                                            <div class="blog-meta">
                                                <span><?php echo e($item->created_at); ?></span>
                                            </div>
                                            <p><?php echo e(str_limit($item->content_vn, 100)); ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!-- blog-area end -->

                    <!-- testimonils-area start -->
                    <div class="testimonils-area box-shadow mtb-35 bg-fff" style="margin-bottom: 0px;">
                        <div class="product-title bg-1 text-uppercase">
                            <i class="fa fa-star-o icon bg-4"></i>
                            <h3>Khách hàng</h3>
                        </div>
                        <div class="feautred-active right left-right-angle">
                            <?php for($i = 0; $i < 2; $i++): ?>
                                <div class="single-testimonil clear">
                                    <div class="testimonil-content p mtb-25">
                                        <p>Sản phẩm tuyệt với, chăm sóc khách hàng quá tốt, Hi vọng sẽ có cơ hội hợp tác tiếp với cửa hàng</p>
                                    </div>
                                    <div class="test-img floatleft">
                                        <img src="https://devitems.com/html/oneclick-preview/oneclick/img/test/2.jpg" alt="">
                                    </div>
                                    <div class="test-info">
                                        <h6>Kaji Hasibur Rahman</h6>
                                        <span>Tp.Hồ Chí Minh</span>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <!-- testimonils-area end -->
                    <div id="sidebar_left">
                        <div id="sidebar_banner">
                            <div class="sidebar__inner">
                                <div class="single-banner mtb-35">
                                    <a href="#">
                                        <img src="https://devitems.com/html/oneclick-preview/oneclick/img/banner/6.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- product-area start -->
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <!-- product-area start -->
                    <?php if(isset($data['treotuong'])): ?>
                        <div class="product box-shadow bg-fff">
                            <div class="product-title bg-1 text-uppercase">
                                <i class="fa fa-paper-plane-o icon bg-4"></i>
                                <a href=""><h3>Điều hòa treo tường</h3></a>
                            </div>
                            <div class="new-product-active left left-right-angle">
                                <?php $__currentLoopData = $data['treotuong']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="product-items" >
                                            <div class="product-wrapper bb bl">
                                                <div class="product-img" style="height: 105px;">
                                                    <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                        <img style="height: 100%;" src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                        <img style="height: 100%;" src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                                    </a>
                                                    <div class="product-icon c-fff hover-bg">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                            <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                    <span><?php echo e(number_format($item->price_mb,0,".",",")); ?> VND</span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <!-- banner-area start -->
                        <div class="banner-area mtb-15">
                            <div class="single-banner">
                                <a href="#">
                                    <img style="max-height: 190px;" src="http://xspace.talaweb.com/daikinvietnam/home/share/dieuhoa/banner+category+air-con2013_980x167px.jpg" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- banner-area end -->
                    <?php endif; ?>
                    <!-- tab-area start -->
                    <?php if(isset($data['amtran'])): ?>
                        <div class="product box-shadow bg-fff">
                            <div class="product-title bg-1 text-uppercase">
                                <i class="fa fa-paper-plane-o icon bg-4"></i>
                                <h3>Điều hòa CASSETTE Âm trần</h3>
                            </div>
                            <div class="new-product-active left left-right-angle">
                                <?php $__currentLoopData = $data['amtran']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="product-items" >
                                        <div class="product-wrapper bb bl">
                                            <div class="product-img" style="max-height: 105px;">
                                                <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                    <img style="height: 100%;" src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                    <img style="height: 100%;" src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                                </a>
                                                <div class="product-icon c-fff hover-bg">
                                                    <ul>
                                                        <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                        <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                                <span><?php echo e(number_format($item->price_mb,0,".",",")); ?> VND</span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <!-- banner-area start -->
                        <div class="banner-area mtb-15">
                            <div class="single-banner">
                                <a href="#">
                                    <img style="max-height: 190px;" src="http://xspace.talaweb.com/daikinvietnam/home/share/dieuhoa/1.invrtrtrplesplit_banner.jpg" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- banner-area end -->
                    <?php endif; ?>
                    <!-- tab-area start -->
                    
                    <?php if(isset($data['tudung'])): ?>
                        <div class="product box-shadow bg-fff">
                            <div class="product-title bg-1 text-uppercase">
                                <i class="fa fa-paper-plane-o icon bg-4"></i>
                                <h3>Điều hòa Tủ Đứng</h3>
                            </div>
                            <div class="new-product-active left left-right-angle">
                                <?php $__currentLoopData = $data['tudung']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="product-items" >
                                        <div class="product-wrapper bl">
                                            <div class="product-img" style="max-height: 105px;">
                                                <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                    <img style="height: 100%;" src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                    <img style="height: 100%;" src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                                </a>
                                                <div class="product-icon c-fff hover-bg">
                                                    <ul>
                                                        <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                        <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                    </ul>
                                                </div>
                                                <span class="sale">Sale</span>
                                            </div>
                                            <div class="product-content">
                                                <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                                <span><?php echo e(number_format($item->price_mb,"0",".",",")); ?> VND</span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    
                        <!-- banner-area start -->
                        <div class="banner-area mtb-15">
                            <div class="single-banner">
                                <a href="#">
                                    <img style="max-height: 190px;" src="http://xspace.talaweb.com/daikinvietnam/home/share/dieuhoa/Top%20banner%20panasonic%20copy.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!-- banner-area end -->
                    <!-- tab-area start -->
                    <?php if(isset($data['trungtam'])): ?>
                        <div class="product box-shadow bg-fff">
                            <div class="product-title bg-1 text-uppercase">
                                <i class="fa fa-paper-plane-o icon bg-4"></i>
                                <h3>Điều Hòa Trung Tâm</h3>
                            </div>
                            <div class="new-product-active left left-right-angle">
                                <?php $__currentLoopData = $data['trungtam']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="product-items" >
                                            <div class="product-wrapper bl">
                                                <div class="product-img" style="max-height: 105px;">
                                                    <a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>">
                                                        <img style="height: 100%;" src="<?php echo e($item->img_avatar1); ?>" alt="" class="primary">
                                                        <img style="height: 100%;" src="<?php echo e($item->img_avatar1); ?>" alt="" class="secondary">
                                                    </a>
                                                    <div class="product-icon c-fff hover-bg">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="" data-original-title="Thêm vào giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                                                            <li><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-search"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <h3><a href="<?php echo e(site_url('product')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                    <span><?php echo e(number_format($item->price_mb,"0",".",",")); ?> VND</span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    
                        <!-- banner-area start -->
                        <div class="banner-area mtb-15">
                            <div class="single-banner">
                                <a href="#">
                                    <img style="max-height: 190px;" src="<?php echo e(asset('shop')); ?>/images/banner1.jpg" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- banner-area end -->
                    <?php endif; ?>
                </div>
                <!-- product-area end -->
            </div>
        </div>
    </div>
    <!-- all-product-area end -->

    <!-- brand-area start -->
    <?php if(isset($data['logo_slide'])): ?>
        <div class="brand-area mb-35">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="brand-active  box-shadow p-15  bg-fff">
                            <?php $__currentLoopData = $data['logo_slide']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="single-brand">
                                    <a href="<?php echo e($item->link_href); ?>">
                                        <img src="<?php echo e($item->link_img); ?>" alt="">
                                    </a>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <!-- brand-area end -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('addjs'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>