<!-- footer-area start -->
<footer class="bg-fff bt">
    <div class="footer-top-area ptb-35 bb">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <div class="footer-logo mb-25">
                            <img src="<?php echo e(asset('shop')); ?>/images/Panasonic.png" alt="" />
                        </div>
                        <div class="footer-content">
                            <p>It's extremely customizable, easy to use and</p>
                            <ul>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Twetter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Instagram"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Google-Plus"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="tooltip" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15">Liên hệ</h3>
                        <ul>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <b>Miền Bắc</b>
                                        <span>Tầng 2, tòa nhà PVV, KĐT Nam Cường, 234 Hoàng Quốc Việt, Q. Bắc Từ Liêm, Hà Nội</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <b>Miền Trung</b>
                                        <span>Số 150 Bế Văn Đàn, P. Chính Gián, Q. Thanh Khê, Đà Nẵng </span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="contuct-content">
                                    <div class="contuct-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <div class="contuct-info">
                                        <b>Miền Nam</b>
                                        <span>Tòa nhà Hải Âu Building, phường 4, Quận Tân Bình, Hồ Chí Minh </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15" style="margin-top: 20px;"></h3>
                        <div class="footer-menu home3-hover">
                            <ul>
                                <li>
                                    <div class="contuct-content">
                                        <div class="contuct-icon">
                                            <i class="fa fa-fax"></i>
                                        </div>
                                        <div class="contuct-info">
                                            <span>0123xxxxxxx</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="contuct-content">
                                        <div class="contuct-icon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="contuct-info">
                                            <span>xxxx@gmail.com</span>
                                        </div>
                                    </div>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
               
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-title bb mb-20 pb-15">Quy định khác</h3>
                        <div class="footer-menu">
                            <ul>
                                <?php if(isset($data['list_news_policy'])): ?>
                                    <?php $__currentLoopData = $data['list_news_policy']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>"><?php echo e(str_limit($item->title_vn, 40)); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom ptb-20">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="copyright">
                    <span>Copyright &copy; 2018 <a href="#"></a> All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div>
</footer>