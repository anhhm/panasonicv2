<?php $__env->startSection('content'); ?>
   <div class="page-contact">
	    <div class="row">
	        <div class="col-md-6 col-sm-6 col-xs-12">

	            <div class="detail-caption">
	                <span> CONTACT INFORMATION</span>
	            </div>
	            <p>Cras ultricies ligula sed magna dictum porta. Pellentesque in ipsum id orci porta dapibus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Cras ultricies ligula sed magna dictum porta. Nulla porttitor accumsan tincidunt.</p>

	            <ul class="list-infomation">

	                <li><p> <i class="fa fa-building"></i> Brooklyn, NY 11213, USA</p></li>
	                <li><p><i class="fa fa-envelope"></i>    info@domain.com</p></li>
	                <li><p> <i class="fa fa-mobile"></i>   (+84) 234 56789</p></li>
	                <li><p> <i class="fa fa-clock-o"></i>  Opening Hours: 9AM – 5PM</p></li>
	            </ul>
	        </div>
	        <div class="col-md-6 col-sm-6 col-xs-12">
	            <!--leave a reply-->
	            <div class="detail-caption">
	                <span>  CONTACT FORM</span>
	            </div>
	            <form class="comment-form">
	                <div class="row">
	                    <div class="col-md-6 col-sm-6 col-xs-12">
	                        <div class="field-item">
	                            <input type="text" placeholder="Name" />
	                        </div>
	                    </div>
	                    <div class="col-md-6 col-sm-6 col-xs-12">
	                        <div class="field-item">
	                            <input type="text" placeholder="Email" />
	                        </div>
	                    </div>
	                </div>
	                <div class="field-item">
	                    <textarea placeholder="Text here..."></textarea>
	                </div>
	                <div class="field-item">
	                    <button class="my-btn my-btn-dark">SUBMIT</button>
	                </div>
	            </form>
	        </div>
	    </div>
	    <div class="map">
            <div id="map"></div>
        </div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('addjs'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
        var mapOptions = {
            zoom: 11,
            center: new google.maps.LatLng(40.6700, -73.9400),
            styles: [{ "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#000000" }, { "lightness": 40 }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 21 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 19 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }]
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(40.6700, -73.9400),
            map: map,
            title: 'Brooklyn, NY 11213, USA!'
        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>