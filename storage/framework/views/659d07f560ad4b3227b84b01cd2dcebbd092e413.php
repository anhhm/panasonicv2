<?php $__env->startSection('category',''); ?>
<?php $__env->startSection('action',trans('admin_catnews.news_header')); ?>
<?php $__env->startSection('addCss'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo isset($data['news']) ? $data['news']['title'] : null; ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_catnews.active')); ?></label>
                                <select class="form-control select2" name="active" data-placeholder="" style="width: 100%;">
                                    <?php if(App::isLocale('vi')): ?>
                                        <option value="0" <?php echo e($data['catnews']['active']=="0"? 'selected':''); ?>>Không</option>
                                        <option value="1" <?php echo e($data['catnews']['active']=="1"? 'selected':''); ?>>Có</option>
                                    <?php endif; ?>

                                    <?php if(App::isLocale('en')): ?>
                                        <option value="0" <?php echo e($data['catnews']['active']=="0"? 'selected':''); ?>>No</option>
                                        <option value="1" <?php echo e($data['catnews']['active']=="1"? 'selected':''); ?>>Yes</option>
                                    <?php endif; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_catnews.category')); ?></label>
                                <select class="form-control" name="id_cat" data-placeholder="chon danh muc" style="width: 100%;">

                                    <?php if(App::isLocale('vi')): ?>
                                        <option value="0">Không thư mục cha nào</option>
                                    <?php endif; ?>

                                    <?php if(App::isLocale('en')): ?>
                                         <option value="0">no parent directory</option>
                                    <?php endif; ?>
                                    <?php echo e(cat_parent($data['cat_news'],0,"&#187;",0)); ?>

                                </select>
                            </div>


                            <div class="form-group">
                                <label for="exampleInput"><?php echo e(trans('admin_catnews.title')); ?>:</label>
                                <input type="text" name="title_vn" class="form-control"  placeholder="<?php echo e(trans('admin_placeholder.placeholder_title_catnews')); ?>" value="<?php echo old('title',isset($data['catnews']) ? $data['catnews']['title_vn'] : null ); ?>">
                                <?php if($errors->has('title_vn')): ?>
                                    <p style="color:red"><?php echo e($errors->first('title_vn')); ?></p>
                                <?php endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo e(trans('admin_catnews.content')); ?>:</label>
                                <textarea class="form-control" class="form-control" name="content_vn" cols="30" rows="4"><?php echo old('content',isset($data['catnews']) ? $data['catnews']['content_vn'] : null ); ?></textarea>
                            </div>

                            <div class="form-group">
                                <div class="box-footer">
                                    <button name="submit" type="submit" id="submit" value="Update" class="btn btn-primary"><?php echo e(trans('actionbtn.btnadd')); ?></button>
                                    <a type="button" class="btn btn-default" href="<?php echo e(url('admin/cat_news/list')); ?>"><?php echo e(trans('actionbtn.btnreturn')); ?></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('addScript'); ?>
    <!-- Select2 -->
    <script src="<?php echo e(asset('backendTemp')); ?>/plugins/select2/select2.full.min.js"></script>
    <!-- Page script -->

    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
    </script>
    <!-- end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>