<?php $__env->startSection('content'); ?>
	<div class="main-container">
        <div class="tab-content">
            <div class="tab-pane active" id="MB">
        		<div class="google-map-area mb-50">
        			<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3723.537866723433!2d105.78390711976212!3d21.05116934787513!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1zdMOyYSBuaMOgIFBWViwgS8SQVCBOYW0gQ8aw4budbmcsIDIzNCBIb8OgbmcgUXXhu5FjIFZp4buHdCwgUS4gQuG6r2MgVOG7qyBMacOqbSwgSMOgIE7hu5lp!5e0!3m2!1svi!2s!4v1545813232538" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        		</div>
            </div>
            <div class="tab-pane" id="MT">
                <div class="google-map-area mb-50">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3834.0083739809556!2d108.19544721424502!3d16.06505524381839!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1zU-G7kSAxNTAgQuG6vyBWxINuIMSQw6BuLCBQLiBDaMOtbmggR2nDoW4sIFEuIFRoYW5oIEtow6osIMSQw6AgTuG6tW5nIA!5e0!3m2!1svi!2s!4v1545813303598" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="tab-pane" id="MN">
                <div class="google-map-area mb-50">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.027730344629!2d106.6619874141936!3d10.809187961545328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175293cceb6790f%3A0x39c6a032b072cee7!2sHai+Au+building!5e0!3m2!1svi!2s!4v1545813442519" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
		<div class="contact-area">
			<div class="container">
				<ul class="row">
					<li class="col-lg-4 col-md-4 col-sm-12 col-xs-12 active">
                        <a href="#MB" data-toggle="tab">
    						<div class="contuct-detail mb-50 p-20 bg-fff box-shadow">
    							<div class="contuct-title">
    								<h2>Khu vực miền bắc</h2>
    							</div>
                                <div class="same">
                                    <h5>Địa chỉ: </h5>
                                    <p>Tầng 2, tòa nhà PVV, KĐT Nam Cường, 234 Hoàng Quốc Việt, Q. Bắc Từ Liêm, Hà Nội</p>
                                </div>
                                <div class="same">
                                    <h5>EMAIL</h5>
                                    <p>hn@panasonicvietnam.com.vn</p>
                                </div>
                                <div class="same">
                                    <h5>PHONE NUMBER</h5>
                                    <p>024.665.88886 / 024.6675.9999 </p>
                                </div>
    						</div>
                        </a>
					</li>
					<li class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <a href="#MT" data-toggle="tab">
    						<div class="contuct-detail mb-50 p-20 bg-fff box-shadow">
    							<div class="contuct-title">
    								<h2>Khu vực miền trung</h2>
    							</div>
                                <div class="same">
                                    <h5>Địa chỉ: </h5>
                                    <p>Số 150 Bế Văn Đàn, P. Chính Gián, Q. Thanh Khê, Đà Nẵng</p>
                                </div>
                                <div class="same">
                                    <h5>EMAIL</h5>
                                    <p>dn@panasonicvietnam.com.vn</p>
                                </div>
                                <div class="same">
                                    <h5>PHONE NUMBER</h5>
                                    <p>024.665.88886</p>
                                </div>
    						</div>
                        </a>
					</li>
					<li class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <a href="#MN" data-toggle="tab">
    						<div class="contuct-detail mb-50 p-20 bg-fff box-shadow">
    							<div class="contuct-title">
    								<h2>Khu vực miền nam</h2>
    							</div>
                                <div class="same">
                                    <h5>Địa chỉ: </h5>
                                    <p>Tòa nhà Hải Âu Building, phường 4, Quận Tân Bình, Hồ Chí Minh </p>
                                </div>
                                <div class="same">
                                    <h5>EMAIL</h5>
                                    <p>hcm@panasonicvietnam.com.vn</p>
                                </div>
                                <div class="same">
                                    <h5>PHONE NUMBER</h5>
                                    <p>024.665.88886</p>
                                </div>
    						</div>
                        </a>
					</li>
				</ul>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>