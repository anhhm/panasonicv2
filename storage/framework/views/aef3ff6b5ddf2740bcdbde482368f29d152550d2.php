<?php $__env->startSection('content'); ?>
   <!--wrap-content-->
    <div id="wrap-body">
        <!--banner-->
        <div class="heading-group page-name color-w">
            <h1>blog</h1>
            <ul class="becku">
                <li><a href="#">Home</a></li>
                <li>Blog</li>
            </ul>
            <div class="star-group">
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o star-1x"></i>
                <i class="fa fa-star-o"></i>
            </div>
        </div>
        <!--/banner-->
        <div class="container" id="main-content">
            <div class="blog list-blog">
                <div class="row">
                    <?php if(isset($data['list_news'])): ?>
                        <?php $__currentLoopData = $data['list_news']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6">
                                <div class="thumbnail">
                                    <a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>" class="hover-img">
                                        <img width="508" height="285" src="<?php echo e($item->img_avatar); ?>" alt="image">
                                    </a>
                                    <div class="date">
                                        <span><?php echo e($item->created_at); ?></span>
                                    </div>
                                    <div class="caption">
                                        <h3><a href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>"><?php echo e($item->title_vn); ?></a></h3>
                                        <p>
                                            <?php echo e($item->content_vn); ?>

                                            <a class="readmore" href="<?php echo e(site_url('news')); ?>/<?php echo e($item->code); ?>">Đọc tiếp</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <p>Chưa có tin tức nào</p>
                    <?php endif; ?>
                    <!--blog-->
                </div>
            </div>
        </div>
    </div>
    <!--/wrap-content-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>