<!--colright-->
<div class="col-md-4 col-sm-12">
    <!--tab popular-->
    <ul role="tablist" class="tab-popular">
        <li class="active">
            <a href="#tab1" role="tab" data-toggle="tab">
                LATEST
            </a>
        </li>
        <li>
            <a href="#tab2" role="tab" data-toggle="tab">
                POPULAR
            </a>
        </li>

        <li>
            <a href="#tab3" role="tab" data-toggle="tab">
                RANDOM
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <ul class="list-news-popular">
                <?php for($i = 0; $i < 5; $i++): ?>
                    <li>
                        <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                            <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/8.jpg">
                        </a>
                        <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">Netflix Speeds Jumped 51% This Year</a></h3>
                        <div class="meta-post">
                            <a href="#">
                                Ashley Ford

                            </a>
                            <em></em>
                            <span>
                                24 Aug  2016
                            </span>
                        </div>
                    </li>
                <?php endfor; ?>
            </ul>

        </div>
        <div role="tabpanel" class="tab-pane" id="tab2">
            <ul class="list-news-popular">
                <?php for($i = 0; $i < 5; $i++): ?>
                    <li>
                        <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                            <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/13.jpg">
                        </a>
                        <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">Tumblr brings Apple’s ‘Live Photos’ to the web</a></h3>
                        <div class="meta-post">
                            <a href="#">
                                Super User
                            </a>
                            <em></em>
                            <span>
                                15 Sep 2016
                            </span>
                        </div>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab3">
            <ul class="list-news-popular">
                <?php for($i = 0; $i < 5; $i++): ?>
                    <li>
                        <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                            <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/13.jpg">
                        </a>
                        <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">Tumblr brings Apple’s ‘Live Photos’ to the web</a></h3>
                        <div class="meta-post">
                            <a href="#">
                                Super User
                            </a>
                            <em></em>
                            <span>
                                15 Sep 2016
                            </span>
                        </div>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>

    <!-- special post-->
    <div class="connect-us">
        <div class="widget-title">
            <span>
                Special post
            </span>
        </div>
        <div class="list-special">
            <?php for($i = 0; $i < 2; $i++): ?>
                <article class="news-two-large">
                    <a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">
                        <img alt="" src="<?php echo e(asset('teznews')); ?>/images/product/4.jpg">
                    </a>
                    <h3><a href="<?php echo e(site_url('news/cua-ot-singapore')); ?>">Six big ways MacOS Sierra is going to change your Apple experience</a></h3>
                    <div class="meta-post">
                        <a href="#">
                            Sue Benson
                        </a>
                        <em></em>
                        <span>
                            23 Sep 2016
                        </span>
                    </div>

                </article>
            <?php endfor; ?>
        </div>
    </div>
    <!-- advertisement-->
    <div class="ads hidden-sm hidden-xs">
        <p>advertisement</p>
        <img alt="" src="<?php echo e(asset('teznews')); ?>/images/ads.jpg" class="img-responsive" />
    </div>
</div>