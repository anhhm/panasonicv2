<?php $__env->startSection('content'); ?>
    <div class="login-box">
        <div class="login-logo">
            <a style="color: #fff;" href=""><b>CMS</b> website</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="<?php echo e(route('login')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>"  placeholder="<?php echo e(__('E-Mail Address')); ?>">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <?php if($errors->has('email')): ?>
                        <span class="invalid-feedback">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="form-group has-feedback">
                    <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" placeholder="<?php echo e(__('Password')); ?>">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <?php if($errors->has('password')): ?>
                        <span class="invalid-feedback">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo e(__('Login')); ?></button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <!-- <a href="<?php echo e(route('register')); ?>" class="text-center">Register a new membership</a> -->
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('auth.masterLogin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>