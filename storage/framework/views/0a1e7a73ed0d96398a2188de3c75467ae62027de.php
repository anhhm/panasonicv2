<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>TECHNEWS | Magagize & News HTML Template</title>
      <!--owl carousel-->

        <?php echo $__env->yieldContent('addCss'); ?>
        <?php echo $__env->make('home.template.linkcss', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </head>
    <body>
        <div class="loader" id="page-loader">
          <div class="loading-wrapper">
          </div>
        </div>

        <div class="page">
          <div class="container">
            <?php echo $__env->make('home.template.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo $__env->make('home.template.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo $__env->yieldContent('content'); ?>
          </div>
          <?php echo $__env->make('home.template.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        

        <?php echo $__env->make('home.template.linkjs', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('addjs'); ?>
        <!-- Load Phone number -->
        <div class="mypage-alo-phone">
            <a href="tel:+842866822522">
                <div class="animated infinite zoomIn mypage-alo-ph-circle"></div>
                <div class="animated infinite pulse mypage-alo-ph-circle-fill"></div>
                <div class="animated infinite tada mypage-alo-ph-img-circle"></div>
            </a>
        </div>
    </body>
</html>