<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();



//ROUTE BACKEND

Route::get('admin', 'Auth\DashboardController@getindex');
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['prefix'=>'admin'], function (){
    // Tin tuc
    Route::group(['prefix'=>'news'],function(){
        Route::match(['get','post'], '/{name?}','Auth\NewsController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\NewsController@index');
    });
    // Danh mục tin tức
    Route::group(['prefix'=>'cat_news'],function(){
        Route::match(['get','post'], '/{name?}','Auth\CatNewsController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\CatNewsController@index');
    });
    // Sản phẩm
    Route::group(['prefix'=>'products'],function(){
        Route::match(['get','post'], '/{name?}','Auth\ProductController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\ProductController@index');
        Route::match(['post'], 'checkcat','Auth\ProductController@CheckCat');
    });
    // Danh mục sản phẩm
    Route::group(['prefix'=>'cat_products'],function(){
        Route::match(['get','post'], '/{name?}','Auth\CatProductController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\CatProductController@index');
    });

    // Danh mục sản phẩm
    Route::group(['prefix'=>'producer'],function(){
        Route::match(['get','post'], '/{name?}','Auth\ProducerController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\ProducerController@index');
    });
     // Thư Viện
    Route::group(['prefix'=>'library'],function(){
        Route::match(['get','post'], '/{name?}','Auth\LibraryController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\LibraryController@index');
        Route::match(['post'],'Auth\LibraryController@postImages');
    });
    // Danh mục thư viện
    Route::group(['prefix'=>'cat_library'],function(){
        Route::match(['get','post'], '/{name?}','Auth\CatLibraryController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\CatLibraryController@index');
    });

    // Danh mục Config
    Route::group(['prefix'=>'config'],function(){
        Route::match(['get','post'], '/{name?}','Auth\ConfigController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\ConfigController@index');
    });

     // Banner Config
    Route::group(['prefix'=>'configbanner'],function(){
        Route::match(['get','post'], '/{name?}','Auth\BannerConfigController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\BannerConfigController@index');
    });

     // Tags
    Route::group(['prefix'=>'tags'],function(){
        Route::match(['get','post'], '/{name?}','Auth\TagsController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\TagsController@index');
    });
    
    // Người dùng
    Route::group(['prefix'=>'users'],function(){
        Route::match(['get','post'], '/{name?}','Auth\UserController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\UserController@index');
    });
    // Phân Quyền Nhóm Người Dùng
    Route::group(['prefix'=>'permissiongroup'],function(){
        Route::match(['get','post'], '/{name?}','Auth\PermissionGroupController@index');
        Route::match(['get','post'], '/{name?}/{id}','Auth\PermissionGroupController@index');
    });

    // Hướng dẫn sử dụng tool
    Route::group(['prefix'=>'guide'],function(){
        Route::get('news', 'Auth\GuidelController@News');
        Route::get('library', 'Auth\GuidelController@Library');
        Route::get('product', 'Auth\GuidelController@Product');
        Route::get('admin', 'Auth\GuidelController@Admin');
        Route::get('config', 'Auth\GuidelController@Config');
    });
});



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');
Route::get('/home','HomeController@index');
Route::get('/menu','MenuController@index');
Route::get('/library','LibraryController@index');
Route::get('/contact','ContactController@index');
//Route::get('/about','AboutUsController@index');
Route::get('/search','ProductController@search');
Route::get('/tag/{code}','ProductController@tag');
Route::group(['prefix'=>'news'],function(){
    Route::get('/', 'NewsController@index');
    Route::get('/list', 'NewsController@index');
    Route::get('/{code}', 'NewsController@detail');
    Route::get('list/{code}', 'NewsController@list');
});
//Add route to AboutUs Page
Route::group(['prefix'=>'about'],function(){
    Route::get('/', 'AboutUsController@index');
    //Route::get('/list', 'NewsController@index');
    Route::get('/{code}', 'AboutUsController@detail');
    //Route::get('list/{code}', 'NewsController@list');
});

Route::group(['prefix'=>'product'],function(){
    Route::get('/', 'ProductController@index');
    Route::get('/list', 'ProductController@index');
    Route::get('/{code}', 'ProductController@detail');
    Route::get('list/{code}', 'ProductController@list');
    Route::get('list-parent/{code}', 'ProductController@list_parent');
});


    //change locale
Route::post('/locale-chooser','LocaleController@changeLocale');
Route::post('/locale/',array(
        'before' => 'csrf',
        'as'     => 'locale-chooser',
        'uses'   => 'LocaleController@changeLocale',
    )
);
Route::get('/locale/',array(
        'before' => 'csrf',
        'as'     => 'locale-chooser',
        'uses'   => 'LocaleController@changeLocale',
    )
);

Route::match(['get','post'], '/locale','LocaleController@formChangeLocle');
//change locale






