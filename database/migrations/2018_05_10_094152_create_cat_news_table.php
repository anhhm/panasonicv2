<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_vn');
            $table->text('content_vn');
//            $table->string('title_en');
//            $table->text('content_en');
            $table->string('code');
            $table->tinyInteger('active');
            $table->integer('parent_id');
            $table->integer('creator_id');
            $table->integer('updator_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_news');
    }
}
