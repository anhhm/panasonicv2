<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_vn');
            $table->text('content_vn');
            $table->text('detail_vn');
//            $table->string('title_en');
//            $table->text('content_en');
//            $table->text('detail_en');
            $table->string('code');
            $table->string('img_avatar');
            $table->tinyInteger('active');
            $table->integer('creator_id');
            $table->integer('updator_id');
            $table->integer('id_cat')->unsigned();
            $table->foreign('id_cat')->references('id')->on('cat_news');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
