<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_view');
            $table->integer('action_edit');
            $table->integer('action_delete');
            $table->integer('creator_id');
            $table->integer('updator_id');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('group');
            $table->integer('permission_id')->unsigned();
            $table->foreign('permission_id')->references('id')->on('permission');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_group');
    }
}
