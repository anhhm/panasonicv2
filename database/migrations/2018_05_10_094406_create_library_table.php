<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_vn');
//            $table->string('title_en');
            $table->string('img_avatar');
            $table->string('list_img');
            $table->tinyInteger('active');
            $table->integer('creator_id');
            $table->integer('updator_id');
            $table->integer('id_cat')->unsigned();
            $table->foreign('id_cat')->references('id')->on('cat_library');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library');
    }
}
