<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission')->insert([
            'title'         =>  'Tin Tức',
            'content'       =>  'Quản Trị Tin Tức',
            'slug'          =>  'news',
            'creator_id'    =>  1,
            'updator_id'    =>  1,
        ]);
    }
}
