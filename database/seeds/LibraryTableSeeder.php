<?php

use Illuminate\Database\Seeder;

class LibraryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('library')->insert([
            'title_vn'              =>  'test',
            'img_avatar'            =>  'upload/images/no-images.jpg',
            'list_img'              =>  'upload/images/no-images.jpg',
            'active'                =>  1,
            'creator_id'            =>  1,
            'updator_id'            =>  1,
            'id_cat'                =>  1,
        ]);
    }
}
