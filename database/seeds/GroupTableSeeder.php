<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('group')->insert([
            'title'         =>  'admin',
            'content'       =>  'Quản trị trang web',
            'active'        =>  1,
            'creator_id'    =>  1,
            'updator_id'    =>  1,
        ]);
    }
}
