<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            GroupTableSeeder::class,
            UserTableSeeder::class,
            PermissionTableSeeder::class,
            PermissionGroupTableSeeder::class,
            CatProductTableSeeder::class,
            ProductTableSeeder::class,
            CatLibraryTableSeeder::class,
            LibraryTableSeeder::class,
            CatNewsTableSeeder::class,
            NewsTableSeeder::class,

        ]);
    }
}
