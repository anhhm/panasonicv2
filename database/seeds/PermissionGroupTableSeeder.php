<?php

use Illuminate\Database\Seeder;

class PermissionGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_group')->insert([
            'action_view'   =>  '1',
            'action_edit'   =>  '1',
            'action_delete' =>  '1',
            'creator_id'    =>  1,
            'updator_id'    =>  1,
            'group_id'      =>  1,
            'permission_id' =>  1,
        ]);
    }
}
