<?php

use Illuminate\Database\Seeder;

class CatProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_products')->insert([
            'title_vn'              =>  'test',
            'content_vn'            =>  'test',
            'code'                  =>  'test',
            'active'                =>  1,
            'creator_id'            =>  1,
            'updator_id'            =>  1,
        ]);
    }
}
