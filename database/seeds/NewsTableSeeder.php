<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            'title_vn'              =>  'test',
            'content_vn'            =>  'test',
            'detail_vn'             =>  'test',
            'code'                  =>  'test',
            'img_avatar'            =>  'upload/images/no-images.jpg',
            'creator_id'            =>  1,
            'updator_id'            =>  1,
            'active'                =>  1,
            'id_cat'                =>  1,
        ]);
    }
}
