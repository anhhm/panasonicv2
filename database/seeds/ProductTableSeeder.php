<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'title_vn'              =>  'test',
            'content_vn'            =>  'test',
            'detail_vn'             =>  'test',
            'code'                  =>  'test',
            'active'                =>  1,
            'img_avatar'            =>  'upload/images/no-images.jpg',
            'creator_id'            =>  1,
            'updator_id'            =>  1,
            'id_cat'                =>  1,
        ]);
    }
}
